@extends('site.layouts.master')
@section('title')
حفاظ
@endsection
@section('content')
<!-- Content page Start -->
  <div class="content-wrapper">
    <section class="content">
			<div class="row">
        <div class="col-md-4 col-md-offset-4">
					<div class="box box-warning">
						<div class="box-header with-border">
							@foreach($student as $s)
								<h3 class="box-title"><span class="semi-bold">دفع مديونيات الطالب : {{$s->student_name}}</span></h3>
							@endforeach
							<div class="box-tools pull-right">
								<a class="btn btn-box-tool" data-widget="collapse" data-toggle="tooltip" title="Collapse"><i class="fa fa-chevron-down"></i></a>
								<a class="btn btn-box-tool"><i class="fa fa-repeat"></i></a>
								<a class="btn btn-box-tool"><i class="fa fa-cog"></i></a>
								<a class="btn btn-box-tool" data-widget="remove" data-toggle="tooltip" title="Remove"><i class="fa fa-times"></i></a>
							</div>
						</div>
						<div class="box-body">
							@foreach($count as $c)
							<p>ديون الطالب : {{$c->amount * -1}}</p>
							<p>طرق الدفع المتاحة <i class="fa fa-arrow-down"></i></p><hr>
							<div id="paypal-button-container"></div>
								<script src="https://www.paypal.com/sdk/js?client-id=sb"></script>
								<script>
									paypal.Buttons({
										createOrder: function(data, actions) {
											return actions.order.create({
												purchase_units: [{
													amount: {
														value: '{{$c->amount * -1}}'
													}
												}]
											});
										}
									}).render('#paypal-button-container');
								</script>
								@endforeach
							</div>
						</div>
          </div><!-- /.col -->
      </div><!-- /.row -->
		</section>
  </div>
  <!-- Content page End -->
@endsection