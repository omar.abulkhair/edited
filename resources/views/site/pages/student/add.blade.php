@extends('site.layouts.master') 
@section('title')
 الطلاب 
@endsection
@section('content')
<!-- Content page Start -->
<div class="content-wrapper">
<section class="content-header">
      <h1>
        <i class="fa fa-arrow-left"></i>
        <span class="semi-bold">الرئيسية</span>
        <small>بيانات الطالب</small>
      </h1>
      <ol class="breadcrumb">
        <li><a href="{{route('site.home')}}"><i class="fa fa-home"></i> الرئيسية</a></li>
        <li><a href="{{route('site.students')}}"> بيانات الطلاب</a></li>
        <li class="active">اضافة</li>
      </ol>
    </section>
	<section class="content">
		<div class="row">
			<div class="col-md-8 col-md-offset-2">
			<div class="box box-warning">
					<div class="box-header with-border">
						<h3 class="box-title"><span class="semi-bold">اضافة طالب جديد</span></h3>
						<div class="box-tools pull-right">
							<a class="btn btn-box-tool" data-widget="collapse" data-toggle="tooltip" title="Collapse"><i class="fa fa-chevron-down"></i></a>
							<a class="btn btn-box-tool"><i class="fa fa-repeat"></i></a>
							<a class="btn btn-box-tool"><i class="fa fa-cog"></i></a>
							<a class="btn btn-box-tool" data-widget="remove" data-toggle="tooltip" title="Remove"><i class="fa fa-times"></i></a>
						</div>
					</div>
					<div class="box-body">
						<form class="mtb-15" action="{{route('site.students.add')}}" enctype="multipart/form-data" method="post" onsubmit="return false;">{{ csrf_field() }}
							<div class="row form-row">
								<div class="col-md-12">
									<div class="user-img-upload">
										<div class="fileUpload user-editimg">
											<!--<span><i class="fa fa-camera"></i> اضف</span>-->
											<input type='file' id="imgInp" class="upload" name="image" />
											<input type="hidden" value="students" name="storage" >
										</div>
										<img src="{{asset('assets/site/img/add.png')}}" id="blah" class="img-circle" alt="">
										<p id="result"></p>
										<br>
									</div>
								</div>
								
								<div class="col-md-12">
									<div class="form-group">
										<label class="checkbox-inline">مشترك فى المواصلات ؟</label>
										<input class="checkbox-inline minimal" name="transportation" type="checkbox">
									</div>
								</div>
								
								<div class="col-md-6">
									<div class="form-group pmd-textfield pmd-textfield-floating-label">
										<label class="control-label">اسم الطالب</label>
										<input name="name" class="form-control" type="text">
									</div>
								</div>
								<div class="col-md-6">
									<div class="form-group pmd-textfield pmd-textfield-floating-label">
										<label class="control-label">الرقم المدنى</label>
										<input name="national_id" class="form-control" type="text">
									</div>
								</div>
								<div class="col-md-6">
									<div class="form-group pmd-textfield pmd-textfield-floating-label">
										<label class="control-label">البريد الالكترونى</label>
										<input name="email" class="form-control" type="text">
									</div>
								</div>
								<div class="col-md-6">
									<div class="form-group pmd-textfield pmd-textfield-floating-label">
										<label>الجنسية</label>
										<select name="nationality" class="form-control pmd-select2 select2">
											<option></option>
                                              <option value="أفغانستان">أفغانستان</option>
                                              <option value="ألبانيا">ألبانيا</option>
                                              <option value="الجزائر">الجزائر</option>
                                              <option value="أندورا">أندورا</option>
                                              <option value="أنغولا">أنغولا</option>
                                              <option value="أنتيغوا وباربودا">أنتيغوا وباربودا</option>
                                              <option value="الأرجنتين">الأرجنتين</option>
                                              <option value="أرمينيا">أرمينيا</option>
                                              <option value="أستراليا">أستراليا</option>
                                              <option value="النمسا">النمسا</option>
                                              <option value="أذربيجان">أذربيجان</option>
                                              <option value="البهاما">البهاما</option>
                                              <option value="البحرين">البحرين</option>
                                              <option value="بنغلاديش">بنغلاديش</option>
                                              <option value="باربادوس">باربادوس</option>
                                              <option value="بيلاروسيا">بيلاروسيا</option>
                                              <option value="بلجيكا">بلجيكا</option>
                                              <option value="بليز">بليز</option>
                                              <option value="بنين">بنين</option>
                                              <option value="بوتان">بوتان</option>
                                              <option value="بوليفيا">بوليفيا</option>
                                              <option value="البوسنة والهرسك ">البوسنة والهرسك </option>
                                              <option value="بوتسوانا">بوتسوانا</option>
                                              <option value="البرازيل">البرازيل</option>
                                              <option value="بروناي">بروناي</option>
                                              <option value="بلغاريا">بلغاريا</option>
                                              <option value="بوركينا فاسو ">بوركينا فاسو </option>
                                              <option value="بوروندي">بوروندي</option>
                                              <option value="كمبوديا">كمبوديا</option>
                                              <option value="الكاميرون">الكاميرون</option>
                                              <option value="كندا">كندا</option>
                                              <option value="الرأس الأخضر">الرأس الأخضر</option>
                                              <option value="جمهورية أفريقيا الوسطى ">جمهورية أفريقيا الوسطى </option>
                                              <option value="تشاد">تشاد</option>
                                              <option value="تشيلي">تشيلي</option>
                                              <option value="الصين">الصين</option>
                                              <option value="كولومبيا">كولومبيا</option>
                                              <option value="جزر القمر">جزر القمر</option>
                                              <option value="كوستاريكا">كوستاريكا</option>
                                              <option value="ساحل العاج">ساحل العاج</option>
                                              <option value="كرواتيا">كرواتيا</option>
                                              <option value="كوبا">كوبا</option>
                                              <option value="قبرص">قبرص</option>
                                              <option value="التشيك">التشيك</option>
                                              <option value="جمهورية الكونغو الديمقراطية">جمهورية الكونغو الديمقراطية</option>
                                              <option value="الدنمارك">الدنمارك</option>
                                              <option value="جيبوتي">جيبوتي</option>
                                              <option value="دومينيكا">دومينيكا</option>
                                              <option value="جمهورية الدومينيكان">جمهورية الدومينيكان</option>
                                              <option value="تيمور الشرقية ">تيمور الشرقية </option>
                                              <option value="الإكوادور">الإكوادور</option>
                                              <option value="مصر">مصر</option>
                                              <option value="السلفادور">السلفادور</option>
                                              <option value="غينيا الاستوائية">غينيا الاستوائية</option>
                                              <option value="إريتريا">إريتريا</option>
                                              <option value="إستونيا">إستونيا</option>
                                              <option value="إثيوبيا">إثيوبيا</option>
                                              <option value="فيجي">فيجي</option>
                                              <option value="فنلندا">فنلندا</option>
                                              <option value="فرنسا">فرنسا</option>
                                              <option value="الغابون">الغابون</option>
                                              <option value="غامبيا">غامبيا</option>
                                              <option value="جورجيا">جورجيا</option>
                                              <option value="ألمانيا">ألمانيا</option>
                                              <option value="غانا">غانا</option>
                                              <option value="اليونان">اليونان</option>
                                              <option value="جرينادا">جرينادا</option>
                                              <option value="غواتيمالا">غواتيمالا</option>
                                              <option value="غينيا">غينيا</option>
                                              <option value="غينيا بيساو">غينيا بيساو</option>
                                              <option value="غويانا">غويانا</option>
                                              <option value="هايتي">هايتي</option>
                                              <option value="هندوراس">هندوراس</option>
                                              <option value="المجر">المجر</option>
                                              <option value="آيسلندا">آيسلندا</option>
                                              <option value="الهند">الهند</option>
                                              <option value="إندونيسيا">إندونيسيا</option>
                                              <option value="إيران">إيران</option>
                                              <option value="العراق">العراق</option>
                                              <option value="جمهورية أيرلندا ">جمهورية أيرلندا </option>
                                              <option value="فلسطين">فلسطين</option>
                                              <option value="إيطاليا">إيطاليا</option>
                                              <option value="جامايكا">جامايكا</option>
                                              <option value="اليابان">اليابان</option>
                                              <option value="الأردن">الأردن</option>
                                              <option value="كازاخستان">كازاخستان</option>
                                              <option value="كينيا">كينيا</option>
                                              <option value="كيريباتي">كيريباتي</option>
                                              <option value="الكويت">الكويت</option>
                                              <option value="قرغيزستان">قرغيزستان</option>
                                              <option value="لاوس">لاوس</option>
                                              <option value="لاوس">لاوس</option>
                                              <option value="لاتفيا">لاتفيا</option>
                                              <option value="لبنان">لبنان</option>
                                              <option value="ليسوتو">ليسوتو</option>
                                              <option value="ليبيريا">ليبيريا</option>
                                              <option value="ليبيا">ليبيا</option>
                                              <option value="ليختنشتاين">ليختنشتاين</option>
                                              <option value="ليتوانيا">ليتوانيا</option>
                                              <option value="لوكسمبورغ">لوكسمبورغ</option>
                                              <option value="مدغشقر">مدغشقر</option>
                                              <option value="مالاوي">مالاوي</option>
                                              <option value="ماليزيا">ماليزيا</option>
                                              <option value="جزر المالديف">جزر المالديف</option>
                                              <option value="مالي">مالي</option>
                                              <option value="مالطا">مالطا</option>
                                              <option value="جزر مارشال">جزر مارشال</option>
                                              <option value="موريتانيا">موريتانيا</option>
                                              <option value="موريشيوس">موريشيوس</option>
                                              <option value="المكسيك">المكسيك</option>
                                              <option value="مايكرونيزيا">مايكرونيزيا</option>
                                              <option value="مولدوفا">مولدوفا</option>
                                              <option value="موناكو">موناكو</option>
                                              <option value="منغوليا">منغوليا</option>
                                              <option value="الجبل الأسود">الجبل الأسود</option>
                                              <option value="المغرب">المغرب</option>
                                              <option value="موزمبيق">موزمبيق</option>
                                              <option value="بورما">بورما</option>
                                              <option value="ناميبيا">ناميبيا</option>
                                              <option value="ناورو">ناورو</option>
                                              <option value="نيبال">نيبال</option>
                                              <option value="هولندا">هولندا</option>
                                              <option value="نيوزيلندا">نيوزيلندا</option>
                                              <option value="نيكاراجوا">نيكاراجوا</option>
                                              <option value="النيجر">النيجر</option>
                                              <option value="نيجيريا">نيجيريا</option>
                                              <option value="كوريا الشمالية ">كوريا الشمالية </option>
                                              <option value="النرويج">النرويج</option>
                                              <option value="سلطنة عمان">سلطنة عمان</option>
                                              <option value="باكستان">باكستان</option>
                                              <option value="بالاو">بالاو</option>
                                              <option value="بنما">بنما</option>
                                              <option value="بابوا غينيا الجديدة">بابوا غينيا الجديدة</option>
                                              <option value="باراغواي">باراغواي</option>
                                              <option value="بيرو">بيرو</option>
                                              <option value="الفلبين">الفلبين</option>
                                              <option value="بولندا">بولندا</option>
                                              <option value="البرتغال">البرتغال</option>
                                              <option value="قطر">قطر</option>
                                              <option value="جمهورية الكونغو">جمهورية الكونغو</option>
                                              <option value="جمهورية مقدونيا">جمهورية مقدونيا</option>
                                              <option value="رومانيا">رومانيا</option>
                                              <option value="روسيا">روسيا</option>
                                              <option value="رواندا">رواندا</option>
                                              <option value="سانت كيتس ونيفيس">سانت كيتس ونيفيس</option>
                                              <option value="سانت لوسيا">سانت لوسيا</option>
                                              <option value="سانت فنسينت والجرينادينز">سانت فنسينت والجرينادينز</option>
                                              <option value="ساموا">ساموا</option>
                                              <option value="سان مارينو">سان مارينو</option>
                                              <option value="ساو تومي وبرينسيب">ساو تومي وبرينسيب</option>
                                              <option value="السعودية">السعودية</option>
                                              <option value="السنغال">السنغال</option>
                                              <option value="صربيا">صربيا</option>
                                              <option value="سيشيل">سيشيل</option>
                                              <option value="سيراليون">سيراليون</option>
                                              <option value="سنغافورة">سنغافورة</option>
                                              <option value="سلوفاكيا">سلوفاكيا</option>
                                              <option value="سلوفينيا">سلوفينيا</option>
                                              <option value="جزر سليمان">جزر سليمان</option>
                                              <option value="الصومال">الصومال</option>
                                              <option value="جنوب أفريقيا">جنوب أفريقيا</option>
                                              <option value="كوريا الجنوبية">كوريا الجنوبية</option>
                                              <option value="جنوب السودان">جنوب السودان</option>
                                              <option value="إسبانيا">إسبانيا</option>
                                              <option value="سريلانكا">سريلانكا</option>
                                              <option value="السودان">السودان</option>
                                              <option value="سورينام">سورينام</option>
                                              <option value="سوازيلاند">سوازيلاند</option>
                                              <option value="السويد">السويد</option>
                                              <option value="سويسرا">سويسرا</option>
                                              <option value="سوريا">سوريا</option>
                                              <option value="طاجيكستان">طاجيكستان</option>
                                              <option value="تنزانيا">تنزانيا</option>
                                              <option value="تايلاند">تايلاند</option>
                                              <option value="توغو">توغو</option>
                                              <option value="تونجا">تونجا</option>
                                              <option value="ترينيداد وتوباغو">ترينيداد وتوباغو</option>
                                              <option value="تونس">تونس</option>
                                              <option value="تركيا">تركيا</option>
                                              <option value="تركمانستان">تركمانستان</option>
                                              <option value="توفالو">توفالو</option>
                                              <option value="أوغندا">أوغندا</option>
                                              <option value="أوكرانيا">أوكرانيا</option>
                                              <option value="الإمارات العربية المتحدة">الإمارات العربية المتحدة</option>
                                              <option value="المملكة المتحدة">المملكة المتحدة</option>
                                              <option value="الولايات المتحدة">الولايات المتحدة</option>
                                              <option value="أوروغواي">أوروغواي</option>
                                              <option value="أوزبكستان">أوزبكستان</option>
                                              <option value="فانواتو">فانواتو</option>
                                              <option value="فنزويلا">فنزويلا</option>
                                              <option value="فيتنام">فيتنام</option>
                                              <option value="اليمن">اليمن</option>
                                              <option value="زامبيا">زامبيا</option>
                                              <option value="زيمبابوي">زيمبابوي</option>
										</select>
									</div>
								</div>
								<div class="col-md-6">
									<div class="form-group pmd-textfield pmd-textfield-floating-label">
										<label>الجنس</label>
										<select name="gender" class="form-control pmd-select2 select2">
											<option></option>
											<option value="ذكر">ذكر</option>
											<option value="أنثى">أنثى</option>
										</select>
									</div>
								</div>
								<div class="col-md-6">
									<div class="form-group pmd-textfield pmd-textfield-floating-label">
										<label>الفصل الدراسي</label>
										<select name="season_id" class="form-control pmd-select2 select2">
											<option></option>
											@foreach($seasons as $season)
											<option value="{{$season->id}}">{{$season->season_name}}</option>@endforeach</select>
									</div>
								</div>
								<div class="col-md-6">
                                    <div class="form-group pmd-textfield pmd-textfield-floating-label">
                                        <label>السنة</label>
                                        <select name="year" class="form-control pmd-select2 select2">
                                        	<option></option>
                                            <option value="2019/2020">2019/2020</option>
                                            <option value="2020/2021">2020/2021</option>
                                            <option value="2021/2022">2021/2022</option>
                                            <option value="2022/2023">2022/2023</option>
                                            <option value="2023/2024">2023/2024</option>
                                            <option value="2024/2025">2024/2025</option>
                                            <option value="2025/2026">2025/2026</option>
                                            <option value="2026/2027">2026/2027</option>
                                            <option value="2027/2028">2027/2028</option>
                                            <option value="2028/2029">2028/2029</option>
                                            <option value="2029/2030">2029/2030</option>
                                            <option value="2030/2031">2030/2031</option>
                                            <option value="2031/2032">2031/2032</option>
                                            <option value="2032/2033">2032/2033</option>
                                            <option value="2033/2034">2033/2034</option>
                                            <option value="2034/2035">2034/2035</option>
                                            <option value="2035/2036">2035/2036</option>
                                            <option value="2036/2037">2036/2037</option>
                                            <option value="2037/2038">2037/2038</option>
                                            <option value="2038/2039">2038/2039</option>
                                            <option value="2039/2040">2039/2040</option>
                                            <option value="2040/2041">2040/2041</option>
                                        </select>
                                    </div>
                                </div>
								<div class="col-md-6">
									<div class="form-group pmd-textfield pmd-textfield-floating-label">
										<label class="control-label">تاريخ الميلاد</label>
										<input name="birth" type="text" class="form-control datepicker">
									</div>
								</div>
								<div class="col-md-6">
									<div class="form-group pmd-textfield pmd-textfield-floating-label">
										<label class="control-label">يوم الالتحاق</label>
										<input name="first_day" type="text" class="form-control datepicker">
									</div>
								</div>
								<div class="col-md-6">
									<div class="form-group pmd-textfield pmd-textfield-floating-label">
										<label>المركز</label>
										<select name="center_id" class="form-control pmd-select2 select2">
											<option></option>
											@foreach($centers as $center)
											<option value="{{$center->id}}">{{$center->center_name}}</option>@endforeach</select>
									</div>
                </div>
                <div class="col-md-12">
                    <div class="form-group pmd-textfield pmd-textfield-floating-label">
                      <label class="control-label">عنوان الطالب</label>
                      <textarea name="address" class="form-control" rows="15"></textarea>
                    </div>
                </div>
								<div class="col-md-12">
									<br> <a href="{{route('site.students')}}" class="btn btn-primary btn-orange pmd-ripple-effect btn-sm"> الغاء</a>
									<button type="submit" class="btn btn-blue btn-blue addButton pmd-ripple-effect btn-sm">حفظ</button>
								</div>
							</div>
						</form>
					</div>
				</div>
			</div>
	</section>
</div>
<!-- Content page End -->
@endsection