@extends('admin.layouts.master') 
@section('title')
 الحلقات 
@endsection
@section('content')
<!-- Content page Start -->
<div class="content-wrapper">
<section class="content-header">
      <h1>
        <i class="fa fa-arrow-left"></i>
        <span class="semi-bold">الرئيسية</span>
        <small>بيانات الحلقة</small>
      </h1>
      <ol class="breadcrumb">
        <li><a href="{{route('admin.home')}}"><i class="fa fa-home"></i> الرئيسية</a></li>
        <li><a href="{{route('admin.courses')}}"> بيانات الحلقات</a></li>
        <li class="active">اضافة</li>
      </ol>
    </section>
	<section class="content">
		<div class="row">
			<div class="col-md-8 col-md-offset-2">
				<div class="box box-warning">
					<div class="box-header with-border">
						<h3 class="box-title"><span class="semi-bold">اضافة حلقة جديد</span></h3>
						<div class="box-tools pull-right">
							<a class="btn btn-box-tool" data-widget="collapse" data-toggle="tooltip" title="Collapse"><i class="fa fa-chevron-down"></i></a>
							<a class="btn btn-box-tool"><i class="fa fa-repeat"></i></a>
							<a class="btn btn-box-tool"><i class="fa fa-cog"></i></a>
							<a class="btn btn-box-tool" data-widget="remove" data-toggle="tooltip" title="Remove"><i class="fa fa-times"></i></a>
						</div>
					</div>
					<div class="box-body">
						<form class="mtb-15" action="{{route('admin.courses.add')}}" enctype="multipart/form-data" method="post" onsubmit="return false;">{{ csrf_field() }}
							<div class="row">
								<div class="col-md-6">
									<div class="form-group pmd-textfield pmd-textfield-floating-label">
										<label class="control-label">اسم الحلقة</label>
										<input name="name" class="form-control" type="text">
									</div>
								</div>
                                <div class="col-md-6">
									<div class="form-group pmd-textfield pmd-textfield-floating-label">
										<label class="control-label">الحد الأقصى لعدد طلاب الحلقة</label>
										<input name="max_num" class="form-control" type="text">
									</div>
								</div>
								<div class="col-md-6">
									<div class="form-group pmd-textfield pmd-textfield-floating-label">
										<label>المركز</label>
										<select name="center_id" class="form-control pmd-select2 select2">
											<option></option>
											@foreach($centers as $center)
												<option value="{{$center->id}}">{{$center->center_name}}</option>
											@endforeach
										</select>
									</div>
								</div>
								<div class="col-md-6">
									<div class="form-group pmd-textfield pmd-textfield-floating-label">
										<label>نوع الحلقة</label>
										<select name="coursetype_id" class="form-control pmd-select2 select2">
											<option></option>
											@foreach($types as $type)
												<option value="{{$type->id}}">{{$type->type_name}}</option>
											@endforeach
										</select>
									</div>
								</div>
								<div class="col-md-6">
									<div class="form-group">
										<label class="switch">
										<input type="checkbox" id="togBtn" name="active" checked>
										<div class="slider round">
											<span class="on">نشط</span>
											<span class="off">غير نشط</span>
										</div>
										</label>
									</div>
								</div>
								<div class="col-md-12">
									<div class="form-group pmd-textfield pmd-textfield-floating-label">
										<label class="control-label">عدد الأيام</label>
										<select id="numberselector" class="form-control pmd-select2 select2" name="time">
										<option>             </option>
										<option value="1">يوم</option>
										<option value="2">يومين</option>
										<option value="3">3 أيام</option>
										<option value="4">4 أيام</option>
										<option value="5">5 أيام</option>
										<option value="6">6 أيام</option>
										<option value="7">7 أيام</option>
										</select>
									</div>
									<div id='payemnt-info'></div>
								</div>
								<div class="col-md-12">
									<br> <a href="{{route('admin.courses')}}" class="btn btn-primary btn-orange pmd-ripple-effect btn-sm"> الغاء</a>
									<button type="submit" class="btn btn-blue btn-blue addButton pmd-ripple-effect btn-sm">حفظ</button>
								</div>
							</div>
						</form>
					</div>
				</div>
			</div>
	</section>
</div>
<!-- Content page End -->
@endsection