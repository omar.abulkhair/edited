@extends('admin.layouts.master') 
@section('title')
 الحلقات 
@endsection
@section('content')
<!-- Content page Start -->
<div class="content-wrapper">
<section class="content-header">
      <h1>
        <i class="fa fa-arrow-left"></i>
        <span class="semi-bold">الرئيسية</span>
        <small>بيانات الحلقة</small>
      </h1>
      <ol class="breadcrumb">
        <li><a href="{{route('admin.home')}}"><i class="fa fa-home"></i> الرئيسية</a></li>
        <li><a href="{{route('admin.courses')}}"> بيانات الحلقات</a></li>
        <li class="active">تعديل</li>
      </ol>
    </section>
	<section class="content">
		<div class="row">
			<div class="col-md-8 col-md-offset-2">
				<div class="box box-warning">
					<div class="box-header with-border">
						<h3 class="box-title"><span class="semi-bold">تعديل بيانات الحلقة</span></h3>
						<div class="box-tools pull-right">
							<a class="btn btn-box-tool" data-widget="collapse" data-toggle="tooltip" title="Collapse"><i class="fa fa-chevron-down"></i></a>
							<a class="btn btn-box-tool"><i class="fa fa-repeat"></i></a>
							<a class="btn btn-box-tool"><i class="fa fa-cog"></i></a>
							<a class="btn btn-box-tool" data-widget="remove" data-toggle="tooltip" title="Remove"><i class="fa fa-times"></i></a>
						</div>
					</div>
					<div class="box-body">
          	@foreach($groups as $group)
						<form class="mtb-15" action="{{route('admin.courses.edit' , ['id' => $group->id])}}" enctype="multipart/form-data" method="post" onsubmit="return false;">{{ csrf_field() }}
							
							
							<div class="row form-row">
							    
								<div class="col-md-6">
									<div class="form-group pmd-textfield pmd-textfield-floating-label">
										<label class="control-label">اسم الحلقة</label>
										<input value="{{$group->course_name}}" name="name" class="form-control" type="text">
									</div>
								</div>
                                <div class="col-md-6">
									<div class="form-group pmd-textfield pmd-textfield-floating-label">
										<label class="control-label">الحد الأقصى لعدد طلاب الحلقة</label>
										<input name="max_num" value="{{$group->max_num}}" class="form-control" type="text">
									</div>
								</div>
								<div class="col-md-6">
									<div class="form-group pmd-textfield pmd-textfield-floating-label">
										<label class="control-label">المركز</label>
										<select name="center_id" class="select2 pmd-select2 form-control">
						                    <option value="{{$group->center_id}}" selected>{{ isset($group->center->center_name) ? $group->center->center_name: ''}}</option>
						                    @foreach($centers as $center)
												<option value="{{$center->id}}">{{$center->center_name}}</option>
											@endforeach
										</select>
									</div>
								</div>
								<div class="col-md-6">
									<div class="form-group pmd-textfield pmd-textfield-floating-label">
										<label class="control-label">نوع الحلقة</label>
										<select name="coursetype_id" class="select2 pmd-select2 form-control">
										    
											<option value="{{$group->id}}" selected>{{ isset($group->CourseType->type_name) ? $group->CourseType->type_name:''}}</option>
									
											@foreach($types as $type)
												<option value="{{$type->id}}">{{$type->type_name}}</option>
											@endforeach
										</select>
									</div>
								</div>
								<div class="col-md-6">
									<div class="form-group">
										<label class="switch">
										<input type="checkbox" id="togBtn" name="active" @if($group->active == 1) checked @endif>
										<div class="slider round">
											<span class="on">نشط</span>
											<span class="off">غير نشط</span>
										</div>
										</label>
									</div>
								</div>
								<div class="col-md-12">
								<h5>تعديل مواعيد الحلقة</h5>
									<div class='payemnt-info'>
									@foreach($times as $time)
									<div class="numbers">
										<div class="inner">
											<h3>بيانات اليوم {{$loop->index + 1}}</h3>
											<div class="row">
												<div class="col-md-4">
													<div class="form-group pmd-textfield pmd-textfield-floating-label">
														<label class="control-label"> اليوم </label>
														<select class="form-control" name="timeName_{{$time->id}}">
															<option value="{{$time->day}}" selected>{{$time->day}}</option>
															<option value="السبت">السبت</option>
															<option value="الأحد">الأحد</option>
															<option value="الاثنين">الأثنين</option>
															<option value="الثلاثاء">الثلاثاء</option>
															<option value="الأربعاء">الأربعاء</option>
															<option value="الخميس">الخميس</option>
															<option value="الجمعة">الجمعة</option>
														</select>
													</div>
												</div>
												<div class="col-md-4">
													<div class="form-group pmd-textfield pmd-textfield-floating-label">
														<label class="control-label">الوقت من</label>
														<input type="text" class="form-control datetimepicker-default" value="{{$time->time_from}}" name="timeFrom_{{$time->id}}" placeholder="">
													</div>
												</div>
												<div class="col-md-4">
													<div class="form-group pmd-textfield pmd-textfield-floating-label">
														<label class="control-label">الوقت إلى</label>
														<input type="text" class="form-control datetimepicker-default" value="{{$time->time_to}}" name="timeTo_{{$time->id}}" placeholder="">
													</div>
												</div>
											</div>
										</div>
									</div>
									@endforeach
									</div>
									<h5> اضافة موعد جديد</h5>
									<div class="form-group pmd-textfield pmd-textfield-floating-label">
										<label class="control-label">عدد الأيام</label>
										<select id="numberselector" class="form-control pmd-select2 select2" name="time">
										<option>             </option>
										<option value="1">يوم</option>
										<option value="2">يومين</option>
										<option value="3">3 أيام</option>
										<option value="4">4 أيام</option>
										<option value="5">5 أيام</option>
										<option value="6">6 أيام</option>
										<option value="7">7 أيام</option>
										</select>
									</div>
									<div id='payemnt-info'></div>
								</div>
								</div>
								<div class="col-md-12">
									<br> <a href="{{route('admin.courses')}}" class="btn btn-orange pmd-ripple-effect btn-sm"> الغاء</a>
									<button type="submit" class="btn btn-blue addButton pmd-ripple-effect btn-sm">حفظ</button>
								</div>
							</div>
						</form>
            			@endforeach
            		
					</div>
				</div>
			</div>
			<div class="row">
				<div class="col-md-8 col-md-offset-2">
					<div class="box box-warning">
						<div class="box-header with-border">
							<h3 class="box-title"><span class="semi-bold"> مواعيد الحلقة</span></h3>
							<div class="box-tools pull-right">
								<a class="btn btn-box-tool" data-widget="collapse" data-toggle="tooltip" title="Collapse"><i class="fa fa-chevron-down"></i></a>
								<a class="btn btn-box-tool"><i class="fa fa-repeat"></i></a>
								<a class="btn btn-box-tool"><i class="fa fa-cog"></i></a>
								<a class="btn btn-box-tool" data-widget="remove" data-toggle="tooltip" title="Remove"><i class="fa fa-times"></i></a>
							</div>
						</div>
						<div class="box-body">
							<table class="table table-bordered table-striped">
								<thead>
									<tr>
										<th>الترتيب</th>
										<th>اليوم</th>
										<th>من</th>
										<th>الى</th>
										<th>العمليات</th>
									</tr>
								</thead>
								<tbody>
								@foreach($times as $t)
									<tr>
										<td>{{$loop->index + 1}}</td>
										<td>{{$t->day}}</td>
										<td>{{$t->time_from}}</td>
										<td>{{$t->time_to}}</td>
										<td>
											<a class="btndelet" href="{{ route('admin.coursetime.delete' , ['id' => $t->id]) }}"  title="حذف">
												<i class="fa fa-trash"></i></a>
										</td>
									</tr>
								@endforeach
								</tbody>
							</table>
						</div>
					</div>
				</div>
			</div>
			<div class="row">
				<div class="col-md-8 col-md-offset-2">
					<div class="box box-warning">
						<div class="box-header with-border">
							<h3 class="box-title"><span class="semi-bold"> مستويات الحلقة</span></h3>
							<div class="box-tools pull-right">
								<a class="btn btn-box-tool" data-widget="collapse" data-toggle="tooltip" title="Collapse"><i class="fa fa-chevron-down"></i></a>
								<a class="btn btn-box-tool"><i class="fa fa-repeat"></i></a>
								<a class="btn btn-box-tool"><i class="fa fa-cog"></i></a>
								<a class="btn btn-box-tool" data-widget="remove" data-toggle="tooltip" title="Remove"><i class="fa fa-times"></i></a>
							</div>
						</div>
						<div class="box-body">
							<table class="table table-bordered table-striped">
								<thead>
									<tr>
										<th>الترتيب</th>
										<th>المستوى</th>
										<th>العمليات</th>
									</tr>
								</thead>
								<tbody>
								@foreach($tlevels as $tlevel)
									<tr>
										<td>{{$loop->index + 1}}</td>
										<td>{{$tlevel->level_name}}</td>
										<td>
											<a class="btndelet" href="{{ route('admin.tcourse.delete' , ['id' => $tlevel->id]) }}" title="حذف">
											<i class="fa fa-trash"></i></a>
										</td>
									</tr>
								@endforeach
								</tbody>
							</table>
						</div>
					</div>
				</div>
			</div>
	</section>
</div>
</section>
<!-- Content page End -->
@endsection