@extends('admin.layouts.master')
@section('title')
شهاده درجه الماهر
  
@endsection
@section('content')
<!-- Content page Start -->
  <div class="content-wrapper">
    <!--<section class="content-header">-->
    <!--  <h1>-->
    <!--    <i class="fa fa-arrow-left"></i>-->
    <!--    <span class="semi-bold">Dashboard</span>-->
    <!--    <small>Control panel</small>-->
    <!--  </h1>-->
    <!--  <ol class="breadcrumb">-->
    <!--    <li><a href="#"><i class="fa fa-home"></i> Home</a></li>-->
    <!--    <li class="active">Dashboard</li>-->
    <!--  </ol>-->
    <!--</section>-->-->
    <section class="content">
        <div class="row">
            <div class="col-md-10 col-md-offset-1">
                <div class="box box-warning">
					<div class="box-header with-border">
						<h3 class="box-title"><span class="semi-bold">   شهاده درجه الماهر </span></h3>
						<div class="box-tools pull-right">
							<a class="btn btn-box-tool" data-widget="collapse" data-toggle="tooltip" title="Collapse"><i class="fa fa-chevron-down"></i></a>
							<a class="btn btn-box-tool"><i class="fa fa-repeat"></i></a>
							<a class="btn btn-box-tool"><i class="fa fa-cog"></i></a>
							<a class="btn btn-box-tool" data-widget="remove" data-toggle="tooltip" title="Remove"><i class="fa fa-times"></i></a>
						</div>
					</div>
				
                   
               
                    <div class="box-body">
                        
                            <table id="tables" class=" dataTable no-footer dtr-inline" style="width:100%">
                        <thead>
                        <tr>
                            <th class="num">#</th>
                                <th> اسم الطالب</th>
                           
                               <th>الفصل الدراسي</th>    
                               <th>العام الدراسي  </th>
                             
                            </th>
                             <th> العمليات</th>
                        </tr>
                           <tr class="tr-head">
                                  <th>الترتيب</th>
                                 <th> اسم الطالب</th>
                           
                               <th>الفصل الدراسي</th>   
                               
                               <th>العام الدراسي  </th>
                             
                             </th>
                             <th> العمليات</th>
                                 </tr>
                        </thead>
                        <tbody>  
                 

          @foreach( $students as $student)             
                    <tr>      
                    <td class="num">{{ $loop->iteration }}</td>
                       <td>  {{$student->student_name}}  </td> 
                        
                     
                           <td>{{ isset($student->season->season_name) ?  $student->season->season_name  : ''}} </td>
                           
                             <td>{{ isset($student->season->season_year) ?  $student->season->season_year  : ''}} </td>  
                       
                    
                 <td>    <a href="{{route('admin.reports.printsss', ['id' => $student->id])}}" title="طباعه"> <i class="fa fa-print" ></i>  طباعه  </a>     </td>
                   
                  
                  
                      
                  
                      
                      
                   
                 </tr>         
              
                  @endforeach
           
                   
                   </table>
                   
                   
                   
                   
                   
                   
                   
                   
                    </div><!-- /.box-body 
                </div><!-- /.box -->
            </div>
        </div>
    </section>
  </div>
  <!-- Content page End -->
@endsection