@extends('admin.layouts.master')
@section('title')
تقارير باعداد الطلاب وفق الاحزاب المختلفه 
@endsection
@section('content')
<!-- Content page Start -->
  <div class="content-wrapper">
    <!--<section class="content-header">-->
    <!--  <h1>-->
    <!--    <i class="fa fa-arrow-left"></i>-->
    <!--    <span class="semi-bold">Dashboard</span>-->
    <!--    <small>Control panel</small>-->
    <!--  </h1>-->
    <!--  <ol class="breadcrumb">-->
    <!--    <li><a href="#"><i class="fa fa-home"></i> Home</a></li>-->
    <!--    <li class="active">Dashboard</li>-->
    <!--  </ol>-->
    <!--</section>-->-->
    <section class="content">
        <div class="row">
            <div class="col-md-10 col-md-offset-1">
                <div class="box box-warning">
					<div class="box-header with-border">
						<h3 class="box-title"><span class="semi-bold"> باعداد الطلاب وفق الاحزاب المختلفه</span></h3>
						<div class="box-tools pull-right">
							<a class="btn btn-box-tool" data-widget="collapse" data-toggle="tooltip" title="Collapse"><i class="fa fa-chevron-down"></i></a>
							<a class="btn btn-box-tool"><i class="fa fa-repeat"></i></a>
							<a class="btn btn-box-tool"><i class="fa fa-cog"></i></a>
							<a class="btn btn-box-tool" data-widget="remove" data-toggle="tooltip" title="Remove"><i class="fa fa-times"></i></a>
						</div>
					</div>
				
                   
                    <div class="box-body">
                    <table id="tables" class="display dataTable no-footer dtr-inline" style="width:100%">
                        <thead>
                           <tr>
                            <th class="num">#</th>
                        
                      <th> اسم المركز</th>
                      <th>الاجمالي</th>
                      <th   colspan="2" > 60</th>
                      <th   colspan="2"  >59</th>
                      <th   colspan="2"  >58</th>
                      <th   colspan="2"  >57</th>
                      <th   colspan="2"    >56</th>
                      <th   colspan="2"   >55</th>
            
                    
                           
                        </tr>

                         <tr class="tr-head">
                                <th>الترتيب</th>
                             <th> اسم المركز</th>
                            <th>الاجمالي</th>
                          <th>  بنات</th>
                          <th>بنين</th>
                          
                          <th>  بنات</th>
                          <th>بنين</th>
                          
                          <th>  بنات</th>
                          <th>بنين</th>
                          
                          <th>  بنات</th>
                          <th>بنين</th>
                        
                          <th>  بنات</th>
                          <th>بنين</th>
                          
                          <th>  بنات</th>
                          <th>بنين</th
                  
                            </tr>
                        </thead>
                        <tbody>
                           @foreach($students as $student)
                  
                    
                        <tr>
                       <td class="num">{{ $loop->iteration }}</td>
                        
                         <td>     {{ isset($student->center->center_name) ?  $student->center->center_name : ''}}    </td>
                         
                            <td>     {{ isset($student->part->total) ?  $student->part->total : ''}}    </td>
                      
                          <td>     {{$student->where('part_id',1)->where('gender','أنثى')->count()}} </td>
                      
                      
                       
                          
                          <td>    {{$student->where('part_id',2)->where('gender','أنثى')->count()}} </td>
                         
                      
                     
                        <td>  {{$student->where('part_id',1)->where('gender','ذكر')->count()}}  </td>
                     
                   
                         
     
                         
                         
                         
                   
                     <td>
                         
                              {{$student->where('part_id',3)->where('gender','اأنثى')->count()}} </td>
                         
                    
                     <td>
                         
                                     {{$student->where('part_id',3)->where('gender','ذكر')->count()}}    </td>
                         
                         
                  
                   
                          <td>   {{$student->where('part_id',4)->where('gender','اأنثى')->count()}}     </td>
                  
                   
                         
                          <td>    {{$student->where('part_id',4)->where('gender','ذكر')->count()}}     </td>
                         
                  
                   
                         
                      <td>     {{$student->where('part_id',5)->where('gender','اأنثى')->count()}}       </td>
                         
                         
               
                     
                  
                      <td>      
                         {{$student->where('part_id',5)->where('gender','ذكر')->count()}}      </td>
                         
                  
                     
                     <td>
                           {{$student->where('part_id',6)->where('gender','اأنثى')->count()}}     </td>
                         
                  
                     
               
                     <td>
                         
                         
                           {{$student->where('part_id',7)->where('gender','اأنثى')->count()}}    
                         
                     </td>
                     
                    <td>
                        
                          {{$student->where('part_id',7)->where('gender','ذكر')->count()}}    
                        
                    </td>


                    
                        </tr>
                        @endforeach
                        </tbody>
                    </table>
                    </div><!-- /.box-body -->
                </div><!-- /.box -->
            </div>
        </div>
    </section>
  </div>
  <!-- Content page End -->
@endsection