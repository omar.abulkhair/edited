@extends('admin.layouts.master')
@section('title')
تقارير باايام عمل المعلمين
@endsection
@section('content')
<!-- Content page Start -->
<div class="content-wrapper">
    <section class="content-header">
    </section>
    <section class="content">
        <div class="row">
            <div class="col-md-10 col-md-offset-1">
                <div class="box box-warning">
					<div class="box-header with-border">
						<h3 class="box-title"><span class="semi-bold"> باايام عمل المعلمين</span></h3>
						<div class="box-tools pull-right">
							<a class="btn btn-box-tool" data-widget="collapse" data-toggle="tooltip" title="Collapse"><i class="fa fa-chevron-down"></i></a>
							<a class="btn btn-box-tool"><i class="fa fa-repeat"></i></a>
							<a class="btn btn-box-tool"><i class="fa fa-cog"></i></a>
							<a class="btn btn-box-tool" data-widget="remove" data-toggle="tooltip" title="Remove"><i class="fa fa-times"></i></a>
						</div>
					</div>
                    <div class="box-body">
                     
                        <table id="tables" class="display dataTable no-footer dtr-inline" style="width:100%">
                            <thead>
                                
                                
                            <tr>
                                <th class="num">#</th>
                                <th>المدرس</th>
                                <th>االايام التي يعمل بها</th>
                                <th>المراكز التي يعمل بها</th>
                                <th>الايام التي لا يعمل بها</th>
                                <th>نوع حلقة </th>
                            </tr>
                            
                           <tr class="tr-head">
                                <th>الترتيب</th>
                              <th>المدرس</th>
                                <th>االايام التي يعمل بها</th>
                                <th>المراكز التي يعمل بها</th>
                                <th>الايام التي لا يعمل بها</th>
                                <th>نوع حلقة </th>
                            </thead>
                            <tbody>
                                
                            
                                
                            @foreach($teachers as $teacher)
                                <tr>
                                    <td class="num">{{$loop->iteration}}</td>
                                    <td>{{$teacher->teacher_name}}    </td>
                                    <td>{{$teacher->first_day}}</td>
                                 <td>{{ isset($teacher->center->center_name) ? $teacher->center->center_name : ''}}</td>
                                 <td>{{$teacher->holiday}}</td>
                                 <td>{{isset($teacher->coursetype->type_name) ?  $teacher->coursetype->type_name :''}}</td>
                                </tr>
                            @endforeach
                            </tbody> 
                        </table>
                    </div>



                </div></div>
            </div>
        </div>
    </section>


</div>
  <!-- Content page End -->
@endsection

