@extends('admin.layouts.master') 
@section('title')
المستويات 
@endsection
@section('content')
<!-- Content page Start -->
@foreach($levels as $level)
<div class="content-wrapper">
<section class="content-header">
      <h1>
        <i class="fa fa-arrow-left"></i>
        <span class="semi-bold">الرئيسية</span>
        <small>بيانات المستوى</small>
      </h1>
      <ol class="breadcrumb">
        <li><a href="{{route('admin.home')}}"><i class="fa fa-home"></i> الرئيسية</a></li>
        <li><a href="{{route('admin.levels')}}"> بيانات المستويات</a></li>
        <li class="active">تعديل</li>
      </ol>
    </section>
	<section class="content">
		<div class="row">
			<div class="col-md-8 col-md-offset-2">
				<div class="box box-warning">
					<div class="box-header with-border">
						<h3 class="box-title"><span class="semi-bold">تعديل بيانات المستوى</span></h3>
						<div class="box-tools pull-right">
							<a class="btn btn-box-tool" data-widget="collapse" data-toggle="tooltip" title="Collapse"><i class="fa fa-chevron-down"></i></a>
							<a class="btn btn-box-tool"><i class="fa fa-repeat"></i></a>
							<a class="btn btn-box-tool"><i class="fa fa-cog"></i></a>
							<a class="btn btn-box-tool" data-widget="remove" data-toggle="tooltip" title="Remove"><i class="fa fa-times"></i></a>
						</div>
					</div>
					<div class="box-body">
						<form class="mtb-15" action="{{route('admin.levels.edit' , ['id' => $level->id])}}" enctype="multipart/form-data" method="post" onsubmit="return false;">{{ csrf_field() }}
							<div class="row form-row">
								<div class="col-md-6">
									<div class="form-group pmd-textfield pmd-textfield-floating-label">
										<label class="control-label">اسم المستوى</label>
										<input value="{{$level->level_name}}" name="name" class="form-control" id="input-1" type="text">
									</div>
								</div>
								<div class="col-md-6">
									<div class="form-group pmd-textfield pmd-textfield-floating-label">
										<label class="control-label">نوع الحلقة</label>
										<select name="coursetype_id" class="select2 pmd-select2 form-control">
											@foreach($courses as $course)
											<option value="{{$course->id}}" @if($course->id == $level->coursetype_id) selected="selected" @endif>{{$course->type_name}}</option>
											@endforeach
										</select>
									</div>
								</div>
								<div class="col-md-6">
									<div class="form-group">
										<label class="switch">
										<input type="checkbox" id="togBtn" name="active" @if($level->active == 1) checked @endif>
										<div class="slider round">
											<span class="on">نشط</span>
											<span class="off">غير نشط</span>
										</div>
										</label>
									</div>
								</div>
								<div class="col-md-12">
									<br> <a href="{{route('admin.levels')}}" class="btn btn-primary btn-orange pmd-ripple-effect btn-sm"> الغاء</a>
									<button type="submit" class="btn btn-blue btn-blue addButton pmd-ripple-effect btn-sm">حفظ</button>
								</div>
							</div>
						</form>
					</div>
				</div>
			</div>
	</section>
</div>
@endforeach
<!-- Content page End -->
@endsection