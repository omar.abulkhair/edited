<!DOCTYPE html>
<html lang="ar">
    <head>
        <!-- Meta Tags
        ========================== -->
        <meta charset="utf-8" />
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta content="width=device-width, initial-scale=1" name="viewport" />
        <meta content="Preview page of Metronic Admin Theme #2 for form layouts" name="description" />
        <meta content="" name="author" />
        <meta name="csrf_token" content="{{csrf_token()}}">


        <!-- Site Title
        ========================== -->
        <title>بطاقة تعريفية : {{ isset($teacher->teacher_name) ?  $teacher->teacher_name  :''}}</title>
        <!-- Favicon
        ===========================-->
        <link rel="shortcut icon" href="{{asset('assets/admin/img/logo-mini.png')}}">
        <link rel="stylesheet" src="{{asset('assets/admin/PrintArea.css')}}">
        <link rel="stylesheet" src="{{asset('assets/admin/jquery-ui-1.10.4.custom.css')}}">

        <link rel="stylesheet" href="{{asset('assets/admin/css/bootstrap.css')}}">
        <link rel="stylesheet" href="{{asset('assets/admin/css/font-awesome.css')}}">
        
        <link rel="stylesheet" href="{{asset('assets/admin/css/style.css')}}" id="stylesheet">
    </head>
    <body class="sidebar-mini">
        <div class="wrapper">
            <header class="main-header">
                <!-- Logo -->
                <a href="index2.html" class="logo">
                    <img src="{{asset('assets/admin/img/logo-mini.png')}}" class="logo-mini" alt="">
                    <img src="{{asset('assets/admin/img/logo.png')}}" class="logo-lg" alt="">
                </a>

                <nav class="navbar navbar-static-top" role="navigation">
                    <a href="#" class="sidebar-toggle" data-toggle="offcanvas" role="button">
                    <span class="sr-only">Toggle navigation</span>
                    </a>

                    <div class="navbar-custom-menu">
                    <ul class="nav navbar-nav">

                        <li class="dropdown user user-menu">
                        <a href="#" class="dropdown-toggle" data-toggle="dropdown">
                            <img src="{{asset('storage/uploads/users').'/'.Auth::guard('admins')->user()->image}}" class="user-image" alt="">
                            <span class="hidden-xs">{{Auth::guard('admins')->user()->name}}</span>
                        </a>
                        <ul class="dropdown-menu">
                            <li class="user-header">
                            <img src="{{asset('storage/uploads/users').'/'.Auth::guard('admins')->user()->image}}" class="img-circle" alt="">
                            <p>{{Auth::guard('admins')->user()->name}}</p>
                            </li>
                            <li class="user-footer">
                            <div class="pull-left"><a href="{{route('admin.profile')}}" class="btn btn-flat">الصفحة الشخصية</a></div>
                            <div class="pull-right"><a href="{{route('admin.logout')}}" class="btn btn-flat">تسجيل الخروج</a></div>
                            </li>
                        </ul>
                        </li>

                        <li class="dropdown settings-menu">
                        <a href="#" class="dropdown-toggle" data-toggle="dropdown">
                            <i class="fa fa-cog" data-toggle="tooltip" data-placement="left" title="Settings"></i>
                        </a>
                        <ul class="dropdown-menu">
                            <li><a href="{{route('admin.profile')}}"><i class="fa fa-user"></i>&nbsp;&nbsp;My Profile</a></li>
                            <li><a href="#" id="fullscreen"><i class="fa fa-arrows"></i>&nbsp;&nbsp;Full Screen</a></li>
                            <li><a href="{{route('admin.lock')}}" id="fullscreen"><i class="fa fa-lock"></i>&nbsp;&nbsp;Lock Screen</a></li>
                            <li class="divider"></li>
                            <li><a href="#"><i class="fa fa-sign-out"></i>&nbsp;&nbsp;Log Out</a></li>
                        </ul>
                        </li>
                    </ul>
                    </div>
                </nav>
            </header>

            <aside class="main-sidebar">
                <section class="sidebar">
                  <div class="user-panel">
                    <div class="pull-left image"><img src="{{asset('storage/uploads/users').'/'.Auth::guard('members')->user()->image}}" class="img-circle" alt=""></div>
                    <div class="pull-left info">
                      <h3>مرحباً</h3>
                      <p>{{Auth::guard('members')->user()->guardian_name}}</p>
                      <a href="{{route('site.profile')}}">الحالة <i class="fa fa-circle-o text-success"></i> أونلاين</a>
                    </div>
                  </div>
                  <ul class="sidebar-menu">
                    <li class="@if(Route::currentRouteName()=='site.home') active @endif">
                      <a href="{{route('site.home')}}">
                        <i class="fa fa-home"></i>
                        <span>لوحة التحكم</span>
                      </a>
                    </li>
                    <li class="treeview @if(Route::currentRouteName()=='site.students') active @elseif(Route::currentRouteName()=='site.students.add') active @endif">
                      <a href="#">
                        <i class="fas fa-graduation-cap"></i>
                        <span>الطلاب</span>
                        <i class="fas fa-angle-left pull-right"></i>
                      </a>
                      <ul class="treeview-menu">
                        <li><a href="{{route('site.students')}}"><i class="fas fa-bullseye"></i> عرض الطلاب</a></li>
                        <li><a href="{{route('site.students.add')}}"><i class="fas fa-bullseye"></i> اضافة طالب</a></li>
                      </ul>
                    </li>
                  </ul>
                  <div class="footer-widget">
                    <div class="progress transparent progress-small no-radius no-margin">
                      <div data-percentage="79%" class="progress-bar progress-bar-success animate-progress-bar" style="width: 79%;"></div>
                    </div>
                    <div class="pull-right">
                      <div class="details-status"> <span data-animation-duration="560" data-value="86" class="animate-number">86</span>% </div>
                      <a href="{{route('site.lock')}}"><i class="fa fa-power-off"></i></a></div>
                  </div>
              
              
                </section>
                <!-- Sidebar End -->
              </aside>
            <!-- Content page Start -->
            <div class="content-wrapper">
                <section class="content-header">
                    <h1>
                        <i class="fa fa-arrow-left"></i>
                        <span class="semi-bold">الرئيسية</span>
                        <small>بيانات الموظف</small>
                    </h1>
                    <ol class="breadcrumb">
                        <li><a href="{{route('admin.home')}}"><i class="fa fa-home"></i> الرئيسية</a></li>
                        <li><a href="{{route('admin.teachers')}}"> بيانات الموظفين</a></li>
                        <li class="active">بطاقة تعريفية</li>
                    </ol>
                </section>
                <section class="content">
                    <div class="row">
                        <div class="col-md-4 col-md-offset-4">
                            <div class="box box-warning">
                                <div class="box-header with-border">
                                    <h3 class="box-title"><span class="semi-bold">بطاقة تعريفية</span></h3>
                                    <div class="box-tools pull-right">
                                        <a class="btn btn-box-tool" data-widget="collapse" data-toggle="tooltip" title="Collapse"><i class="fa fa-chevron-down"></i></a>
                                        <a class="btn btn-box-tool"><i class="fa fa-repeat"></i></a>
                                        <a class="btn btn-box-tool"><i class="fa fa-cog"></i></a>
                                        <a class="btn btn-box-tool" data-widget="remove" data-toggle="tooltip" title="Remove"><i class="fa fa-times"></i></a>
                                    </div>
                                </div>
                                <div class="box-body" id="PrintArea1">
                                    <img class="img-PrintArea1" src="{{asset('storage/uploads/teachers').'/'.$teacher->image}}">
                                    <div class="info-PrintArea1"  dir="rtl">
                                        <p>{{isset($teacher->teacher_name) ? $teacher->teacher_name   :''}}</p>
                                        <p>{{ isset($teacher->address) ? $teacher->address:''}}</p>
                                        <p>{{ isset($teacher->email) ?$teacher->email :''}}</p>
                                        <p>{{isset($teacher->phone)  ?$teacher->phone :''}}</p>
                                        <p>{{isset($teacher->national_id) ? $teacher->national_id:''}}</p>
                                        {!! DNS1D::getBarcodeHTML($teacher->code, 'C128A',1.2,33) !!}
                                    </div>
                                </div><!-- /.box-body -->
                                <div class="box-footer">
                                    <a class="btn btn-blue pmd-ripple-effect btn-sm" href="javascript:getScreenShot()">
                                        تحميل
                                    </a>
                                    <a id="test">
                                    </a>
								</div>
                            </div><!-- /.box -->
                        </div>
                    </div>

                    <div class="row">
                        <div class="col-md-4 col-md-offset-4">
                            <div class="box box-warning">
                                <div class="box-body" id="PrintArea2">
                                </div><!-- /.box-body -->
                            </div><!-- /.box -->
                        </div>
                    </div>
                </section>
            </div>
            <!-- Content page End -->
        </div>
        
        <script src="{{asset('assets/admin/js/jQuery-2.1.4.min.js')}}"></script>
        <script src="{{asset('assets/admin/js/jquery-ui.min.js')}}"></script>
        <script src="{{asset('assets/admin/js/bootstrap.min.js')}}"></script>
        
        <!-- fullscreen -->
        <script src="{{asset('assets/admin/js/screenfull.js')}}"></script>
        <!-- text-rotator -->
        <script src="{{asset('assets/admin/js/morphext.js')}}"></script>
        <!-- Tanseeq App -->
        <script src="{{asset('assets/admin/js/app.min.js')}}"></script>
        <script src="{{asset('assets/admin/plugins/nicescroll/jquery.nicescroll.min.js')}}"></script>
        <script type="text/javascript">
          /* Nice Scroll
          ===============================*/
          $(document).ready(function () {
              
              "use strict";
              
              $("html").niceScroll({
                  scrollspeed: 60,
                  mousescrollstep: 35,
                  cursorwidth: 5,
                  cursorcolor: 'rgba(243, 131, 78, 0.7)',
                  cursorborder: 'none',
                  background: 'rgba(27, 30, 36, 0.0)',
                  cursorborderradius: 3,
                  autohidemode: false,
                  cursoropacitymin: 0.1,
                  cursoropacitymax: 1,
                  zindex: "999",
                  horizrailenabled: false
              });
            
          });
        </script>
        <script src="{{asset('assets/admin/js/html2canvas.js')}}"></script>
        <script src="{{asset('assets/admin/js/html2canvas.min.js')}}"></script>
        <script>
            function getScreenShot(){
                html2canvas(document.getElementById("PrintArea1")).then(function(canvas) {
                    document.getElementById("PrintArea2").appendChild(canvas);
                    $('#test').attr('href', canvas.toDataURL("image/png"));
                    $('#test').attr('download','Test file.png');
                    $('#test')[0].click();
                });
            }
        </script>
    </body>
</html>
