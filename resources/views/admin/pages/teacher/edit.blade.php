@extends('admin.layouts.master') 
@section('title')
الموظفين 
@endsection
@section('content')
<!-- Content page Start -->
<div class="content-wrapper">
<section class="content-header">
      <h1>
        <i class="fa fa-arrow-left"></i>
        <span class="semi-bold">الرئيسية</span>
        <small>بيانات الموظف</small>
      </h1>
      <ol class="breadcrumb">
        <li><a href="{{route('admin.home')}}"><i class="fa fa-home"></i> الرئيسية</a></li>
        <li><a href="{{route('admin.teachers')}}"> بيانات الموظفين</a></li>
        <li class="active">تعديل</li>
      </ol>
    </section>
	<section class="content">
		<div class="row">
			<div class="col-md-8 col-md-offset-2">
			<div class="box box-warning">
					<div class="box-header with-border">
						<h3 class="box-title"><span class="semi-bold">تعديل بيانات الموظف</span></h3>
						<div class="box-tools pull-right">
							<a class="btn btn-box-tool" data-widget="collapse" data-toggle="tooltip" title="Collapse"><i class="fa fa-chevron-down"></i></a>
							<a class="btn btn-box-tool"><i class="fa fa-repeat"></i></a>
							<a class="btn btn-box-tool"><i class="fa fa-cog"></i></a>
							<a class="btn btn-box-tool" data-widget="remove" data-toggle="tooltip" title="Remove"><i class="fa fa-times"></i></a>
						</div>
					</div>
					<div class="box-body">
						@foreach($teachers as $teacher)
							<form class="mtb-15" action="{{route('admin.teachers.edit' , ['id' => $teacher->id])}}" enctype="multipart/form-data" method="post" onsubmit="return false;">
								{{ csrf_field() }}
							<div class="row form-row">
								<div class="col-md-12">
									<div class="user-img-upload">
										<div class="fileUpload user-editimg">
											<span><i class="fa fa-camera"></i> تغيير</span>
											<input type='file' id="imgInp" class="upload" name="image" value="{{$teacher->image}}"/>
											<input type="hidden" value="teachers" name="storage" >
										</div>
										<img src="{{asset('storage/uploads/teachers').'/'.$teacher->image}}" id="blah" class="img-circle" alt="">
										<p id="result"></p>
										<br>
									</div>
								</div>
								<div class="col-md-6">
									<div class="form-group pmd-textfield pmd-textfield-floating-label">
										<label class="control-label">اسم الموظف</label>
										<input value="{{$teacher->teacher_name}}" name="name" class="form-control" type="text">
									</div>
								</div>
								<div class="col-md-6">
									<div class="form-group pmd-textfield pmd-textfield-floating-label">
										<label>  الوظيفة</label>
										<select name="job" class="form-control pmd-select2 select2">
										<option value="{{$teacher->job}}" selected>{{$teacher->job_name}}</option>
										@foreach($jobs as $job)
											<option value="{{$job->id}}">{{$job->job_name}}</option>
										@endforeach
										</select>
									</div>
								</div>
								<div class="col-md-6">
									<div class="form-group pmd-textfield pmd-textfield-floating-label">
										<label class="control-label">الراتب الأساسى</label>
										<input name="salary" value="{{$teacher->salary}}" type="number" class="form-control">
									</div>
								</div>
								<div class="col-md-6">
									<div class="form-group pmd-textfield pmd-textfield-floating-label">
										<label class="control-label">البريد الالكترونى</label>
										<input value="{{$teacher->email}}" name="email" class="form-control" type="text">
									</div>
								</div>
								<div class="col-md-6">
									<div class="form-group pmd-textfield pmd-textfield-floating-label">
										<label class="control-label">عدد ساعات العمل اليومية</label>
										<input value="{{$teacher->hours}}" name="hours" class="form-control" type="number">
									</div>
								</div>
								<div class="col-md-6">
									<div class="form-group pmd-textfield pmd-textfield-floating-label">
										<label class="control-label">رقم الهاتف</label>
										<input name="phone" id="input-3" value="{{$teacher->phone}}" type="text" class="form-control">
									</div>
								</div>
								<div class="col-md-6">
									<div class="form-group pmd-textfield pmd-textfield-floating-label">
										<label class="control-label">تاريخ الميلاد</label>
										<input value="{{$teacher->birth}}" name="birth" type="text" class="form-control datepicker">
									</div>
								</div>
                				<div class="col-md-6">
									<div class="form-group pmd-textfield pmd-textfield-floating-label">
										<label class="control-label">يوم التوظيف</label>
										<input name="first_day" value="{{$teacher->first_day}}" type="text" class="form-control datepicker">
									</div>
								</div>
								<div class="col-md-6">
									<div class="form-group pmd-textfield pmd-textfield-floating-label">
										<label class="control-label">الرقم المدنى</label>
										<input  value="{{$teacher->national_id}}" name="national_id" class="form-control" type="text">
									</div>
								</div>
								<div class="col-md-6">
									<div class="form-group">
										<label class="switch">
										<input type="checkbox" id="togBtn" name="active" @if($teacher->active == 1) checked @endif>
										<div class="slider round">
											<span class="on">نشط</span>
											<span class="off">غير نشط</span>
										</div>
										</label>
									</div>
								</div>
								<div class="col-md-12">
									<div class="form-group pmd-textfield pmd-textfield-floating-label">
										<label class="control-label">عنوان الموظف</label>
										<textarea name="address" class="form-control"type="text">{{$teacher->address}}</textarea>
									</div>
								</div>



								<div class="row">








									<div class="col-md-3">
										<div class="form-group pmd-textfield pmd-textfield-floating-label">
											<label> المحافظة</label>
											<select name="id" class="form-control pmd-select2 select2" id="add_town">
												<option></option>
												@foreach(\App\Town::all() as $town)
													<option value="{{$town->id}}" selected>{{$town->town_name}}</option>
												@endforeach
											</select>
										</div>
									</div>

									<div class="col-md-3">
										<div class="form-group pmd-textfield pmd-textfield-floating-label">
											<label>المركز</label>
											<select name="center_id" class="form-control pmd-select2 select2" id="add_center">
												@foreach(\App\Center::where("active", 1)->get() as $center)
													@if($center->id == $student->center_id)
														<option value="{{$center->id}}" selected>{{$center->center_name}}</option>
													@endif
												@endforeach
											</select>
										</div>
									</div>


									<div class="col-md-3">
										<div class="form-group pmd-textfield pmd-textfield-floating-label">
											<label>نوع الحلقة</label>
											<select name="coursetype_id" class="form-control pmd-select2 select2" id="add_type">
												@foreach($types as $type)
													@if($type->id == $student->coursetype_id)
														<option value="{{$type->id}}" selected>{{$type->type_name}}</option>
													@endif
												@endforeach
											</select>
										</div>
									</div>



									<div class="col-md-3">
										<div class="form-group pmd-textfield pmd-textfield-floating-label">
											<label>الحلقة</label>
											<select class="form-control pmd-select2 select2" id="add_course" name="course_id">
												@foreach(\App\Course::where("active", 1)->get() as $type)
													@if($type->id == $student->course_id)
														<option value="{{$type->id}}" selected>{{$type->course_name}}</option>
													@endif
												@endforeach
											</select>
										</div>
									</div>















								</div>




								<div class="col-md-12">
									<br> <a href="{{route('admin.teachers')}}" class="btn btn-orange pmd-ripple-effect btn-sm"> الغاء</a>
									<button type="submit" class="btn btn-blue addButton pmd-ripple-effect btn-sm">حفظ</button>
								</div>
							</div>
						</form>
					</div>
          		@endforeach
				</div>
			</div>
			<div class="row">
			<div class="col-md-8 col-md-offset-2">
			<div class="col-md-6">
				<div class="box box-warning">
					<div class="box-header with-border">
						<h3 class="box-title"><span class="semi-bold">المستويات</span></h3>
						<div class="box-tools pull-right">
							<a class="btn btn-box-tool" data-widget="collapse" data-toggle="tooltip" title="Collapse"><i class="fa fa-chevron-down"></i></a>
							<a class="btn btn-box-tool"><i class="fa fa-repeat"></i></a>
							<a class="btn btn-box-tool"><i class="fa fa-cog"></i></a>
							<a class="btn btn-box-tool" data-widget="remove" data-toggle="tooltip" title="Remove"><i class="fa fa-times"></i></a>
						</div>
					</div>
					<div class="box-body">
									<table class="table table-bordered table-striped">
										<thead>
										<tr>
											<th>الترتيب</th>
											<th>المستوى</th>
											<th>العمليات</th>
										</tr>
										</thead>
										<tbody>
										@foreach($tlevels as $tlevel)
										<tr>
											<td>{{$loop->index + 1}}</td>
											<td>{{$tlevel->level_name}}</td>
											<td>
											<button class="btn btn-orange btndelet pmd-ripple-effect btn-sm" data-url="{{ route('admin.tlevel.delete' , ['id' => $tlevel->id]) }}" >
												حذف
											</button>
											</td>
										</tr>
										@endforeach
										</tbody>
									</table>
								</div>
					</div>
				</div>
				<div class="col-md-6">
				<div class="box box-warning">
					<div class="box-header with-border">
						<h3 class="box-title"><span class="semi-bold">الدروس</span></h3>
						<div class="box-tools pull-right">
							<a class="btn btn-box-tool" data-widget="collapse" data-toggle="tooltip" title="Collapse"><i class="fa fa-chevron-down"></i></a>
							<a class="btn btn-box-tool"><i class="fa fa-repeat"></i></a>
							<a class="btn btn-box-tool"><i class="fa fa-cog"></i></a>
							<a class="btn btn-box-tool" data-widget="remove" data-toggle="tooltip" title="Remove"><i class="fa fa-times"></i></a>
						</div>
					</div>
					<div class="box-body">
									<table class="table table-bordered table-striped">
										<thead>
										<tr>
											<th>الترتيب</th>
											<th>الدرس</th>
											<th>العمليات</th>
										</tr>
										</thead>
										<tbody>
										@foreach($mats as $mat)
										<tr>
											<td>{{$loop->index + 1}}</td>
											<td>{{$mat->material_name}}</td>
											<td>
											<button class="btn btn-orange btndelet pmd-ripple-effect btn-sm" data-url="{{ route('admin.tmaterial.delete' , ['id' => $mat->id]) }}" >
												حذف
											</button>
											</td>
										</tr>
										@endforeach
										</tbody>
									</table>
							</div>
					</div>
				</div>
				</div>
			</div>
	</section>
</div>
<!-- Content page End -->
@endsection