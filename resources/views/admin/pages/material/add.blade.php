@extends('admin.layouts.master') 
@section('title')
الدروس 
@endsection
@section('content')
<!-- Content page Start -->
<div class="content-wrapper">
<section class="content-header">
      <h1>
        <i class="fa fa-arrow-left"></i>
        <span class="semi-bold">الرئيسية</span>
        <small>بيانات الدرس</small>
      </h1>
      <ol class="breadcrumb">
        <li><a href="{{route('admin.home')}}"><i class="fa fa-home"></i> الرئيسية</a></li>
        <li><a href="{{route('admin.materials')}}"> بيانات الدروس</a></li>
        <li class="active">اضافة</li>
      </ol>
    </section>
	<section class="content">
		<div class="row">
			<div class="col-md-8 col-md-offset-2">
			<div class="box box-warning">
					<div class="box-header with-border">
						<h3 class="box-title"><span class="semi-bold">اضافة درس جديد</span></h3>
						<div class="box-tools pull-right">
							<a class="btn btn-box-tool" data-widget="collapse" data-toggle="tooltip" title="Collapse"><i class="fa fa-chevron-down"></i></a>
							<a class="btn btn-box-tool"><i class="fa fa-repeat"></i></a>
							<a class="btn btn-box-tool"><i class="fa fa-cog"></i></a>
							<a class="btn btn-box-tool" data-widget="remove" data-toggle="tooltip" title="Remove"><i class="fa fa-times"></i></a>
						</div>
					</div>
					<div class="box-body">
						<form class="mtb-15" action="{{route('admin.materials.add')}}" enctype="multipart/form-data" method="post" onsubmit="return false;">{{ csrf_field() }}
							<div class="row form-row">
								<div class="col-md-6">
									<div class="form-group">
										<label class="switch">
										<input type="checkbox" id="togBtn" name="active" checked>
										<div class="slider round">
											<span class="on">نشط</span>
											<span class="off">غير نشط</span>
										</div>
										</label>
									</div>
									<div class="form-group pmd-textfield pmd-textfield-floating-label">
										<label class="control-label">اسم الدرس</label>
										<input name="name" class="form-control" type="text">
									</div>
									
									
									<div class="form-group pmd-textfield pmd-textfield-floating-label">
										<label class="control-label">التفاصيل </label>
										<input name="details" class="form-control" type="text">
									</div>
								
									<div class="form-group pmd-textfield pmd-textfield-floating-label">
										<label class="control-label">درجة النجاح</label>
										<input name="success" class="form-control" type="number">
									</div>
									
									
									
									
									
									<div class="form-group pmd-textfield pmd-textfield-floating-label">
										<label for="select-4">المستوى</label>
										<select name="level_id" class="select2 pmd-select2 form-control">
											<option></option>
											@foreach($levels as $level)
												<option value="{{$level->id}}">{{$level->level_name}}</option>
											@endforeach
										</select>
									</div>
								</div>
								
								<div class="col-md-6"><br>
								<table id="example1" class="table table-bordered table-striped">
									<thead>
									<tr>
										<th>التقدير</th>
										<th>النسبة</th>
									</tr>
									</thead>
									<tbody>
									<tr>
									<td>امتياز</td>
									<td><input name="p1" type="text" size="3">   %</td>
									</tr>
									<tr>
									<td>جيد جدا</td>
									<td><input name="p2" type="text" size="3">   %</td>
									</tr>
									<tr>
									<td>جيد</td>
									<td><input name="p3" type="text" size="3">   %</td>
									</tr>
									<tr>
									<td>مقبول</td>
									<td><input name="p4" type="text" size="3">   %</td>
									</tr>
									<tr>
									<td>ضعيف  (<small>أقل من</small>)</td>
									<td><input name="p5" type="text" size="3">   %</td>
									</tr>
									</tfoot>
								</table>
								</div>
								<div class="col-md-12">
									<br> <a href="{{route('admin.materials')}}" class="btn btn-primary btn-orange pmd-ripple-effect btn-sm"> الغاء</a>
									<button type="submit" class="btn btn-blue btn-blue addButton pmd-ripple-effect btn-sm">حفظ</button>
								</div>
							</div>
						</form>
					</div>
				</div>
			</div>
		</div>
	</section>
</div>
<!-- Content page End -->
@endsection