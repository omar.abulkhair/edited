@extends('admin.layouts.master')
@section('title')
الطلاب
@endsection
@section('content')
<!-- Content page Start -->
<div class="content-wrapper">
    <section class="content-header">
    </section>
    <section class="content">
        <div class="row">
            <div class="col-md-12">
                <div class="box box-warning">
					<div class="box-header with-border">
						<h3 class="box-title"><span class="semi-bold">الطلاب</span></h3>
						<div class="box-tools pull-right">
							<a class="btn btn-box-tool" data-widget="collapse" data-toggle="tooltip" title="Collapse"><i class="fa fa-chevron-down"></i></a>
							<a class="btn btn-box-tool"><i class="fa fa-repeat"></i></a>
							<a class="btn btn-box-tool"><i class="fa fa-cog"></i></a>
							<a class="btn btn-box-tool" data-widget="remove" data-toggle="tooltip" title="Remove"><i class="fa fa-times"></i></a>
						</div>
					</div>
                    <div class="box-body">
                        <table id="tables" class="display" style="width:100%">
                            <thead>
                                <tr>
                                    <th class="num">#</th>
                                    <th>الاسم</th>
                                    <th>الرقم المدنى</th>
                                    <th>السن</th>
                                    <th>الفصل الدراسي</th>
                                    <th>المركز</th>
                                    <th>النوع</th>
                                    <th>الجنسية</th>
                                    <th>العمليات</th>
                                </tr>
                                <tr class="tr-head">
                                    <th> </th>
                                    <th>الاسم</th>
                                    <th>الرقم المدنى</th>
                                    <th>السن</th>
                                    <th>الفصل الدراسي</th>
                                    <th>المركز</th>
                                    <th>النوع</th>
                                    <th>الجنسية</th>
                                    <th>العمليات</th>
                                </tr>
                            </thead>
                            <tbody>
                            @foreach($students as $student)
                                <tr>
                                    <td class="num"></td>
                                    <td>{{$student->student_name}}</td>
                                    <td>{{$student->national_id}}</td>
                                    <td>{{$student->age}}</td>
                                    <td>{{$student->season_name}}</td>
                                    <td>{{$student->center_name}}</td>
                                    <td>{{$student->gender}}</td>
                                    <td>{{$student->nationality}}</td>
                                    <td  class="action">
                                        <!--<a href="{{ route('admin.students.edit' , ['id' => $student->id]) }}" class="btn btn-orange">عرض</a>-->
                                        <a href="{{ route('admin.students.count' , ['id' => $student->id]) }}"><i class="fa fa-calculator" title="الحسابات"></i></a>
                                        <a href="{{ route('admin.students.edit' , ['id' => $student->id]) }}" title="تعديل"><i class="fa fa-edit"></i></a>
                                        
                                        <a class="btndelet" href="{{ route('admin.students.delete' , ['id' => $student->id]) }}"  title="حذف">
                                        <i class="fa fa-trash"></i>
                                        </a>                                        
                                    </td>
                                </tr>
                            @endforeach
                            </tbody>
                            
                        </table>
                    </div>



                </div>
            </div>
        </div>
    </section>


</div>
  <!-- Content page End -->
@endsection

