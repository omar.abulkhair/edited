@extends('admin.layouts.master') 
@section('title')
المراكز 
@endsection
@section('content')
<!-- Content page Start -->
<div class="content-wrapper">
<section class="content-header">
      <h1>
        <i class="fa fa-arrow-left"></i>
        <span class="semi-bold">الرئيسية</span>
        <small>بيانات المركز</small>
      </h1>
      <ol class="breadcrumb">
        <li><a href="{{route('admin.home')}}"><i class="fa fa-home"></i> الرئيسية</a></li>
        <li><a href="{{route('admin.centers')}}"> بيانات المراكز</a></li>
        <li class="active">تعديل</li>
      </ol>
    </section>
	<section class="content">
		<div class="row">
			<div class="col-md-8 col-md-offset-2">
				<div class="box box-warning">
					<div class="box-header with-border">
						<h3 class="box-title"><span class="semi-bold">تعديل بيانات المركز</span></h3>
						<div class="box-tools pull-right">
							<a class="btn btn-box-tool" data-widget="collapse" data-toggle="tooltip" title="Collapse"><i class="fa fa-chevron-down"></i></a>
							<a class="btn btn-box-tool"><i class="fa fa-repeat"></i></a>
							<a class="btn btn-box-tool"><i class="fa fa-cog"></i></a>
							<a class="btn btn-box-tool" data-widget="remove" data-toggle="tooltip" title="Remove"><i class="fa fa-times"></i></a>
						</div>
					</div>
					<div class="box-body">
					   
						@foreach($centers as $center)
						
						<form class="mtb-15" action="{{route('admin.centers.edit' , ['id' => $center->id])}}" enctype="multipart/form-data" method="post" onsubmit="return false;">{{ csrf_field() }}
							<div class="row form-row">
								<div class="col-md-6">
									<div class="form-group pmd-textfield pmd-textfield-floating-label">
										<label class="control-label"">اسم المركز</label>
										<input name="name" value="{{$center->center_name}}" class="form-control" type="text">
									</div>
								</div>
								<div class="col-md-6">
									<div class="form-group pmd-textfield pmd-textfield-floating-label">
										<label> المحافظة</label>
										<select name="town_id" class="form-control pmd-select2 select2">
											<option value="{{$center->town_id}}" selected>{{$center->town_name}}</option>
										@foreach($towns as $town)
												<option value="{{$town->id}}">{{$town->town_name}}</option>
											@endforeach
										</select>
									</div>
								</div>
								<div class="col-md-6">
									<div class="form-group pmd-textfield pmd-textfield-floating-label">
										<label class="control-label">1 رقم الهاتف</label>
										<input value="{{$center->phone}}" name="phone" class="form-control" type="text">
									</div>
								</div>
								<div class="col-md-6">
									<div class="form-group pmd-textfield pmd-textfield-floating-label">
										<label class="control-label">2 رقم الهاتف</label>
										<input name="phone2" value="{{$center->phone2}}" type="text" class="form-control">
									</div>
								</div>
                                <div class="col-md-6">
									<div class="form-group pmd-textfield pmd-textfield-floating-label">
										<label class="control-label">رقم الهاتف 3</label>
										<input name="phone3" value="{{$center->phone3}}" type="text" class="form-control">
									</div>
								</div>
                                <div class="col-md-6">
									<div class="form-group pmd-textfield pmd-textfield-floating-label">
										<label class="control-label">البريد الالكترونى</label>
										<input value="{{$center->email}}" name="email" class="form-control" type="text">
									</div>
								</div>
                                
								<div class="col-md-6">
									<div class="form-group pmd-textfield pmd-textfield-floating-label">
										<label class="control-label">اسم المشرف</label>
										<input name="manager" value="{{$center->manager}}" class="form-control" type="text">
									</div>
								</div>
								<div class="col-md-6">
									<div class="form-group pmd-textfield pmd-textfield-floating-label">
										<label class="control-label"0>اسم المدير العام</label>
										<input name="head_manager" value="{{$center->head_manager}}" class="form-control"  type="text">
									</div>
								</div>
								<div class="col-md-12">
									<div class="form-group pmd-textfield pmd-textfield-floating-label">
										<label class="control-label">الموقع الالكترونى</label>
										<input value="{{$center->website}}" name="website" class="form-control" type="text">
									</div>
								</div>
								<div class="col-md-6">
									<div class="form-group pmd-textfield pmd-textfield-floating-label">
										<label class="control-label">سن القبول</label>
										<input value="{{$center->age}}" name="age" class="form-control" type="text">
									</div>
								</div>
								<div class="col-md-12">
									<div class="form-group pmd-textfield pmd-textfield-floating-label">
										<label class="control-label">عنوان المركز</label>
										<textarea name="address" class="form-control" type="text">{{$center->address}}</textarea>
									</div>
								</div>
								<div class="col-md-6">
										<div class="form-group">
											<label class="switch">
											<input type="checkbox" id="togBtn" name="active" @if($center->active == 1) checked @endif>
											<div class="slider round">
												<span class="on">نشط</span>
												<span class="off">غير نشط</span>
											</div>
											</label>
										</div>
									</div>
								<div class="col-md-12">
									<br> <a href="{{route('admin.centers')}}" class="btn btn-primary btn-orange pmd-ripple-effect btn-sm"> الغاء</a>
									<button type="submit" class="btn btn-blue btn-blue addButton pmd-ripple-effect btn-sm">حفظ</button>
								</div>
							</div>
						</form>
						@endforeach
					</div>
				</div>
			</div>
	</section>
</div>
<!-- Content page End -->
@endsection