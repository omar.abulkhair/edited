<!DOCTYPE html>
<html lang="ar" dir="rtl">
    <head>
        <!-- Meta Tags
        ========================== -->
        <meta charset="utf-8" />
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta content="width=device-width, initial-scale=1" name="viewport" />
        <meta content="Preview page of Metronic Admin Theme #2 for form layouts" name="description" />
        <meta content="" name="author" />
        <meta name="csrf_token" content="{{csrf_token()}}">


        <!-- Site Title
        ========================== -->
        <title>@yield('title')</title>
        
        <!-- Favicon
        ===========================-->
        <link rel="shortcut icon" href="{{asset('assets/admin/img/logo-mini.png')}}">

        
        <link rel="stylesheet" href="{{asset('assets/admin/css/bootstrap.css')}}">
        <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.7.1/css/all.css" integrity="sha384-fnmOCqbTlWIlj8LyTjo7mOUStjsKC4pOpQbqyi7RrhN7udi9RwhKkMHpvLbHG9Sr" crossorigin="anonymous">


        <link rel="stylesheet" href="{{asset('assets/admin/css/datatables.css')}}">
        

        <link rel="stylesheet" href="{{asset('assets/admin/plugins/daterangepicker/daterangepicker-bs3.css')}}">
        <!-- iCheck for checkboxes and radio inputs -->
        <link rel="stylesheet" href="{{asset('assets/admin/plugins/iCheck/all.css')}}">
        <!-- Bootstrap Color Picker -->
        <link rel="stylesheet" href="{{asset('assets/admin/plugins/colorpicker/bootstrap-colorpicker.min.css')}}">
        <!-- Bootstrap time Picker -->
        <link rel="stylesheet" href="{{asset('assets/admin/css/datetimepicker.css')}}">

        <!-- Select2 
        <link rel="stylesheet" href="{{asset('assets/admin/css/select2.css')}}">-->  

        

        
        <link href="{{asset('assets/admin/plugins/bootstrap-fileinput/bootstrap-fileinput.css')}}" rel="stylesheet">
        <link href="{{asset('assets/admin/css/sweetalert.css')}}" rel="stylesheet">
        <link href="{{asset('assets/admin/css/custom.css')}}" rel="stylesheet">

        <link rel="stylesheet" href="{{asset('assets/admin/css/textfield.css')}}">
        <link rel="stylesheet" href="{{asset('assets/admin/css/component.css')}}">

        <link rel="stylesheet" href="{{asset('assets/admin/css/style.css')}}" id="stylesheet">
        
        

        

        

        <!--[if lt IE 9]>
            <script src="https://oss.maxcdn.com/html5shiv/3.7.2/html5shiv.min.js"></script>
            <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
        <![endif]-->
    </head>
    <body class="sidebar-mini">
    
        <div class="wrapper">
            @include('admin.layouts.header')
            @include('admin.layouts.sidebar')
            @yield('content')
        </div>

        @yield('modals')
        @yield('templates')

        <!-- common edit modal with ajax for all project -->
        <div id="common-modal" class="modal fade" role="dialog">
                    <!-- modal -->
        </div>

        <!-- delete with ajax for all project -->
        <div id="delete-modal" class="modal fade" role="dialog">
                    <div class="modal-dialog">
                        <!-- Modal content-->
                    </div>
        </div>
        <script id="template-modal" type="text/html" >
                    <div class = "modal-content" >
                        <input type = "hidden" name = "_token" value="{{ csrf_token() }}" >
                        <div class = "modal-header" >
                            <button type = "button" class = "close" data - dismiss = "modal" > &times; </button>
                            <h4 class = "modal-title bold" >هل تريد مسح العنصر ؟</h4>
                        </div>
                        <div class = "modal-body" >
                            <p >يرجى العلم بأنه سيتم حذف العنصر بكل ما يتعلق به من بيانات</p>
                        </div>
                        <div class = "modal-footer" >
                            <a
                                href = "{url}"
                                id = "delete" class = "btn btn-danger" >
                                <li class = "fa fa-trash" > </li> مسح
                            </a>

                            <button type = "button" class = "btn btn-default" data-dismiss = "modal" >
                                <li class = "fa fa-times" > </li> أغلق</button >
                        </div>
                    </div>
        </script>
                
        @include('admin.templates.alerts')
        @include('admin.templates.delete-modal')

        <form action="#" id="csrf">{!! csrf_field() !!}</form>

        <!-- Scripts
            ========================== -->
        @if(  Route::currentRouteName() == 'admin.teachers.edit' || Route::currentRouteName() == 'admin.students.edit' ||  Route::currentRouteName() == 'admin.teachers.add' || Route::currentRouteName() == 'admin.levels.add' || Route::currentRouteName() == 'admin.guardians.edit' )
        <script src="{{asset('assets/admin/js/jQuery-2.1.4.min.js')}}"></script>
        
        
        <script src="{{asset('assets/admin/js/jquery-ui.min.js')}}"></script>
        <script src="{{asset('assets/admin/js/bootstrap.min.js')}}"></script>

        <script src="{{asset('assets/admin/js/global.js')}}"></script>

        <script src="{{asset('assets/admin/js/textfield.js')}}"></script>
        <!-- Select2 
        <script src="{{asset('assets/admin/js/select2.min.js')}}"></script>-->

        <!-- fullscreen -->
        <script src="{{asset('assets/admin/js/screenfull.js')}}"></script>

        <!-- text-rotator -->
        <script src="{{asset('assets/admin/js/morphext.js')}}"></script>

        <script src="{{asset('assets/admin/js/bootstrap-datetimepicker.js')}}"></script>
        <script>
            $(function () {
              function readURL(input) {
                  if (input.files && input.files[0]) {
                      var reader = new FileReader();
                      reader.onload = function(e) {
                          $('#blah').attr('src', e.target.result);
                      }
                      reader.readAsDataURL(input.files[0]);
                  }
              }
              $("#imgInp").change(function() {
                  readURL(this);
              });
              
              function readURL(input) {
                  if (input.files && input.files[0]) {
                      var reader = new FileReader();
                      reader.onload = function(e) {
                          $('#blah2').attr('src', e.target.result);
                      }
                      reader.readAsDataURL(input.files[0]);
                  }
              }
              $("#imgInp2").change(function() {
                  readURL(this);
              });
              /* Default date and time picker */
              $('.datetimepicker-default').datetimepicker({
              });
              $('.datepicker').datetimepicker({
                'format' : "YYYY-MM-DD",
              });
              $('.timepicker').datetimepicker({
                'format' : "LT",
              });
            });
          </script>


        
        <script src="{{asset('assets/admin/js/tab-scrollable.js')}}"></script>
        <script src="{{asset('assets/admin/js/jquery.sparkline.min.js')}}"></script>

        
        <script src="{{asset('assets/admin/js/app.min.js')}}"></script>
        
        <script>

          $(document).ready(function() {
            $('#advanced-check').change(function() {
              $('#advanced-search').toggle();
            });
          });  

        </script>


        

        <script src="{{asset('assets/admin/plugins/nicescroll/jquery.nicescroll.min.js')}}"></script>
        
        <script type="text/javascript">
          /* Nice Scroll
          ===============================*/
          $(document).ready(function () {
              
              "use strict";
              
              $("html").niceScroll({
                  scrollspeed: 60,
                  mousescrollstep: 35,
                  cursorwidth: 5,
                  cursorcolor: 'rgba(243, 131, 78, 0.7)',
                  cursorborder: 'none',
                  background: 'rgba(27, 30, 36, 0.0)',
                  cursorborderradius: 3,
                  autohidemode: false,
                  cursoropacitymin: 0.1,
                  cursoropacitymax: 1,
                  zindex: "999",
                  horizrailenabled: false
              });
            
          });
        </script>
        <script src="{{asset('assets/admin/plugins/bootstrap-sweetalert/sweetalert.js')}}" type="text/javascript"></script>
        <script src="{{asset('assets/admin/plugins/bootstrap-wizard/jquery.bootstrap.wizard.min.js')}}" type="text/javascript"></script>
        <script src="{{asset('assets/admin/js/ui-sweetalert.js')}}" type="text/javascript"></script>
        <script src="{{asset('assets/admin/js/process.js')}}" type="text/javascript"></script>

        <script type="text/javascript">
           $('#types').on('change',function(e){
            $('#types').addClass("pmd-textfield-floating-label-active pmd-textfield-floating-label-completed");
            
            var type_id = e.target.value;
            console.log(type_id);
              $('#courses').empty();
              $('#courses').append('<option>      </option>')
              $.ajax({
                    url:"{{route('ajaxdata.getcoursedata')}}",
                    method:'get',
                    data:{id:type_id},
                    dataType:'json',
                    success:function(data)
                    {
                        $.each(data, function(index, courseObj){
                            $('#courses').append('<option value="'+courseObj.id+'">'+courseObj.course_name+'</option>')
                        });
                    }
              });
          })
        </script>
        <script>
          $('#add_town').on('change',function(e){
            console.log(e);
           
              $('#add_center').empty();
              $('#add_course').empty();
              $('#add_level').empty();
              $('#add_material').empty();
            var town_id = e.target.value;


            $.ajax({
                    url:"{{route('ajaxdata.getcentersdata')}}",
                    method:'get',
                    data:{id:town_id},
                    dataType:'json',
                    success:function(data)
                    {
                        $.each(data, function(index, centerObj){
                          $('#add_center').append('<option>             </option>')
                            $('#add_center').append('<option value="'+centerObj.id+'">'+centerObj.center_name+'</option>')
                        });
                    }
              });
          });

          $('#add_center').on('change',function(e){
            console.log(e);
              $('#add_type').empty();
              $('#add_level').empty();
              $('#add_material').empty();
            var center_id = e.target.value;


            $.ajax({
                    url:"{{route('ajaxdata.getcoursesdata')}}",
                    method:'get',
                    data:{id:center_id},
                    dataType:'json',
                    success:function(data)
                    {
                        $.each(data, function(index, courseObj){
                          $('#add_type').append('<option>             </option>')
                          
                            $('#add_type').append('<option value="'+courseObj.coursetype_id +'">'+courseObj.type_name+'</option>')

                        });
                    }
              });
          });
          
          

          $('#add_type').on('change',function(e){
            console.log(e);
            $('#add_course').empty();
            $('#add_level').empty();
            $('#add_material').empty();
            var type_id = e.target.value;

            
            $.ajax({
                    url:"{{route('ajaxdata.getcoursesdataa')}}",
                    method:'get',
                    data:{id:type_id},
                    dataType:'json',
                    success:function(data)
                    {
                        $.each(data, function(index, courseObj){
                          $('#add_course').append('<option>             </option>')
                          $('#add_course').append('<option value="'+courseObj.id+'">'+courseObj.course_name+'</option>')

                        });
                    }
              });
          });
    
          $('#add_type').on('change',function(e){
            console.log(e);
            $('#add_level').empty();
            $('#add_material').empty();
            //var course_id = e.target.value;
             var type_id = e.target.value;

            $.ajax({
                    url:"{{route('ajaxdata.getlevelsdata')}}",
                    method:'get',
                    data:{id:type_id},
                    dataType:'json',
                    success:function(data)
                    {
                        $.each(data, function(index, levelObj){
                          $('#add_level').append('<option>             </option>')
                          $('#add_level').append('<option value="'+levelObj.id+'">'+levelObj.level_name+'</option>')
                        });
                    }
              });
          });
    
          $('#add_level').on('change',function(e){
            console.log(e);
    
            var level_id = e.target.value;
            
            $('#add_material').empty();
            $('#add_material').append('<tr><td>الدرس</td><td>تحديد</td><td>ملاحظات</td></tr>');
            $.ajax({
                    url:"{{route('ajaxdata.getmaterialsdata')}}",
                    method:'get',
                    data:{id:level_id},
                    dataType:'json',
                    success:function(data)
                    {
                        $.each(data, function(index, materialObj){
                          $('#add_material').append('<tr><td>'+materialObj.material_name+'</td><td><input name="mat'+materialObj.material_id+'" type="checkbox" checked> </td><td><input class="form-control" name="notes'+materialObj.material_id+'" type="text"></td></tr>')
                        });
                    }
                });
            });
    
    </script>
        @elseif(Route::currentRouteName() == 'admin.home') 
        <script src="{{asset('assets/admin/js/jQuery-2.1.4.min.js')}}"></script>
        
        
        <script src="{{asset('assets/admin/js/jquery-ui.min.js')}}"></script>
        <script src="{{asset('assets/admin/js/bootstrap.min.js')}}"></script>

        <script src="{{asset('assets/admin/js/global.js')}}"></script>

        <script src="{{asset('assets/admin/js/textfield.js')}}"></script>
        <!-- Select2 
        <script src="{{asset('assets/admin/js/select2.min.js')}}"></script>-->

        <!-- fullscreen -->
        <script src="{{asset('assets/admin/js/screenfull.js')}}"></script>

        <!-- text-rotator -->
        <script src="{{asset('assets/admin/js/morphext.js')}}"></script>

        <script src="{{asset('assets/admin/js/bootstrap-datetimepicker.js')}}"></script>
        <script>
          $(function () {
            /* Default date and time picker */
            $('.datetimepicker-default').datetimepicker({
            });
            $('.datepicker').datetimepicker({
              'format' : "YYYY-MM-DD",
            });
            $('.timepicker').datetimepicker({
              'format' : "LT",
            });
          });
        </script>


        
        <script src="{{asset('assets/admin/js/tab-scrollable.js')}}"></script>
        <script src="{{asset('assets/admin/js/jquery.sparkline.min.js')}}"></script>

        
        <script src="{{asset('assets/admin/js/app.min.js')}}"></script>
        
        <script>

        $(document).ready(function() {
          $('#advanced-check').change(function() {
            $('#advanced-search').toggle();
          });
        });  

        </script>


        

        <script src="{{asset('assets/admin/plugins/nicescroll/jquery.nicescroll.min.js')}}"></script>
        
        <script type="text/javascript">
          /* Nice Scroll
          ===============================*/
          $(document).ready(function () {
              
              "use strict";
              
              $("html").niceScroll({
                  scrollspeed: 60,
                  mousescrollstep: 35,
                  cursorwidth: 5,
                  cursorcolor: 'rgba(243, 131, 78, 0.7)',
                  cursorborder: 'none',
                  background: 'rgba(27, 30, 36, 0.0)',
                  cursorborderradius: 3,
                  autohidemode: false,
                  cursoropacitymin: 0.1,
                  cursoropacitymax: 1,
                  zindex: "999",
                  horizrailenabled: false
              });
            
          });
        </script>
        <script src="{{asset('assets/admin/plugins/bootstrap-sweetalert/sweetalert.js')}}" type="text/javascript"></script>
        <script src="{{asset('assets/admin/plugins/bootstrap-wizard/jquery.bootstrap.wizard.min.js')}}" type="text/javascript"></script>
        <script src="{{asset('assets/admin/js/ui-sweetalert.js')}}" type="text/javascript"></script>
        <script src="{{asset('assets/admin/js/process.js')}}" type="text/javascript"></script> 

<script>
      $('#get_new_center').on('change',function(e){
        console.log(e);
          $('#get_new_course').empty();
          $('#get_new_teacher').empty();
          $('#get_new_level').empty();
        var center_id = e.target.value;

        $.get('../admin/ajax-getnew-course?center_id=' + center_id, function(data){
          $('#get_new_course').append('<option>             </option>')
          $.each(data, function(index, courseObj){
            
            $('#get_new_course').append('<option value="'+courseObj.id+'">'+courseObj.course_name+'</option>')
          });
          
        })
      })

      $('#get_new_course').on('change',function(e){
        console.log(e);

        var course_id = e.target.value;

        $.get('../admin/ajax-getnew-level?course_id=' + course_id, function(data){
          
          $('#get_new_level').empty();
          $('#get_new_level').append('<option>             </option>')
          $.each(data, function(index, teacherObj){
            $('#get_new_level').append('<option value="'+teacherObj.level_id+'">'+teacherObj.level_name+'</option>')
          });
        })
      })

      $('#get_new_level').on('change',function(e){
        console.log(e);

        var level_id = e.target.value;
        $('#get_new_material').empty();
        $('#get_new_material').append('<tr><td>الدرس</td><td>تحديد</td></tr>')
        $.get('../admin/ajax-getnew-material?level_id=' + level_id, function(data){
          
          $.each(data, function(index, materialObj){
            $('#get_new_material').append('<tr><td>'+materialObj.material_name+'</td><td><input name="mat'+materialObj.material_id+'" type="checkbox" checked> </td></tr>')
          });
        })
      })

</script>

<script>
      $('#new_center').on('change',function(e){
        console.log(e);
          $('#new_course').empty();
          $('#new_teacher').empty();
          $('#new_level').empty();
        var center_id = e.target.value;

        $.get('../admin/ajax-new-teacher', function(data){

        $('#new_teacher').append('<option>             </option>')
        $.each(data, function(index, teacherObj){

            $('#new_teacher').append('<option value="'+teacherObj.id+'">'+teacherObj.teacher_name+'</option>')
        });
        })

        $.get('../admin/ajax-new-course?center_id=' + center_id, function(data){
          $('#new_course').append('<option>             </option>')
          $.each(data, function(index, courseObj){
            
            $('#new_course').append('<option value="'+courseObj.id+'">'+courseObj.course_name+'</option>')
          });
          
        })
      })

      $('#new_course').on('change',function(e){
        console.log(e);

        var course_id = e.target.value;

        $.get('../admin/ajax-new-level?course_id=' + course_id, function(data){
          
          $('#new_level').empty();
          $('#new_level').append('<option>             </option>')
          $.each(data, function(index, teacherObj){
            $('#new_level').append('<option value="'+teacherObj.level_id+'">'+teacherObj.level_name+'</option>')
          });
        })
      })

      $('#new_level').on('change',function(e){
        console.log(e);

        var level_id = e.target.value;
        $('#new_material').empty();
        $('#new_material').append('<tr><td>الدرس</td><td>تحديد</td><td>ملاحظات</td></tr>')
        $.get('../admin/ajax-new-material?level_id=' + level_id, function(data){
          
          $.each(data, function(index, materialObj){
            $('#new_material').append('<tr><td>'+materialObj.material_name+'</td><td><input name="mat'+materialObj.material_id+'" type="checkbox" checked> </td><td><input class="form-control" name="notes'+materialObj.material_id+'" type="text"></td></tr>')
          });
        })
      })

</script>
        
<script>
      $('#add_center').on('change',function(e){
        console.log(e);
          $('#add_course').empty();
          $('#add_student').empty();
          $('#add_level').empty();
          $('#add_type').empty();
        var center_id = e.target.value;

        $.get('../admin/ajax-add-student?center_id=' + center_id, function(data){

        $('#add_student').append('<option>             </option>')
        $.each(data, function(index, studentObj){

            $('#add_student').append('<option value="'+studentObj.id+'">'+studentObj.student_name+'     '+studentObj.national_id+'</option>')
        });
        })

        $.get('../admin/ajax-add-course?center_id=' + center_id, function(data){
          $('#add_course').append('<option>             </option>')
          $.each(data, function(index, courseObj){
            
            $('#add_course').append('<option value="'+courseObj.id+'">'+courseObj.course_name+'</option>')
          });
          
        })
      })

      $('#add_course').on('change',function(e){
        console.log(e);

        var course_id = e.target.value;

        $.get('../admin/ajax-add-level?course_id=' + course_id, function(data){
          
          $('#add_level').empty();
          $('#add_level').append('<option>             </option>')
          $.each(data, function(index, studentObj){
            $('#add_level').append('<option value="'+studentObj.level_id+'">'+studentObj.level_name+'</option>')
          });
        })
      })

      $('#add_level').on('change',function(e){
        console.log(e);

        var level_id = e.target.value;
        $('#add_material').empty();
        $('#add_material').append('<tr><td>الدرس</td><td>تحديد</td><td>ملاحظات</td></tr>')
        $.get('../admin/ajax-add-material?level_id=' + level_id, function(data){
          
          $.each(data, function(index, materialObj){
            $('#add_material').append('<tr><td>'+materialObj.material_name+'</td><td><input name="mat'+materialObj.material_id+'" type="checkbox" checked> </td><td><input class="form-control" name="notes'+materialObj.material_id+'" type="text"></td></tr>')
          });
        })
      })

</script>

<script>
      $('#center').on('change',function(e){
        console.log(e);
        $('#course').empty();
          $('#student').empty();
          $('#material').empty();
        var student_id = e.target.value;

        $.get('../admin/ajax-student?student_id=' + student_id, function(data){

        $('#student').append('<option>             </option>')
        $.each(data, function(index, studentObj){

            $('#student').append('<option value="'+studentObj.student_id+'">'+studentObj.student_name+'     '+studentObj.national_id+'</option>')
        });
        })
      })

      $('#student').on('change',function(e){
        console.log(e);

        var material_id = e.target.value;

        $.get('../admin/ajax-material?material_id=' + material_id, function(data){
          
          $('#material').empty();
          $('#material').append('<option>             </option>')
          $.each(data, function(index, materialObj){

            $('#material').append('<option value="'+materialObj.material_id+'">'+materialObj.material_name+'</option>')
          });
        })
      })

      $('#material').on('change',function(e){
        console.log(e);

        var percent_id = e.target.value;

        $.get('../admin/ajax-percent?percent_id=' + percent_id, function(data){
          
          $.each(data, function(index, percentObj){

            $('#percent').append('<tr><td>'+percentObj.percent_name+'</td><td><input name="p'+percentObj.id+'" type="text" size="3" placeholder="'+percentObj.grade+'" required></td></tr>')
          });
        })
      })
</script>

<script>
      $('#centers').on('change',function(e){
        console.log(e);

        var courses_id = e.target.value;

        $.get('../admin/ajax-courses?courses_id=' + courses_id, function(data){
          
          $('#courses').empty();
          $('#students').empty();
          $('#materials').empty();
          $('#courses').append('<option>             </option>')
          $.each(data, function(index, courseObj){
            
            $('#courses').append('<option value="'+courseObj.id+'">'+courseObj.course_name+'</option>')
          });
          
        })
      })

      $('#courses').on('change',function(e){
        console.log(e);

        var students_id = e.target.value;

        $.get('../admin/ajax-students?students_id=' + students_id, function(data){
          
          $('#students').empty();
          $('#materials').empty();
          $('#students').append('<option>             </option>')
          $.each(data, function(index, studentObj){
            $('#students').append('<option value="'+studentObj.id+'">'+studentObj.student_name+'     '+studentObj.national_id+'</option>')
          });
        })
      })

      $('#students').on('change',function(e){
        console.log(e);

        var materials_id = e.target.value;

        

        $.get('../admin/ajax-smaterial?materials_id=' + materials_id, function(data){

          $('#per').empty();
          var sum = 0;
          var count = 0;
          $.each(data, function(index, materialObj){
            sum = sum + Number(materialObj.total);
            count = count + 1;
            $('#per').append('<tr><td>'+materialObj.material_name+'</td><td>'+materialObj.total+'</td><td>'+materialObj.percent+'</td><td>'+materialObj.date+'</td><td><button class="btn btn-blue" id="button'+materialObj.id+'" data-toggle="modal" data-target="#modal-default">تفاصيل</button></td></tr>')
            $('#button'+materialObj.id+'').on('click',function(e){
              console.log(e);
              var p_id = materialObj.id;
              console.log(p_id);
              $.get('../admin/ajax-spercent?p_id=' + p_id, function(data){
          
              $('#percents').empty();
              $('#percents').append('<tr><td>البند</td><td>الدرجة</td></tr>')
              $.each(data, function(index, percentObj){

                $('#percents').append('<tr><td>'+percentObj.percent_name+'</td><td>'+percentObj.grade+'</td></tr>')
              });
            })
            })
          });
          var total = sum / count;
          var percent = "";
          if(total >= 85 && total <= 100){
            percent = "امتياز";
          }
          if(total >= 75 && total <= 85){
            percent = "جيد جدا";
          }
          if(total >= 65 && total <= 75){
            percent = "جيد";
          }
          if(total >= 50 && total <= 65){
            percent = "مقبول";
          }
          if(total >= 45 && total <= 50){
            percent = "ضعيف";
          }
          if(total < 45){
            percent = "راسب";
          }
          $('#per').append('<tr><td>تقييم المادة = </td><td>'+total+'</td><td> التقدير = </td><td>'+percent+'</td></tr>')
        
        })

        $.get('../admin/ajax-materials?materials_id=' + materials_id, function(data){
          
          $('#materials').empty();
          $('#materials').append('<option>             </option>')

          $.each(data, function(index, materialObj){

            $('#materials').append('<option value="'+materialObj.material_id+'">'+materialObj.material_name+'</option>')
            
          });
        })
      })

      $('#materials').on('change',function(e){
        console.log(e);

        var percents_id = e.target.value;
        var student_id = $('#students').val();

        $.get('../admin/ajax-percents?percents_id=' + percents_id+'&student_id=' + student_id, function(data){

          $('#per').empty();
          $('#per').append('<tr><td>المادة</td><td>الدرجة</td><td>التقدير</td><td>التاريخ</td><td>عرض</td></tr>')
          var sum = 0;
          var count = 0;
          $.each(data, function(index, materialObj){
            sum = sum + Number(materialObj.total);
            count = count + 1;
            $('#per').append('<tr><td>'+materialObj.material_name+'</td><td>'+materialObj.total+'</td><td>'+materialObj.percent+'</td><td>'+materialObj.date+'</td><td><button class="btn btn-blue"  data-toggle="modal" data-target="#modal-default" id="button'+materialObj.id+'">تفاصيل</button></td></tr>')
            $('#button'+materialObj.id+'').on('click',function(e){
              console.log(e);
              var p_id = materialObj.id;
              $.get('../admin/ajax-spercent?p_id=' + p_id, function(data){

                $('#percents').empty();
                $('#percents').append('<tr><td>البند</td><td>الدرجة</td></tr>')
                $.each(data, function(index, percentObj){

                  $('#percents').append('<tr><td>'+percentObj.percent_name+'</td><td>'+percentObj.grade+'</td></tr>')
                });
              })
            })
          });
          var total = sum / count;
          var percent = "";
          if(total >= 85 && total <= 100){
            percent = "امتياز";
          }
          if(total >= 75 && total <= 85){
            percent = "جيد جدا";
          }
          if(total >= 65 && total <= 75){
            percent = "جيد";
          }
          if(total >= 50 && total <= 65){
            percent = "مقبول";
          }
          if(total >= 45 && total <= 50){
            percent = "ضعيف";
          }
          if(total < 45){
            percent = "راسب";
          }
          $('#per').append('<tr><td>تقييم المادة = </td><td>'+total+'</td><td> التقدير = </td><td>'+percent+'</td></tr>')
        })
      })

      $('#from').on('change',function(e){
        console.log(e);

        var from = $('#from');
        var search = from.val();

        $.get('../admin/ajax-from?from=' + search, function(data){

          $('#per').empty();
          $('#per').append('<tr><td>المادة</td><td>الدرجة</td><td>التقدير</td><td>التاريخ</td><td>عرض</td></tr>')
          $.each(data, function(index, materialObj){

            $('#per').append('<tr><td>'+materialObj.material_name+'</td><td>'+materialObj.total+'</td><td>'+materialObj.percent+'</td><td>'+materialObj.date+'</td><td><button class="btn btn-blue"  data-toggle="modal" data-target="#modal-default" id="button'+materialObj.id+'">تفاصيل</button></td></tr>')
            $('#button'+materialObj.id+'').on('click',function(e){
              console.log(e);
              var p_id = materialObj.id;
              $.get('../admin/ajax-spercent?p_id=' + p_id, function(data){

                $('#percents').empty();
                $('#percents').append('<tr><td>البند</td><td>الدرجة</td></tr>')
                $.each(data, function(index, percentObj){

                  $('#percents').append('<tr><td>'+percentObj.percent_name+'</td><td>'+percentObj.grade+'</td></tr>')
                });
              })
            })
          });
        })
      })

      $('#to').on('change',function(e){
        console.log(e);
        var from = $('#from').val();
        var to = $('#to').val();

        $.get('../admin/ajax-to?to=' + to +'&from=' + from, function(data){

          $('#per').empty();
          $('#per').append('<tr><td>المادة</td><td>الدرجة</td><td>التقدير</td><td>التاريخ</td><td>عرض</td></tr>')
          $.each(data, function(index, materialObj){

            $('#per').append('<tr><td>'+materialObj.material_name+'</td><td>'+materialObj.total+'</td><td>'+materialObj.percent+'</td><td>'+materialObj.date+'</td><td><button class="btn btn-blue"  data-toggle="modal" data-target="#modal-default" id="button'+materialObj.id+'">تفاصيل</button></td></tr>')
            $('#button'+materialObj.id+'').on('click',function(e){
              console.log(e);
              var p_id = materialObj.id;
              $.get('../admin/ajax-spercent?p_id=' + p_id, function(data){

                $('#percents').empty();
                $('#percents').append('<tr><td>البند</td><td>الدرجة</td></tr>')
                $.each(data, function(index, percentObj){

                  $('#percents').append('<tr><td>'+percentObj.percent_name+'</td><td>'+percentObj.grade+'</td></tr>')
                });
              })
            })
          });
        })
      })
</script>
<script>
      $('#cent').on('change',function(e){
        console.log(e);

        var group_id = e.target.value;

        $.get('../admin/ajax-group?group_id=' + group_id, function(data){
          
          $('#group').empty();

          $('#group').append('<option>             </option>')
          $.each(data, function(index, groupObj){
            
            $('#group').append('<option value="'+groupObj.id+'">'+groupObj.course_name+'</option>')
          });
          
        })
      })

      $('#group').on('change',function(e){
        console.log(e);

        var student_id = e.target.value;

        $.get('../admin/ajax-student?student_id=' + student_id, function(data){
          $('#absent').empty();
          $('#absent').append('<tr><td>الطالب</td><td>الحالة</td><td></td></tr>')
          $.each(data, function(index, groupObj){
            if(groupObj.status == 1){
              $('#absent').append('<tr><td>'+groupObj.student_name+'</td><td><input name="ab'+groupObj.student_id+'" type="checkbox" checked> حاضر</td><td><input type="hidden" name="st'+groupObj.student_id+'" value="'+groupObj.student_id+'"></td></tr>')
            }else{
              $('#absent').append('<tr><td>'+groupObj.student_name+'</td><td><input name="ab'+groupObj.student_id+'" type="checkbox"> حاضر</td><td><input type="hidden" name="st'+groupObj.student_id+'" value="'+groupObj.student_id+'"></td></tr>')
            }
          });
        })
      })
</script>

<script>
      $('#cen').on('change',function(e){
        console.log(e);

        var group_id = e.target.value;

        $.get('../admin/ajax-group?group_id=' + group_id, function(data){
          
          $('#groups').empty();

          $('#groups').append('<option>             </option>')
          $.each(data, function(index, groupObj){
            
            $('#groups').append('<option value="'+groupObj.id+'">'+groupObj.course_name+'</option>')
          });
          
        })
      })
</script>



<script>
      $('#dto').on('change',function(e){
        console.log(e);

        var dfrom = $('#dfrom').val();
        var dto = $('#dto').val();

        var center = $('#cen').val();
        var course = $('#groups').val();
        $('#absents').empty();
        var col = '';
        $.get('../admin/ajax-dates?dto=' + dto +'&dfrom=' + dfrom+'&center=' + center+'&course=' + course, function(data){
          col += '<tr>';
          col += '<td>الطالب</td>';
          $.each(data, function(index, dateObj){
            col += '<td>'+dateObj.date+'</td>';
          });
          col += '</tr>';
          $('#absents').append(col);
        })
        
        $.get('../admin/ajax-studs?dto=' + dto +'&dfrom=' + dfrom+'&center=' + center+'&course=' + course, function(data){
          var row = '';
          $.each(data, function(index, groupObj){
            var s_id = groupObj.student_id;
            $.get('../admin/ajax-dto?dto=' + dto +'&dfrom=' + dfrom+'&center=' + center+'&course=' + course+'&s_id=' + s_id, function(data){
              row += '<tr>';
              row += '<td>'+groupObj.student_name+'</td>';
              var r = '';
              $.each(data, function(index, statusObj){
                if(statusObj.status == 1){
                  r += '<td>حاضر</td>';
                }else{
                  r += '<td>غائب</td>';
                }
              });
              row += r;
              row += '</tr>';
              $('#absents').append(row);
              console.log(row);
              row = '';
            })
          });
        })
      })
</script>
<script>
      $('#center_pay').on('change',function(e){
        console.log(e);
        $('#course_pay').empty();
        $('#date').empty();
        $('#amount').empty();
        $('#remain').empty();
        $('#refund').empty();
        $('#price').empty();
        $('#student_pay').empty();
        var course_id = e.target.value;

        $.get('../admin/ajax-pcourse?course_id=' + course_id, function(data){

        $('#course_pay').append('<option>             </option>')
        $.each(data, function(index, courseObj){
            
            $('#course_pay').append('<option value="'+courseObj.id+'">'+courseObj.course_name+'</option>')
          });
        })
      })

      $('#course_pay').on('change',function(e){
        console.log(e);
        $('#date').empty();
        $('#amount').empty();
        $('#remain').empty();
        $('#refund').empty();
        $('#price').empty();
        $('#student_pay').empty();
        var student_id = e.target.value;

        $.get('../admin/ajax-pstudent?student_id=' + student_id, function(data){
          
          $('#student_pay').empty();
          $('#student_pay').append('<option>             </option>')
          $.each(data, function(index, studentObj){
            $('#student_pay').append('<option value="'+studentObj.id+'">'+studentObj.student_name+'     '+studentObj.national_id+'</option>')
          });
        })
      })

      $('#student_pay').on('change',function(e){
        console.log(e);
        $('#date').empty();
        $('#amount').empty();
        $('#remain').empty();
        $('#refund').empty();
        $('#price').empty();
        var student_id = e.target.value;
        var price = 0;
        var total = 0;
        
        $.get('../admin/ajax-materialprice?student_id=' + student_id, function(data){
          
          $.each(data, function(index, materialObj){
            price += Number(materialObj.price);
          });
          
          document.getElementById("price").innerHTML = '<h5>مصروفات الطالب  : <strong>'+price+'</strong></h5>';
          console.log(price);
          $.get('../admin/ajax-date?student_id=' + student_id, function(data){
          
            $.each(data, function(index, matObj){
              document.getElementById("date").innerHTML = '<h5> تاريخ آخر عملية دفع : <strong>'+matObj.date+'</strong></h5>';
              if(matObj.remain < 0){
                document.getElementById("amount").innerHTML = '<h5> آخر مبلغ تم دفعه : <strong>'+matObj.amount+'</strong></h5>';
                document.getElementById("remain").innerHTML = '<h5> المبلغ المتبقى : <strong>'+Number(matObj.remain * -1)+'</strong></h5>';
              }
              if(matObj.remain >= 0){
                document.getElementById("amount").innerHTML = '<h5>آخر مبلغ تم دفعه : <strong>'+matObj.amount+'</strong></h5>';
                document.getElementById("refund").innerHTML = '<h5>الرصيد المتبقي : <strong>'+matObj.remain+'</strong></h5>';
              }
            });
          })

        })
        
      })

</script>

<script>
      $('#center_pays').on('change',function(e){
        console.log(e);
        $('#course_pays').empty();
        $('#student_pays').empty();
        var course_id = e.target.value;

        $.get('../admin/ajax-pcourse?course_id=' + course_id, function(data){

        $('#course_pays').append('<option>             </option>')
        $.each(data, function(index, courseObj){
            
            $('#course_pays').append('<option value="'+courseObj.id+'">'+courseObj.course_name+'</option>')
          });
        })
      })

      $('#course_pays').on('change',function(e){
        console.log(e);
        $('#student_pays').empty();
        var student_id = e.target.value;

        $.get('../admin/ajax-pstudent?student_id=' + student_id, function(data){
          
          $('#student_pays').empty();
          $('#student_pays').append('<option>             </option>')
          $.each(data, function(index, studentObj){
            $('#student_pays').append('<option value="'+studentObj.id+'">'+studentObj.student_name+'     '+studentObj.national_id+'</option>')
          });
        })
      })

      $('#years').on('change',function(e){
        console.log(e);
        $('#process').empty();
        var year = e.target.value;
        var student = $('#student_pays');
        var student_id = student.val();
        
        $.get('../admin/ajax-process?year=' + year+'&student_id=' + student_id, function(data){
          $('#process').append('<tr><td>التاريخ</td><td>المبلغ</td></tr>')
          var total = 0;
          $.each(data, function(index, pObj){
              var c = Number(pObj.amount);
            total = total + c;
            $('#process').append('<tr><td>'+pObj.date+'</td><td>'+pObj.amount+'</td></tr>')
          });
          $('#process').append('<tr><td>اجمالى المدفوع : </td><td>'+total+'</td></tr>')
        })
      })

      $('#month').on('change',function(e){
        console.log(e);
        $('#process').empty();
        var month = e.target.value;
        var year = $('#years');
        var year_id = year.val();
        var student = $('#student_pays');
        var student_id = student.val();

        $.get('../admin/ajax-month?year_id=' + year_id+'&student_id=' + student_id+'&month=' + month, function(data){
          var total = 0;
          $('#process').append('<tr><td>التاريخ</td><td>المبلغ</td></tr>')
          $.each(data, function(index, pObj){
              var c = Number(pObj.amount);
            total = total + c;
            $('#process').append('<tr><td>'+pObj.date+'</td><td>'+pObj.amount+'</td></tr>')
          });
          $('#process').append('<tr><td>اجمالى المدفوع : </td><td>'+total+'</td></tr>')
        })
      })

</script>


<script>
  $('#teacher_salary').on('change',function(e){
    $('#salary').empty();
    $('#days_salary').empty();
    $('#minus_salary').empty();
    $('#hours_salary').empty();
    $('#bonus_salary').empty();
    $('#parts_salary').empty();
    console.log(e);
    var month = $('#month_salary').val();
    var year = $('#year_salary').val();
    var teacher = e.target.value;
    $.get('../admin/ajax-salary?teacher=' + teacher, function(data){
      console.log(data);
      $.each(data, function(index, salaryObj){
        $("#salary").attr({
            "value" : salaryObj.salary
        });
        $("#thour").attr({
            "value" : salaryObj.hours
        });
      });
    })
  })

  $('#month_salary').on('change',function(e){
    $('#salary').empty();
    $('#days_salary').empty();
    $('#minus_salary').empty();
    $('#hours_salary').empty();
    $('#bonus_salary').empty();
    $('#parts_salary').empty();
    console.log(e);
    var teacher = $('#teacher_salary').val();
    var year = $('#year_salary').val();
    var month = e.target.value;
    $.get('../admin/ajax-parts?teacher=' + teacher+'&year=' + year+'&month=' + month, function(data){
      var tr = 0;
      $.each(data, function(index, partObj){
        tr = tr + partObj.part;
      });
      $("#parts_salary").attr({
        "value" : tr
      });
    })
    $.get('../admin/ajax-days?teacher=' + teacher+'&year=' + year+'&month=' + month, function(data){
      console.log(data);
      var count = 0;
      $.each(data, function(index, salaryObj){
        count = count + 1;
      });
      $("#days_salary").attr({
        "value" : count
      });
      var thour = $("#thour").val();
    var salary = $("#salary").val();    
    var days = count;
    var salary_hour = Math.floor((salary/30)/thour);
    var day = Math.floor((salary/30) * days);
    
    $.get('../admin/ajax-attendSalary?teacher=' + teacher+'&year=' + year+'&month=' + month, function(data){
      var result1 = 0;
      var result2 = 0;
      $.each(data, function(index, sObj){
        var a = sObj.attends;
        var b = sObj.leaves;
            
            var first = a.split(":")
            var second = b.split(":")
                
                var xx;
                var yy;
                
                if(parseInt(first[0]) < parseInt(second[0])){          
                    
                    if(parseInt(first[1]) < parseInt(second[1])){
                        
                        yy = parseInt(first[1]) + 60 - parseInt(second[1]);
                        xx = parseInt(first[0]) + 24 - 1 - parseInt(second[0]);
                    
                    }else{
                      yy = parseInt(first[1]) - parseInt(second[1]);
                      xx = parseInt(first[0]) + 24 - parseInt(second[0]);
                    }
                  
                
                
                }else if(parseInt(first[0]) == parseInt(second[0])){
                
                  if(parseInt(first[1]) < parseInt(second[1])){
                        
                        yy = parseInt(first[1]) + 60 - parseInt(second[1]);
                        xx = parseInt(first[0]) + 24 - 1 - parseInt(second[0]);
                    
                    }else{
                      yy = parseInt(first[1]) - parseInt(second[1]);
                      xx = parseInt(first[0]) - parseInt(second[0]);
                    }
                
                }else{
                    
                    
                  if(parseInt(first[1]) < parseInt(second[1])){
                        
                        yy = parseInt(first[1]) + 60 - parseInt(second[1]);
                        xx = parseInt(first[0]) - 1 - parseInt(second[0]);
                    
                    }else{
                      yy = parseInt(first[1]) - parseInt(second[1]);
                      xx = parseInt(first[0]) - parseInt(second[0]);
                    }
                
                
                }
            
            if(xx < 10)
                xx = "0" + xx;
                
            
            if(yy < 10)
                yy = "0" + yy;
          
            var rr = xx + ":" + yy;
            if(xx > thour){
              result1 = xx - thour;
            }

            if(xx < thour){
              result2 = thour - xx ;
            }
        
      });
              $("#hours_salary").attr({
                "value" : result1
              });
              var h = (result1 * salary_hour);
              $("#bonus_salary").attr({
                "value" : h
              });
              var hh = (result2 * salary_hour);
              $("#minus_salary").attr({
                "value" : hh
              });
    })
    })
  })
</script>
        @elseif(  Route::currentRouteName() == 'admin.reports.levelss' ||   Route::currentRouteName() == 'admin.levels' || Route::currentRouteName() == 'admin.reports.skilldegree'  || Route::currentRouteName() == 'admin.reports.finaltest'  || Route::currentRouteName() == 'admin.reports.graduationcertificate'  || Route::currentRouteName() == 'admin.reports.passlevels'  ||  Route::currentRouteName() == 'admin.reports.differentparties'  || Route::currentRouteName() == 'admin.reports.bookstudents'  || Route::currentRouteName() == 'admin.reports.parts'  || Route::currentRouteName() == 'admin.reports.levelstudents'  || Route::currentRouteName() == 'admin.reports.studentdatacourse' ||  Route::currentRouteName() == 'admin.reports.studentss'  ||  Route::currentRouteName() == 'admin.reports.Exemptstudent'  || Route::currentRouteName() == 'admin.reports.teacher'  || Route::currentRouteName() == 'admin.reports.studentcourse'  || Route::currentRouteName() == 'admin.reports.datacenter' || Route::currentRouteName() == 'admin.reports.students'  ||Route::currentRouteName() == 'admin.reports.coursebookstudent' ||Route::currentRouteName() == 'admin.reports.coursereservation'||Route::currentRouteName() == 'admin.reports.financialclaims' || Route::currentRouteName() == 'admin.reports.Scounts' || Route::currentRouteName() == 'admin.students.pendingadds' || Route::currentRouteName() == 'admin.students.pendingedits' || Route::currentRouteName() == 'admin.types' || Route::currentRouteName() == 'admin.towns' || Route::currentRouteName() == 'admin.seasons' || Route::currentRouteName() == 'admin.store' || Route::currentRouteName() == 'admin.reports.salaries' || Route::currentRouteName() == 'admin.jobs' || Route::currentRouteName() == 'admin.reports.attend' || Route::currentRouteName() == 'admin.reports.absent' || Route::currentRouteName() == 'admin.reports.counts' || Route::currentRouteName() == 'admin.reports.grades' || Route::currentRouteName() == 'admin.students' || Route::currentRouteName() == 'admin.students.count' || Route::currentRouteName() == 'admin.teachers' || Route::currentRouteName() == 'admin.guardians' || Route::currentRouteName() == 'admin.materials' || Route::currentRouteName() == 'admin.centers' || Route::currentRouteName() == 'admin.courses' ||  Route::currentRouteName() == 'admin.transportations') 
        <script src="{{asset('assets/admin/js/jQuery-2.1.4.min.js')}}"></script>
        <script src="{{asset('assets/admin/js/jquery-ui.min.js')}}"></script>
        <script src="{{asset('assets/admin/js/bootstrap.min.js')}}"></script>
        
        <script src="https://cdn.datatables.net/1.10.19/js/jquery.dataTables.min.js"></script>
        <script src="https://cdn.datatables.net/buttons/1.5.2/js/dataTables.buttons.min.js"></script>
        <script src="https://cdnjs.cloudflare.com/ajax/libs/jszip/3.1.3/jszip.min.js"></script>
        <script src="https://cdnjs.cloudflare.com/ajax/libs/pdfmake/0.1.36/pdfmake.min.js"></script>
        <script src="https://cdnjs.cloudflare.com/ajax/libs/pdfmake/0.1.36/vfs_fonts.js"></script>
        <script src="https://cdn.datatables.net/buttons/1.5.2/js/buttons.html5.min.js"></script>
        <script src="https://cdn.datatables.net/buttons/1.5.2/js/buttons.print.min.js"></script>
        <script src="https://cdn.datatables.net/buttons/1.5.2/js/buttons.colVis.min.js"></script>
        <script src="https://nightly.datatables.net/responsive/js/dataTables.responsive.min.js"></script>

        <script>
            $(document).ready(function() {
              var t = $('#tables').DataTable( {
                  initComplete: function () {
                      this.api().columns().every( function () {
                          var column = this;
                          var select = $('<select><option value=""></option></select>')
                              .appendTo( $(column.footer()).empty() )
                              .on( 'change', function () {
                                  var val = $.fn.dataTable.util.escapeRegex(
                                      $(this).val()
                                  );
          
                                  column
                                      .search( val ? '^'+val+'$' : '', true, false )
                                      .draw();
                              } );
          
                          column.data().unique().sort().each( function ( d, j ) {
                              select.append( '<option value="'+d+'">'+d+'</option>' )
                          } );
                      } );
                  },
                  "language":{
                    "decimal":        "",
                    "emptyTable":     "لا يوجد بيانات",
                    "info": "عرض صفحة _PAGE_ من _PAGES_ صفحات",
                    "infoEmpty":      "عرض مدخلات من 0 الى 0 ",
                    "infoFiltered":   "(محدد من _MAX_ عنصر)",
                    "infoPostFix":    "",
                    "thousands":      ",",
                    "lengthMenu":     "عرض مدخلات القائمة",
                    "loadingRecords": "...تحميل",
                    "processing":     "...تنفيذ",
                    "search":         "ابحث:",
                    "zeroRecords":    "لا يوجد نتائج للبحث",
                    "paginate": {
                        "first":      "الأول",
                        "last":       "الأخير",
                        "next":       "التالى",
                        "previous":   "السابق"
                    },
                    "aria": {
                        "sortAscending":  ": activate to sort column ascending",
                        "sortDescending": ": activate to sort column descending"
                    }
                  },
                  responsive: true,
                  dom: 'Bfrtip',
                    columnDefs: [
                        {
                            targets: 1,
                            className: 'noVis'
                        }
                    ],
                    buttons: [
                        {
                            text: 'نسخ',
                            extend: 'copyHtml5',
                            exportOptions: {
                                columns: [ ':visible' ]
                            }
                        },
                        {
                          text: 'اكسل',
                            extend: 'excelHtml5',
                            exportOptions: {
                                columns: ':visible'
                            }
                        },
                        {
                          text: 'طباعة',
                            extend: 'print',
                            messageTop: '',
                            exportOptions: {
                                columns: [ ':visible' ]
                            }
                        },
                        {
                          extend: 'colvis',
                          className: 'btn-orange',
                          text: 'تحديد الأعمدة'
                        },
                        
                    ],
                    orderCellsTop: true,
                    fixedHeader: true,
                    pageLength: 100,
                    "columnDefs": [ {
                        "searchable": false,
                        "orderable": false,
                        "targets": 0
                    } ],
                    "order": [[ 1, 'asc' ]]
                } );
            
                t.on( 'order.dt search.dt', function () {
                    t.column(0, {search:'applied', order:'applied'}).nodes().each( function (cell, i) {
                        cell.innerHTML = i+1;
                    } );
                } ).draw();

                $('#tables thead tr:eq(1) th').each( function () {
                    var title = $(this).text();
                    $(this).html( '<input type="text" placeholder="بحث ب'+title+'" class="form-control" />' );
                } );
            
            // Apply the search
                $( '#tables thead'  ).on( 'keyup', ".form-control",function () {
                    t.column( $(this).parent().index() )
                        .search( this.value )
                        .draw();
                } );
            } );
        </script>
        
        

        <!-- fullscreen -->
        <script src="{{asset('assets/admin/js/screenfull.js')}}"></script>

        <!-- text-rotator -->
        <script src="{{asset('assets/admin/js/morphext.js')}}"></script>

        


        
        
        <script src="{{asset('assets/admin/js/app.min.js')}}"></script>

        
        <script src="{{asset('assets/admin/plugins/nicescroll/jquery.nicescroll.min.js')}}"></script>
        <script type="text/javascript">
          /* Nice Scroll
          ===============================*/
          $(document).ready(function () {
              
              "use strict";
              
              $("html").niceScroll({
                  scrollspeed: 60,
                  mousescrollstep: 35,
                  cursorwidth: 5,
                  cursorcolor: 'rgba(243, 131, 78, 0.7)',
                  cursorborder: 'none',
                  background: 'rgba(27, 30, 36, 0.0)',
                  cursorborderradius: 3,
                  autohidemode: false,
                  cursoropacitymin: 0.1,
                  cursoropacitymax: 1,
                  zindex: "999",
                  horizrailenabled: false
              });
            
          });
        </script>
        <script src="{{asset('assets/admin/js/process.js')}}" type="text/javascript"></script>

      @else
        <script data-require="jquery@*" data-semver="2.1.4" src="{{asset('assets/admin/js/jQuery-2.1.4.min.js')}}"></script>
        <script src="{{asset('assets/admin/js/jquery-ui.min.js')}}"></script>
        <script src="{{asset('assets/admin/js/bootstrap.min.js')}}"></script>

        <script src="{{asset('assets/admin/js/global.js')}}"></script>

        <script src="{{asset('assets/admin/js/textfield.js')}}"></script>
        <!-- Select2 
        <script src="{{asset('assets/admin/js/select2.min.js')}}"></script>-->

        
        <!-- fullscreen -->
        <script src="{{asset('assets/admin/js/screenfull.js')}}"></script>

        <!-- text-rotator -->
        <script src="{{asset('assets/admin/js/morphext.js')}}"></script>

        <script src="{{asset('assets/admin/js/bootstrap-datetimepicker.js')}}"></script>
        <script>
          $(function () {
            /* Default date and time picker */
            $('.datepicker').datetimepicker({
              'format' : "YYYY-MM-DD",
            });
            
            $('.datetimepicker-default').datetimepicker({
              'format' : "LT",
            });

            
          });
        </script>
        

        
        <script src="{{asset('assets/admin/js/custom-file-input.js')}}"></script>

        <script src="{{asset('assets/admin/js/app.min.js')}}"></script>
        
        <script>

        $(document).ready(function() {
          $('#advanced-check').change(function() {
            $('#advanced-search').toggle();
          });
        });  

        </script>


        

        <script src="{{asset('assets/admin/plugins/nicescroll/jquery.nicescroll.min.js')}}"></script>
        <script type="text/javascript">
          /* Nice Scroll
          ===============================*/
          $(document).ready(function () {
              
              "use strict";
              
              $("html").niceScroll({
                  scrollspeed: 60,
                  mousescrollstep: 35,
                  cursorwidth: 5,
                  cursorcolor: 'rgba(243, 131, 78, 0.7)',
                  cursorborder: 'none',
                  background: 'rgba(27, 30, 36, 0.0)',
                  cursorborderradius: 3,
                  autohidemode: false,
                  cursoropacitymin: 0.1,
                  cursoropacitymax: 1,
                  zindex: "999",
                  horizrailenabled: false
              });
            
          });
        </script>
        <script src="{{asset('assets/admin/plugins/ckeditor/ckeditor.js')}}"></script>
        <script src="{{asset('assets/admin/plugins/bootstrap-sweetalert/sweetalert.js')}}" type="text/javascript"></script>
        <script src="{{asset('assets/admin/plugins/bootstrap-wizard/jquery.bootstrap.wizard.min.js')}}" type="text/javascript"></script>
        <script src="{{asset('assets/admin/js/ui-sweetalert.js')}}" type="text/javascript"></script>
        <script src="{{asset('assets/admin/plugins/bootstrap-fileinput/bootstrap-fileinput.js')}}" type="text/javascript"></script>
        
        <script src="{{asset('assets/admin/js/process.js')}}" type="text/javascript"></script>
        <script src="{{asset('assets/admin/js/main.js')}}" type="text/javascript"></script>
        <script src="{{asset('assets/admin/js/main2.js')}}" type="text/javascript"></script>
        
        <script>
          function readURL(input) {
            if (input.files && input.files[0]) {
            var reader = new FileReader();
            reader.onload = function(e) {
              $('#blah').attr('src', e.target.result);
            }
            reader.readAsDataURL(input.files[0]);
            }
          }
          $("#imgInp").change(function() {
            readURL(this);
          });
        </script>
        <script>
          $(function () {
            /* Default date and time picker */
            $('.datetimepicker-default').datetimepicker({
            });
            $('.datepicker').datetimepicker({
              'format' : "YYYY-MM-DD",
            });
            $('.timepicker').datetimepicker({
              'format' : "LT",
            });
          });
        </script>
        
        @endif
        @yield('footer')
    </body>
</html>