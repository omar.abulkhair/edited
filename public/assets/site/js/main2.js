$(document).ready(function () {
    "use strict";
    $('.btn-responsive-nav').on('click', function () {
        $(this).parents('.wrapper').toggleClass('sidebar-collapse');
    });
});

/*File Upload 
 =========================*/
$(document).ready(function () {
    "use strict";
    $("#image2").on("change", function () {
        document.getElementById("progressBar2").style.width = 0 + '%';
        document.getElementById("progressBar2").innerHTML = 0 + '%';
        $('.total-upload').html('');
        $("#status2").html('');
        $(this).next("p").text($(this).val());
    });
});


/* Upload Progress Bar Script
 Script written by Adam Khoury @ DevelopPHP.com */
/* Video Tutorial: http://www.youtube.com/watch?v=EraNFJiY0Eg 
 ===============================*/
$(document).ready(function () {

    

    "use strict";
    
    var url=$('#url2').val();
    var storage=$('#storage2').val();
    function get_elment(el) {
        return document.getElementById(el);
    }
    function progressHandler(event) {
        get_elment("loaded_n_total2").innerHTML = "<span class='total-upload'>Uploaded  " + event.loaded + " bytes of " + event.total + "</span>";
        var percent = (event.loaded / event.total) * 100;
        var rounPercent = get_elment("progressBar2").value = Math.round(percent);
        get_elment("progressBar2").style.width = rounPercent + '%';
        get_elment("progressBar2").innerHTML = rounPercent + '%';
    }
    function completeHandler(event) {
        get_elment("status2").innerHTML = "<div class='callout callout-info pull-right'>تم تحميل الملف بنجاح </div>";
        get_elment("progressBar2").value = 0;
        $("#upload-btn2").removeAttr("disabled");
//        cons
//        $("#img2").val(event.success);

//        ;
//        alert(event.success);
    }
    function errorHandler(event) {
        get_elment("status2").innerHTML="";
        $("#upload-btn2").removeAttr("disabled");
    }
    function abortHandler(event) {
        get_elment("status2").innerHTML = "Upload Aborted";
    }
    function uploadFile() {
        get_elment("status2").innerHTML ="";
        var file = get_elment("image2").files[0],
            formdata = new FormData(),
            ajax = new XMLHttpRequest();
        var fileName = get_elment("image2").value;
        var allowed_extensions = new Array("jpg","png","gif","pdf");
        var file_extension = fileName.split('.').pop(); 
        
            if(allowed_extensions.includes(file_extension))
               {
                formdata.append("image2", file);
                formdata.append("_token", $('[name="csrf_token"]').attr('content'));
                formdata.append("storage2", storage2);
                ajax.upload.addEventListener("progress", progressHandler, false);
                ajax.addEventListener("load", completeHandler, false);
                ajax.addEventListener("error", errorHandler, false);
                ajax.addEventListener("abort", abortHandler, false);
                ajax.open("POST", url);
                ajax.send(formdata);
                ajax.onreadystatechange = function () {
                    var response=JSON.parse(this.response);
        //            console.log(response);
                    if (this.readyState == 4 && this.status2 == 200) {
                        $("#img2").val(response.success);
                    }
                };
            }
            else
            {
                 get_elment("status2").innerHTML="<div class='callout callout-danger pull-right'>يرجى تحميل ملفات بصيغة  JPG,PNG,GIF,PDF</div>";
            }
        
        
    }

    $('#upload-btn2').on('click', function () {
        uploadFile();
        $(this).attr("disabled", false);
    });
});
