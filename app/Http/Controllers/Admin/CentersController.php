<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Center;
use App\Town;
use App\CenterDocument;
use Carbon\Carbon;
use Config;
use DB;

class CentersController extends Controller
{
    public function getIndex() {
        $centers = Center::where("active", 1)->get();
        return view('admin.pages.center.index', compact('centers'));
    }

    public function getAdd() {
        $centers = Center::where("active", 1)->get();
        $towns = Town::where("active", 1)->get();
        return view('admin.pages.center.add', compact('centers','towns'));
    }

    public function insert(Request $request) {
        $v = validator($request->all() ,[
            'name' => 'required',
            'address' => 'required',
            'manager' => 'required',
            'head_manager' => 'required',
            'phone' => 'required',
            'email' => 'required',
            'website' => 'required',
            'town_id' => 'required',
            'age' => 'required',
        ] ,[
            'name.required' => 'من فضلك أدخل اسم المركز',
            'address.required' => 'من فضلك أدخل عنوان المركز',
            'manager.required' => 'من فضلك أدخل اسم المشرف',
            'head_manager.required' => 'من فضلك أدخل اسم المدير العام',
            'phone.required' => 'من فضلك أدخل رقم الهاتف',
            'email.required' => 'من فضلك أدخل البريد الالكترونى',
            'website.required' => 'من فضلك أدخل الموقع الالكترونى',
            'town_id.required' => 'من فضلك اختر المحافظة',
            'age.required' => 'من فضلك حدد سن القبول',
        ]);

        if ($v->fails()){
            return ['status' => false , 'data' => implode(PHP_EOL ,$v->errors()->all())];
        }

        $center = new Center();

        $center->center_name = $request->name;
        $center->address = $request->address;
        $center->manager = $request->manager;
        $center->head_manager = $request->head_manager;
        $center->phone = $request->phone;
        $center->phone2 = $request->phone2;
        $center->phone3 = $request->phone3;
        $center->email = $request->email;
        $center->website = $request->website;
        $center->town_id = $request->town_id;
        $center->age = $request->age;
        if($request->active == "on"){
            $center->active = 1;
        }elseif(empty($request->active)){
            $center->active = 0;
        }

        if ($center->save()){
            return ['status' => 'succes' ,'data' => 'تم اضافة البيانات بنجاح'];            
        }else{
            return ['status' => false ,'data' => 'حدث خطأ , من فضلك أعد المحاولة '];
        }
    }

    public function getEdit($id) {
        if (isset($id)) {
            $centers = DB::table('centers')
                    ->join('towns','centers.town_id','=','towns.id')
                    ->select('towns.*', 'centers.*')
                    ->where('centers.id','=',$id)
                    ->get();
                    
            $towns = Town::where("active", 1)->get();
            return view('admin.pages.center.edit', compact('centers','towns'));
        }        
    }

    public function postEdit(Request $request,$id) {
      
        $center = Center::find($id);

        $center->center_name = $request->name;
        $center->address = $request->address;
        $center->manager = $request->manager;
        $center->head_manager = $request->head_manager;
        $center->phone = $request->phone;
        $center->email = $request->email;
        $center->website = $request->website;
        $center->town_id = $request->town_id;
        $center->age = $request->age;
        
        
        if($request->active == "on"){
            $center->active = 1;
        }elseif(empty($request->active)){
            $center->active = 0;
        }

        if ($center->save()){
            return ['status' => 'succes' ,'data' => 'تم تحديث البيانات بنجاح'];
        }else{
            return ['status' => false ,'data' => 'حدث خطأ , من فضلك أعد المحاولة '];
        }
    }

    public function delete($id) {
        if (isset($id)) {
            $center = Center::find($id);
            $center->delete();

            DB::table('courses')->where('center_id','=', $id)->delete();
            DB::table('students')->where('center_id','=', $id)->delete();
            DB::table('center_documents')->where('center_id','=', $id)->delete();
            DB::table('absents')->where('center_id','=', $id)->delete();
            DB::table('transportations')->where('center_id','=', $id)->delete();

            return redirect()->back();
        }
    }

    public function files() {
        $centers = Center::where("active", 1)->get();
        $docs = DB::table('center_documents')
                ->join('centers','center_documents.center_id','=','centers.id')
                ->select('center_documents.*','centers.center_name')
                ->get();
        $now = Carbon::now();
        return view('admin.pages.center.files', compact('now','centers','docs'));
    }

    public function addDoc(Request $request) {

        $v = validator($request->all() ,[
            'image2' => 'required|mimes:jpeg,jpg,png,gif,pdf|max:20000',
            'center_id' => 'required',
            'type' => 'required',
        ] ,[
            'image2.required' => 'من فضلك قم بتحميل الملف',
            'image2.mimes' => 'يرجى تحميل ملفات بصيغة  JPG,PNG,GIF,PDF',
            'image2.max' => 'الحد الاقصى لحجم الملف : 20 MB',
            'center_id.required' => 'من فضلك اختر مركز',
            'type.required' => 'من فضلك اختر نوع الوثيقة',
        ]);

        if ($v->fails()){
            return ['status' => false , 'data' => implode(PHP_EOL ,$v->errors()->all())];
        }

        $doc = new CenterDocument();
        $doc->center_id = $request->center_id;
        $doc->type = $request->type;

        $destination = storage_path('uploads/' . $request->storage);
        $image = $request->file('image2');
        if ($image) {
            if (is_file($destination . "/{$image}")) {
                @unlink($destination . "/{$image}");
            }
            $imageName = $image->getClientOriginalName();
            $image->move($destination, $imageName);
            $doc->file = $imageName;
        }
        

        if ($doc->save()){
            return ['status' => 'succes' ,'data' => 'تم اضافة البيانات بنجاح'];            
        }else{
            return ['status' => false ,'data' => 'حدث خطأ , من فضلك أعد المحاولة '];
        }
    }

    public function deleteDoc($id) {
        if (isset($id)) {
            $doc = CenterDocument::find($id);

            $doc->delete();

            return redirect()->back();
        }
    }
    
    
    public function downloadFile($id )
    {
       $file = CenterDocument::find($id);
       
    $pathToFile = storage_path('uploads/'.$file->file);

  
     return response()->download($pathToFile);
    
  
    }

}
