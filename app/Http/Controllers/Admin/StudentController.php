<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Student;
use App\Season;
use App\StudentArchive;
use App\Guardian;
use App\Center;
use App\Course;
use App\Level;
use App\Material;
use App\Member;
use App\Payment;
use Carbon\Carbon;
use App\StudentDocument;
use App\StudentCourse;
use App\StudentMaterial;
use Response;
use Illuminate\Support\Facades\Input;
use Config;
use Session;
use Auth;
use Hash;
use DB;


class StudentController extends Controller{
    public function getIndex() {
        $seasons = Season::where("active", 1)->get();
        $students = DB::table('students')
                ->join('centers','centers.id','=','students.center_id')
                ->join('seasons','seasons.id','=','students.season_id')
                ->select('students.*','centers.center_name','seasons.season_name')
                ->where("students.active", 1)
                ->get();
        $guardians = Guardian::where("active", 1)->get();
        $centers = Center::where("active", 1)->get();
        $courses = Course::where("active", 1)->get();
        $levels = Level::where("active", 1)->get();
        $docs = DB::table('student_documents')
                ->join('students','student_documents.student_id','=','students.id')
                ->select('student_documents.*','students.student_name')
                ->get();
        $materials = Material::where("active", 1)->get();
        $mats = DB::table('student_materials')
                    ->join('students','student_materials.student_id','=','students.id')
                    ->join('materials','student_materials.material_id','=','materials.id')
                    ->select('student_materials.*','students.student_name','materials.material_name')
                    ->get();
        $now = Carbon::now();
        return view('admin.pages.student', compact('seasons','now','courses','students','docs','guardians','centers','levels','materials','mats'));
    }

    function fetchcenters(){
        $centers = Center::get()->where('active','=',1);
        echo json_encode($centers);
    }

    function fetchseasons(){
        $seasons = Season::get()->where('active','=',1);
        echo json_encode($seasons);
    }

    public function storeData(Request $request) {
        $v = validator($request->all() ,[
            'image' => 'image|mimes:jpeg,jpg,png,gif,pdf|max:20000',
            'name' => 'required',
            'email' => 'required|email|max:255',
            'birth' => 'required',
            'address' => 'required',
            'gender' => 'required',
            'first_day' => 'required',
            'national_id' => 'required|unique:students',
            'guardian_id' => 'required',
            'nationality' => 'required',
            'center_id' => 'required',
            'year' => 'required',
            'season_id' => 'required',
            'coursetype_id' => 'required',
            'course_id' => 'required',
            
              
            
            
        ] ,[
            'image.image' => 'من فضلك حمل صورة وليس فيديو',
            'image.mimes' => 'يرجى تحميل ملفات بصيغة  JPG,PNG,GIF,PDF',
            'image.max' => 'الحد الاقصى لحجم الملف : 20 MB',
            'name.required' => 'من فضلك أدخل اسم الطالب',
            'birth.required' => 'من فضلك أدخل تاريخ الميلاد',
            'address.required' => 'من فضلك أدخل العنوان',
            'gender.required' => 'من فضلك أدخل الجنس',
            'first_day.required' => 'من فضلك أدخل يوم الالتحاق',
            'national_id.required' => 'من فضلك أدخل الرقم المدنى',
            'national_id.unique' => 'هذا الرقم المدنى تم استخدامه من قبل',
            'center_id.required' => 'من فضلك اختر المركز',
            'guardian_id.required' => 'من فضلك اختر ولى الأمر',
            'nationality.required' => 'من فضلك أدخل الجنسية',
            'season_id.required' => 'من فضلك اختر الفصل الدراسي',
            'year.required' => 'من فضلك أدخل السنة الدراسية',
            'email.required' => 'البريد الالكتروني مطلوب',
            'email.email' => 'البريد الالكتروني غير صحيح' ,
            'course_id.required'=>'ادخل الكورس من فضلك '       
            
        ]);

        if ($v->fails()){
            return ['status' => false , 'data' => implode(PHP_EOL ,$v->errors()->all())];
        }

        $student = new Student();
        $student->student_name = $request->input('name');
        $student->birth = $request->input('birth');        
        $student->age = Carbon::parse($request->input('birth'))->age;
        $student->address = $request->input('address');
        $student->email = $request->input('email');
        $student->first_day = $request->input('first_day');
        $student->gender = $request->input('gender');
        $student->national_id = $request->input('national_id');
        $student->guardian_id = $request->input('guardian_id');
        $student->center_id = $request->input('center_id');
        
         $student->coursetype_id = $request->input('coursetype_id');
         $student->course_id = $request->input('course_id');
     
          
          
          
          
          
        $student->nationality = $request->input('nationality');
        $student->season_id = $request->input('season_id');
        $student->level_id = $request->input('level_id');
        $student->year = $request->input('year');
        $student->notes = $request->notes;
        if($request->input('transportation') != null){
            $student->transportation = 1;
        }

        $destination = storage_path('uploads/' . $request->storage);
        $image = $request->file('image');
        if ($image) {
            if (is_file($destination . "/{$image}")) {
                @unlink($destination . "/{$image}");
            }
            $imageName = $image->getClientOriginalName();
            $image->move($destination, $imageName);
            $student->image = $imageName;
        }
        if($request->active == "on"){
            $student->active = 1;
        }elseif(empty($request->active)){
            $student->active = 0;
        }
        $centers = Center::get()->where('id','=',$request->input('center_id'));
        $age = Carbon::parse($request->input('birth'))->age;
        foreach($centers as $center){
            if($center->age > $age){
                return ['status' => false ,'data' => 'حدث خطأ , سن الطالب اقل من سن القبول '];
            }
        }
        $course = Course::get()->where('id','=',$request->input('course_id'));
        //$count = Student::count();
        $count = StudentCourse::where('course_id','=',$request->input('course_id'))->count();

        
        //student_courses
        foreach($course as $c){
            if($count > $c->max_num){
                    return ['status' => false ,'data' => 'حدث خطأ , لقد تخطيت الحد الاقصى لعدد الطلاب فى الحلقة '];
            }
        }
        
        if ($student->save()){
            $archive = new StudentArchive();
            $archive->student_id = $student->id;
            $archive->center_id = $request->center_id;
            $archive->level_id = $request->level_id;
            $archive->season_id = $request->season_id;
            $archive->year = $request->year;
            $now = Carbon::now();
            $price = DB::table('seasons')
                    ->join('students','students.season_id','=','seasons.id')
                    ->where('students.id','=',$student->id)
                    ->sum('seasons.price');
            $amount = 0;
            $search = DB::table('counts')
                    ->select('*')
                    ->where('student_id','=',$student->id)
                    ->first();
        
            $prev = DB::table('counts')
                    ->where('student_id','=',$student->id)
                    ->sum('amount');
        
            $notes = 'مصروفات الطالب '.$student->student_name.'';
            if($search){
                $amount = $prev - $price;
                $data = array(
                    'amount'=>$amount,
                    'date'=>$now,
                    );
                DB::table('counts')->where('student_id',$id)->update($data);
            }else{
                $amount = $amount - $price;
                $data = array(
                    'student_id'=>$student->id,
                    'amount'=>$amount,
                    'date'=>$now,
                    'notes'=>$notes
                );
                DB::table('counts')->insert($data);
            }
                    
            $studentcourse = new StudentCourse();
            $studentcourse->course_id = $request->course_id;
            $studentcourse->student_id = $student->id;
            $studentcourse->save();
            $archive->save();
            $now = Carbon::now();
                                
            $materials = DB::table('level_materials')
                        ->join('levels','level_materials.level_id','=','levels.id')
                        ->join('materials','level_materials.material_id','=','materials.id')
                        ->select('level_materials.*','levels.level_name','materials.material_name')
                        ->where('level_materials.level_id','=',$request->level_id)
                        ->where('materials.active','=', 1)
                        ->get();
                    
            foreach($materials as $m){
                $material = new StudentMaterial();
                $studentm = $request->input('mat'.$m->material_id);
                $notes = $request->input('notes'.$m->material_id);
                if(isset($studentm)){
                    $material->student_id = $student->id;
                    $material->material_id = $m->material_id;
                    $material->status = 1;
                    $material->notes = $notes;
                }else{
                    $material->student_id = $student->id;
                    $material->material_id = $m->material_id;
                    $material->status = 0;
                    $material->notes = $notes;
                }

                $material->save();
                        
            }
            return ['status' => 'succes' ,'data' => 'تم اضافة البيانات بنجاح'];
        }else{
            return ['status' => false ,'data' => 'حدث خطأ , من فضلك أعد المحاولة '];
        }
    }

    public function getpendingadd() {
        $seasons = Season::where("active", 1)->get();
        $students = DB::table('students')
               
                ->join('seasons','students.season_id','=','seasons.id')
                ->join('centers','students.center_id','=','centers.id')
                ->select('seasons.season_name','centers.center_name','students.*','students.national_id')
                ->orderBy('id', 'asc')
             ->get();
                
               
        $guardians = Guardian::where("active", 1)->get();
        $centers = Center::where("active", 1)->get();
        $courses = Course::where("active", 1)->get();
        $levels = Level::where("active", 1)->get();
        $docs = DB::table('student_documents')
                ->join('students','student_documents.student_id','=','students.id')
                ->select('student_documents.*','students.student_name')
                ->get();
        $materials = Material::where("active", 1)->get();
        $mats = DB::table('student_materials')
                    ->join('students','student_materials.student_id','=','students.id')
                    ->join('materials','student_materials.material_id','=','materials.id')
                    ->select('student_materials.*','students.student_name','materials.material_name')
                    ->get();
        $now = Carbon::now();
        return view('admin.pages.student.pendingadd', compact('seasons','now','courses','students','docs','guardians','centers','levels','materials','mats'));
    }

    public function getpendingedit() {
        $seasons = Season::where("active", 1)->get();
        $students = DB::table('students')
                ->join('centers','centers.id','=','students.center_id')
                ->join('seasons','seasons.id','=','students.season_id')
                ->join('counts','counts.student_id','=','students.id')
                ->select('students.*','centers.center_name','seasons.season_name','counts.amount')
                ->where("students.active", 3)
                ->get();
                
              
                
        $guardians = Guardian::where("active", 1)->get();
        $centers = Center::where("active", 1)->get();
        $courses = Course::where("active", 1)->get();
        $levels = Level::where("active", 1)->get();
        $docs = DB::table('student_documents')
                ->join('students','student_documents.student_id','=','students.id')
                ->select('student_documents.*','students.student_name')
                ->get();
        $materials = Material::where("active", 1)->get();
        $mats = DB::table('student_materials')
                    ->join('students','student_materials.student_id','=','students.id')
                    ->join('materials','student_materials.material_id','=','materials.id')
                    ->select('student_materials.*','students.student_name','materials.material_name')
                    ->get();
        $now = Carbon::now();
        return view('admin.pages.student.pendingedit', compact('seasons','now','courses','students','docs','guardians','centers','levels','materials','mats'));
    }

    public function postpendingadd($id) {
        if (isset($id)) {
            $student = Student::find($id);
            $student->active = 1;
            $student->save();
            return redirect()->back();
        }
    }

    public function postpendingedit($id) {
        if (isset($id)) {
            $student = Student::find($id);
            $student->active = 1;
            $student->save();
            return redirect()->back();
        }
    }

    public function storeStudentData(Request $request) {
        $v = validator($request->all() ,[
            'image' => 'image|mimes:jpeg,jpg,png,gif,pdf|max:20000',
            'name' => 'required',
            'username' => 'required',
            'password' => 'required',
            'address' => 'required',
            'phone' => 'required',
            'whatsapp' => 'required',
            'national_id' => 'required',
            'nationality' => 'required',
            'job' => 'required',
        ] ,[
            'image.image' => 'من فضلك حمل صورة وليس فيديو',
            'image.mimes' => 'يرجى تحميل ملفات بصيغة  JPG,PNG,GIF,PDF',
            'image.max' => 'الحد الاقصى لحجم الملف : 20 MB',
            'name.required' => 'من فضلك أدخل اسم ولى الأمر',
            'username.required' => 'من فضلك أدخل اسم المستخدم',
            'password.required' => 'من فضلك أدخل كلمة السر',
            'address.required' => 'من فضلك أدخل العنوان',
            'phone.required' => 'من فضلك أدخل رقم الهاتف',
            'nationality.required' => 'من فضلك أدخل الجنسية',
            'whatsapp.required' => 'من فضلك أدخل رقم الواتس',
            'national_id.required' => 'من فضلك أدخل الرقم القومى',
            'job.required' => 'من فضلك أدخل وظيفة',
        ]);

        if ($v->fails()){
            return ['status' => false , 'data' => implode(PHP_EOL ,$v->errors()->all())];
        }
        $guardian = new Guardian();
        $member = new Member();

        $member->guardian_name = $request->name;
        $member->username = $request->username;
        $member->password = bcrypt($request->password);
        $member->recover = $request->password;
        $member->address = $request->address;
        $member->email = $request->email;
        $member->phone = $request->phone;
        $member->whatsapp = $request->whatsapp;
        $member->national_id = $request->national_id;
        $member->nationality = $request->nationality;
        $member->job = $request->job;

        $destination = storage_path('uploads/' . $request->storage);
        $image = $request->file('image');
        if ($image) {
            if (is_file($destination . "/{$image}")) {
                @unlink($destination . "/{$image}");
            }
            $imageName = $image->getClientOriginalName();
            $image->move($destination, $imageName);
            $member->image = $imageName;
            $guardian->image = $imageName;
        }

        $guardian->guardian_name = $request->name;
        $guardian->username = $request->username;
        $guardian->password = bcrypt($request->password);
        $guardian->recover = $request->password;
        $guardian->address = $request->address;
        $guardian->email = $request->email;
        $guardian->phone = $request->phone;
        $guardian->whatsapp = $request->whatsapp;
        $guardian->national_id = $request->national_id;
        $guardian->nationality = $request->nationality;
        $guardian->job = $request->job;
        if($request->active == "on"){
            $guardian->active = 1;
        }elseif(empty($request->active)){
            $guardian->active = 0;
        }

        if ($guardian->save() && $member->save()){
            $count = $request->time;
            for($i=1; $i<=$count; $i++){
                $student = new Student();

                // $destination = storage_path('uploads/' . $request->input('storage'.$i));
                // $image = $request->file('image'.$i);
                // if ($image) {
                //     if (is_file($destination . "/{$image}")) {
                //         @unlink($destination . "/{$image}");
                //     }
                //     $imageName = $image->getClientOriginalName();
                //     $image->move($destination, $imageName);
                //     $student->image = $imageName;
                // }

                $student->student_name = $request->input('name'.$i);
                $student->birth = $request->input('birth'.$i);        
                $student->age = Carbon::parse($request->input('birth'.$i))->age;
                $student->address = $request->input('address'.$i);
                $student->email = $request->input('email'.$i);
                $student->first_day = $request->input('first_day'.$i);
                $student->gender = $request->input('gender'.$i);
                $student->national_id = $request->input('national_id'.$i);
                $student->guardian_id = $guardian->id;
                $student->center_id = $request->input('center_id');
                $student->nationality = $request->input('nationality'.$i);
                $student->season_id = $request->input('season_id');
                $student->year = $request->input('year');
                $student->notes = $request->notes;
                $student->active = 1;
                if($request->input('transportation'.$i) != null){
                    $student->transportation = 1;
                }
                //$student->save();
                if($student->save()){
                    $archive = new StudentArchive();
                    $archive->student_id = $student->id;
                    $archive->center_id = $request->center_id;
                    $archive->level_id = $request->level_id;
                    $archive->season_id = $request->season_id;
                    $archive->year = $request->year;
                    $now = Carbon::now();
                    $price = DB::table('seasons')
                            ->join('students','students.season_id','=','seasons.id')
                            ->where('students.id','=',$student->id)
                            ->sum('seasons.price');
                    $amount = 0;
                    $search = DB::table('counts')
                            ->select('*')
                            ->where('student_id','=',$student->id)
                            ->first();
        
                    $prev = DB::table('counts')
                            ->where('student_id','=',$student->id)
                            ->sum('amount');
        
                    //if($prev < 0){
                    //    return ['status' => false ,'data' => 'حدث خطأ , يرجى سداد ديون الطالب '];
                    //}else{
                            
                    //}
        
                    $notes = 'مصروفات الطالب '.$student->student_name.'';
                    if($search){
                        $amount = $prev - $price;
                        $data = array(
                            'amount'=>$amount,
                            'date'=>$now,
                            );
                        DB::table('counts')->where('student_id',$id)->update($data);
                    }else{
                        $amount = $amount - $price;
                        $data = array(
                            'student_id'=>$student->id,
                            'amount'=>$amount,
                            'date'=>$now,
                            'notes'=>$notes
                            );
                        DB::table('counts')->insert($data);
                    }
                    $archive->save();
                }else{
                    return ['status' => false ,'data' => 'حدث خطأ , من فضلك أعد المحاولة '];
                }
            }
            return ['status' => 'succes' ,'data' => 'تم اضافة البيانات بنجاح'];
        }else{
            return ['status' => false ,'data' => 'حدث خطأ , من فضلك أعد المحاولة '];
        }
    }

}