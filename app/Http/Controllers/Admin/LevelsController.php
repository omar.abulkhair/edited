<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Level;
use App\Course;
use App\CourseLevel;
use App\CourseType;
use Config;
use DB;

class LevelsController extends Controller
{
    public function getIndex() {
        
      $levels = DB::table('levels')
                ->join('course_types','course_types.id','=','levels.coursetype_id')
                ->select('levels.*','course_types.type_name')
                ->where("levels.active", 1)
                ->get();
               
                
        //   $levels=Level::get(); 
       
//         $levels = DB::table('levels')
// ->join('course_types','levels.type_id','=','course_types.type_id')
// ->select('levels.*','course_types.type_name')
// ->where("levels.active", 1)
// ->select('levels.*')
// ->distinct('levels.id')
// ->get();


         
        return view('admin.pages.level.index', compact('levels'));
    }

    public function getAdd() {
        $types = CourseType::where("active", 1)->get();
        return view('admin.pages.level.add', compact('types'));
    }

    public function insert(Request $request) {
        $v = validator($request->all() ,[
            'name' => 'required',
            'coursetype_id' => 'required',
        ] ,[
            'name.required' => 'من فضلك أدخل اسم المرحلة',
            'coursetype_id.required' => 'من فضلك اختر نوع الحلقة ',
        ]);

        if ($v->fails()){
            return ['status' => false , 'data' => implode(PHP_EOL ,$v->errors()->all())];
        }

        $level = new Level();

        $level->level_name = $request->name;
        $level->coursetype_id = $request->coursetype_id;
        
        if($request->active == "on"){
            $level->active = 1;
        }elseif(empty($request->active)){
            $level->active = 0;
        }
        
        if ($level->save()){
            $courselevel = new CourseLevel();
            $courselevel->coursetype_id = $request->coursetype_id;
            $courselevel->level_id = $level->id;
            $courselevel->save();
            return ['status' => 'succes' ,'data' => 'تم اضافة البيانات بنجاح'];            
        }else{
            return ['status' => false ,'data' => 'حدث خطأ , من فضلك أعد المحاولة '];
        }
    }

    public function getEdit($id) {
        if (isset($id)) {
           // $courses = Course::where("active", 1)->get();
            $courses = CourseType::where("active", 1)->get();
            $levels = DB::table('levels')
                ->join('course_types','levels.coursetype_id','=','course_types.id')
                ->select('levels.*','course_types.type_name')
                ->where('levels.id','=',$id)
                ->get();
            return view('admin.pages.level.edit', compact('levels','courses'));
        }        
    }

    public function postEdit(Request $request,$id) {
        
        $level = Level::find($id);

        $level->level_name = $request->name;
        $level->coursetype_id = $request->coursetype_id;
        if($request->active == "on"){
            $level->active = 1;
        }elseif(empty($request->active)){
            $level->active = 0;
        }
        if ($level->save()){
            $courselevel = new CourseLevel();
            $courselevel->coursetype_id = $request->coursetype_id;
            $courselevel->level_id = $id;
            $courselevel->save();
            return ['status' => 'succes' ,'data' => 'تم تحديث البيانات بنجاح'];
        }else{
            return ['status' => false ,'data' => 'حدث خطأ , من فضلك أعد المحاولة '];
        }
    }

    public function delete($id) {
        if (isset($id)) {
            $level = Level::find($id);
            $level->delete();

            return redirect()->back();
        }
    }

    public function getcoursedata(Request $request) {
        $id = $request->input('id');
        $courses = Course::where("type_id", $id)->get();
        echo json_encode($courses);
    }

}
