<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Town;
use Config;
use DB;

class TownsController extends Controller
{
    public function getindex() {
        $towns = Town::where("active", 1)->get();
        return view('admin.pages.town.index', compact('towns'));
    }

    public function getAdd() {
        $towns = Town::where("active", 1)->get();
        return view('admin.pages.town.add', compact('towns'));
    }

    public function insert(Request $request) {
        $v = validator($request->all() ,[
            'name' => 'required',
        ] ,[
            'name.required' => 'من فضلك أدخل اسم الفصل الدراسي',
        ]);

        if ($v->fails()){
            return ['status' => false , 'data' => implode(PHP_EOL ,$v->errors()->all())];
        }

        $town = new Town();

        $town->town_name = $request->name;
        if($request->active == "on"){
            $town->active = 1;
        }elseif(empty($request->active)){
            $town->active = 0;
        }
        
        if ($town->save()){
            return ['status' => 'succes' ,'data' => 'تم اضافة البيانات بنجاح'];            
        }else{
            return ['status' => false ,'data' => 'حدث خطأ , من فضلك أعد المحاولة '];
        }
    }

    public function getEdit($id) {
        if (isset($id)) {
            $towns = Town::where("active", 1)->get();
            $town = Town::find($id);
            return view('admin.pages.town.edit', compact('town','towns'));
        }        
    }

    public function postEdit(Request $request,$id) {

        $town = Town::find($id);
        $town->town_name = $request->name;
        if($request->active == "on"){
            $town->active = 1;
        }elseif(empty($request->active)){
            $town->active = 0;
        }

        if ($town->save()){
            return ['status' => 'succes' ,'data' => 'تم تحديث البيانات بنجاح'];
        }else{
            return ['status' => false ,'data' => 'حدث خطأ , من فضلك أعد المحاولة '];
        }
    }

    public function delete($id) {
        
            $town = Town::find($id);
            $town->delete();
            DB::table('centers')->where('town_id','=', $id)->delete();

            return redirect()->back();
        
    }

}
