<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Percent;
use Illuminate\Http\Request;
use App\Level;
use App\Salary;
use App\Center;
use App\Absent;
use App\Attend;
use App\Student;
use App\Count;
use App\Teacher;
use App\Part;
use Config;
use DB;


class ReportsController extends Controller
{
    public function absent() {
         $students = DB::table('absents')
                 ->join('students','absents.student_id','=','students.id')
                ->join('courses','absents.course_id','=','courses.id')
                ->join('centers','absents.center_id','=','centers.id')
                ->select('students.student_name','courses.course_name','centers.center_name','absents.*','students.national_id')
                ->orderBy('id', 'asc')
             ->get();
        
    //  $students=Absent::all();

        return view('admin.pages.reports.absent', compact('students'));
    }

    public function counts() {
        $students = DB::table('payments')
                ->join('students','payments.student_id','=','students.id')
                ->select('students.student_name','payments.*','students.national_id')
                ->orderBy('id', 'asc')
                ->get();

//   $students=count::orderBy('id', 'asc')->get();


        return view('admin.pages.reports.counts', compact('students'));
    }

    public function Scounts() {
        $students = DB::table('counts')
                ->join('students','counts.student_id','=','students.id')
                ->select('students.student_name','counts.*','students.national_id')
                ->orderBy('id', 'asc')
                ->get();
        
       

        return view('admin.pages.reports.scounts', compact('students'));
    }

    public function grades() {
        $students = DB::table('student_grades')
                ->join('students','student_grades.student_id','=','students.id')
                ->join('materials','student_grades.material_id','=','materials.id')
                ->join('centers','student_grades.center_id','=','centers.id')
                ->select('students.student_name','centers.center_name','materials.material_name','student_grades.*','students.national_id')
                ->orderBy('id', 'asc')
                ->get();

        return view('admin.pages.reports.grades', compact('students'));
    }

    public function attend() {
       $attends = DB::table('attend')
                ->join('teachers','attend.teacher_id','=','teachers.id')
               ->select('teachers.teacher_name','attend.*','teachers.national_id')
               ->orderBy('id', 'asc')
               ->get();
        // $attends=Attend::get()->all();
        

        return view('admin.pages.reports.attend', compact('attends'));
    }

    public function salariesReport() {
        $salaries = DB::table('salaries')
                ->join('teachers','salaries.teacher_id','=','teachers.id')
                ->select('salaries.*','teachers.teacher_name','teachers.national_id')
                ->where('status', 1)
                ->get();
        return view('admin.pages.reports.salaries', compact('salaries'));
    }

    public function store() {
        $counts = DB::table('payments')
                ->join('students','payments.student_id','=','students.id')
                ->select('students.student_name','payments.*','students.national_id')
                ->where('amount','>', 0)
                ->orderBy('id', 'asc')
                ->get();

        $total1 = DB::table('payments')
                ->select('amount')
                ->where('amount','>', 0)
                ->sum('amount');

        $salaries = DB::table('salaries')
                ->join('teachers','salaries.teacher_id','=','teachers.id')
                ->select('teachers.teacher_name','salaries.*','teachers.national_id')
                ->where('status','=', 1)
                ->orderBy('id', 'asc')
                ->get();

        $total2 = DB::table('salaries')
                ->select('final')
                ->where('status','=', 1)
                ->sum('final');

        return view('admin.pages.store', compact('counts','salaries','total1','total2'));
    }
    
    
    public function students(){
        
       $centers=Center::with('coursetypes')->get();

       
       return view('admin.pages.reports.students', compact('centers')); 
        
  
    }
    public function levels(){
        
    $levels=Level::join('students','levels.id','=','students.level_id')
                ->select('students.gender','levels.*','students.national_id')
                
                ->orderBy('id', 'asc')
                ->get();
  
    
      return view('admin.pages.reports.levels', compact('levels'));    
    }
    
    public function search(Request $request){
      $search=$request->get('search');  
      
      $levels=Level::where('name','like','%'.$search.'%')->paginate(5);
        
       return view('admin.pages.reports.levels', compact('levels'));  
    }
    
    
    public function teacherss(){
        
        
        $teachers=Teacher::with('center')->get();
        
        return view('admin.pages.reports.teacher', compact('teachers'));  
        
    }
    
    
    public function Exemptstudent(){
        
        
      $students=Student::with('center')->get();
      
      
                return view('admin.pages.reports.Exemptstudent', compact('students')); 
        
        
    }
    
    
    public function studentss(){
        
      $students=Student::with('center')->get();
      
       return view('admin.pages.reports.studentss', compact('students')); 
        
    }
    
    
    
    
    public function studentcourse(){
        
          $students=Student::join('teachers','students.id','=','teachers.center_id')
                ->select('teachers.teacher_name','students.*','teachers.national_id')
                
                ->orderBy('id', 'asc')
                ->get();
   return view('admin.pages.reports.studentcourse', compact('students')); 
        
        
    }
    
    public function datacenter(){
        
        
             $centers=Center::get();
        
            return view('admin.pages.reports.datacenter', compact('centers')); 
        
        
        
    }
    
    
    
    public function levelstudents(){
        
        
      $students=Student::with('center')->get();
      
         return view('admin.pages.reports.levelstudents', compact('students')); 
        
    }
    
    
    public function studentdatacourse(){
        
       $students=Student::join('teachers','students.id','=','teachers.center_id')
                ->select('teachers.teacher_name','students.*','teachers.national_id')
                
                ->orderBy('id', 'asc')
                ->get();
   return view('admin.pages.reports.studentdatacourse', compact('students')); 
        
        
    }
    
    
    
    public function parts(){

    
    $parts=DB::table('percents')
         ->join('centers','percents.id','=','centers.id')
        ->join('materials','percents.material_id','=','materials.id')
        ->select('centers.center_name','percents.*','materials.material_name','materials.success')

        ->orderBy('id', 'asc')
        ->get();
    
     return view('admin.pages.reports.parts', compact('parts')); 
    
        
        
    }
    
    public function coursebookstudent(){
        
     $students=Student::with(['level','part'])->get(); 
    
        
        return view('admin.pages.reports.coursebookstudent', compact('students')); 
    }
    
    public function coursereservation(){
        
          $students=Student::get(); 
    
        
        return view('admin.pages.reports.coursereservation', compact('students'));  
        
    }
    
    
    public function financialclaims(){
        
        $students=Student::with('guardian')->get(); 
    
        
        return view('admin.pages.reports.financialclaims', compact('students'));    
        
    }
    
    
    public function bookstudents(){
        
        
          $students=Student::with('level')->get(); 
    
        
        return view('admin.pages.reports.bookstudents', compact('students'));   
        
        
    }
    
    
    
    public function differentparties(){
        
        
        
         $students=Student::with('center')->get(); 
    
        
        return view('admin.pages.reports.differentpart', compact('students'));     
        
        
        
        
    }
    
    
    
    public function passlevel(){
        
     
         $students=Student::with('center')->get(); 
    
        
        return view('admin.pages.reports.passlevel', compact('students'));   
        
        
        
    }
    
    
   
     
    public function passlevels(){
        
     
         $students=Student::with('center')->get(); 
    
        
        return view('admin.pages.reports.passlevels', compact('students'));   
        
        
        
    }
    
    
        public function  finaltest(){
        
     
         $students=Student::with('center')->get(); 
    
        
        return view('admin.pages.reports.finaltest', compact('students'));   
        
        
        
    }
    
     public function  finaltests(){
        
     
         $students=Student::with('center')->get(); 
    
        
        return view('admin.pages.reports.finaltests', compact('students'));   
        
        
        
    }
    
    
    public function graduation(){
        
           
         $students=Student::with('center')->get(); 
    
        
        return view('admin.pages.reports.graduation', compact('students'));     
        
    }
      
     public function graduations(){
        
           
         $students=Student::with('center')->get(); 
    
        
        return view('admin.pages.reports.graduations', compact('students'));     
        
    }
    
    
    public function skilldegree(){
        
           $students=Student::with('center')->get(); 
    
        
        return view('admin.pages.reports.degree', compact('students'));   
        
    }
    
    public function stats(){
        
        
        $students=Student::join('seasons','students.season_id','=','seasons.id')
                ->select('seasons.season_name','students.*','seasons.season_name')
                
                ->orderBy('id', 'asc')
                ->get();
   return view('admin.pages.reports.course', compact('students'));  
        
        
        
    }
    
    
    public function getEdit($id){
         if (isset($id)) {
             
             
            $students=Student::find($id);
            
               return view('admin.pages.reports.passlevel', compact('students'));    
             
             
         }
        
        
    }
    
    public function getprint($id){
        
              if (isset($id)) {
             
             
            $students=Student::find($id);
            
               return view('admin.pages.reports.graduation', compact('students'));    
             
             
         }
        
        
    }
    
    
        public function getprints($id){
        
              if (isset($id)) {
             
             
            $students=Student::find($id);
            
               return view('admin.pages.reports.finaltests', compact('students'));    
             
             
         }
        
        
    }
    
         public function getprintss($id){
        
              if (isset($id)) {
             
             
            $students=Student::find($id);
            
               return view('admin.pages.reports.skilldegree', compact('students'));    
             
             
         }
        
        
    }
    
    
    
    public function graduationss(){
        
     
        $students=Student::join('seasons','students.season_id','=','seasons.id')
                ->select('seasons.season_name','students.*','seasons.season_name')
                
                ->orderBy('id', 'asc')
                ->get();
   return view('admin.pages.reports.guaration', compact('students'));     
        
    }
       

}

