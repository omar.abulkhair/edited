<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Notifications\SendNewUserInfoMail;
use App\Permission;
use App\Role;
use App\User;
use Illuminate\Http\Request;

class PermissionController extends Controller
{
    public function get(Request $request)
    {

        return view('admin.pages.desigh');


    }


    public function insert(Request $request)
    {

        $user = new User;

        $user->username = $request->username;

        $user->email = $request->email;

        $user->password = bcrypt($request->password);
        $role = Role::findOrFail($request->rolelist);
        $p = [];
        foreach ($request->permission as $perm) {
            $permission = Permission::findOrFail($perm);
            $p[] = $perm;
            //$role->attachPermission(($perm));
        }
        $user->permmisions = serialize($p);



        $userSaved = $user->save();
        $user->roles()->attach($role);
        $user->roles()->attach($user->id);
        if ($userSaved) {
            return ['status' => 'succes', 'data' => 'تم اضافة الصلاحيه بنجاح'];
            $user->notify(new SendNewUserInfoMail($request->password));

        } else

            return ['status' => false, 'data' => 'حدث خطأ , من فضلك أعد المحاولة '];
    }


}
