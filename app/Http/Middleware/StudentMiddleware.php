<?php

namespace App\Http\Middleware;

use App\Role;
use App\User;
use Closure;

class StudentMiddleware
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        $checkUser = auth('admins')->check();
        if ($checkUser) {
            if ($this->checkIfUserIsStudent(auth('admins')->user())) {
                return redirect()->route('admin.home');
            }
        }
        return redirect()->guest('admin/auth');
    }

    /**
     * @param User $user
     * @return mixed
     */
    private function checkIfUserIsStudent($user) {
        $role = Role::findOrFail(1);
        return $user->hasRole($role);
    }
}
