<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Level extends Model
{
    protected $fillable = ['level_name','course_id','type_id','active'];
    
    

 
  public function materials(){
        return $this->hasMany(Material::class);
    }
 
  public function coursetype(){
        return $this->belongsTo(CourseType::class,'coursetype_id');
    }
 
 
  public function students(){
        return $this->hasMany(Student::class);
    }
}
