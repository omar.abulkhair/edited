<?php

namespace App;

use Illuminate\Notifications\Notifiable;
use Illuminate\Foundation\Auth\User as Authenticatable;

class Guardian extends Authenticatable
{
    use Notifiable;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = ['guardian_name','username','email' ,'phone','phone2' ,'job','whatsapp','image','national_id','address','nationality','active'];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     * 
     * 
     * 
     * 
     */
     
     
     
      public function students(){
        return $this->hasMany(Student::class );
    }
     
     
     
     
     
     
     
     
     
     
     
     
     
    protected $hidden = [
        'password'
    ];
}
