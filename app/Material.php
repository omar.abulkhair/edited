<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Material extends Model
{
    protected $fillable = ['success' ,'p1','p2','p3','p4','p5' ,'level_id'];

    public function details(){
        return $this->hasMany(CourseMaterial::class ,'material_id');
    }

    public function teachers(){
        return $this->hasMany(TeacherMaterial::class ,'material_id');
    }
       public function course(){
        return $this->belongto(Course::class );
    }
    
      public function level(){
        return $this->belongsTo(Level::class ,'level_id');
    }
    
      public function students(){
        return $this->hasMany(Student::class);
    }
    
    
     public function Parts(){
        return $this->hasMany(Part::class );
    }



    public function percents(){
        return $this->hasMany(Percent::class );
    }
}
