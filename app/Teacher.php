<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Teacher extends Model
{
    protected $fillable = ['teacher_name','username','password','recover','birth','address','email' ,'phone', 'job', 'job_id', 'qualifications','image','salary','first_day','national_id','hours','code','active'];
    
    public function details(){
        return $this->hasMany(TeacherMaterial::class ,'teacher_id');
    }

    public function levels(){
        return $this->hasMany(TeacherLevel::class ,'teacher_id');
    }
    
      public function attend(){
        return $this->belongto(Attend::class);
    }
    
      public function course(){
        return $this->belongto(Course::class );
    }
   
   
       public function job(){
        return $this->belongsTo(Job::class ,'job_id');
    }
   
       public function center(){
        return $this->belongsTo(Center::class,'center_id' );
    }
    
        public function coursetype(){
        return $this->belongsTo(CourseType::class,'coursetype_id' );
    }
}
