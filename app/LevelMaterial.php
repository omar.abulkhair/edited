<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class LevelMaterial extends Model
{
    protected $fillable = ['course_id' ,'material_id'];

    public function material(){
        return $this->belongsTo(Material::class ,'material_id');
    }

    public function level(){
        return $this->belongsTo(Level::class ,'level_id');
    }
}
