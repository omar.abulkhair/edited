<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class StudentMaterial extends Model
{
    protected $fillable = ['student_id' ,'material_id','status'];

}
