<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class StudentArchive extends Model
{
    protected $fillable = ['center_id' ,'level_id','season_id','student_id','year'];

    public function center(){
        return $this->belongsTo(Center::class ,'center_id');
    }

    public function level(){
        return $this->belongsTo(Level::class ,'level_id');
    }

    public function season(){
        return $this->belongsTo(Season::class ,'season_id');
    }

    public function student(){
        return $this->belongsTo(Student::class ,'student_id');
    }
}
