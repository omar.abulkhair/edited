<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class MaterialDocument extends Model
{
    protected $fillable = ['file'];

}
