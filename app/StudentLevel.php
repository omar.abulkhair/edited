<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class StudentLevel extends Model
{
    protected $fillable = ['student_id' ,'level_id'];

    public function level(){
        return $this->belongsTo(Level::class ,'level_id');
    }

    public function student(){
        return $this->belongsTo(Student::class ,'student_id');
    }
}
