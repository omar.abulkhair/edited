<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Season extends Model
{
    protected $fillable = ['season_name','price','season_year','coursetype_id','active'];
    
    
    public function CourseType(){
        return $this->belongsTo(CourseType::class ,'coursetype_id');
    }
    
      public function students(){
        return $this->hasMany(Student::class );
    }
}
