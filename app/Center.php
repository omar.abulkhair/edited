<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Center extends Model
{
    protected $fillable = ['center_name' ,'address' ,'manager','head_manager','business_registration' ,'tax_card' ,'phone','fax','email','website','active'];


   public function Attends(){
        return $this->hasMany(Attend::class );
    }
    
 
    
 public function Absents(){
        return $this->hasMany(Absent::class );
    }
    
     public function students(){
        return $this->hasMany(Student::class );
    }
     public function coursetypes (){
        return $this->hasMany('App\CourseType');
    }
    
                                                                                                                        
      public function courses(){
        return $this->hasMany(Course::class );
    }
    
      public function teachers (){
        return $this->hasMany('App\Teacher');
    }
    
       public function town (){
        return $this->belongsTo('App\Town');
    }
}
