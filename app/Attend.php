<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Attend extends Model
{
      protected $table = 'attend';
    public $timestamps = true;
    
    protected $fillable = ['student_id' ,'teacher_id' ,'date' ,'days','month','year','attends','leaves'];

    public function students(){
        return $this->hasMany(Student::class ,'student_id');
    }
    
      public function teachers(){
        return $this->hasMany(Teacher::class );
    }
    
    
       public function center(){
        return $this->belongto(Center::class ,'center_id');
    }
}
