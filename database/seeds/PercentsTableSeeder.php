<?php

use Illuminate\Database\Seeder;

class PercentsTableSeeder extends Seeder
{

    /**
     * Auto generated seed file
     *
     * @return void
     */
    public function run()
    {
        

        \DB::table('percents')->delete();
        
        \DB::table('percents')->insert(array (
            0 => 
            array (
                'id' => 28,
                'percent_name' => 'جزء 3 08-12-2019',
                'grade' => 40,
                'material_id' => 33,
                'created_at' => '2019-12-08 07:44:34',
                'updated_at' => '2019-12-08 07:44:34',
            ),
            1 => 
            array (
                'id' => 27,
                'percent_name' => 'جزء 2 08-12-2019',
                'grade' => 40,
                'material_id' => 33,
                'created_at' => '2019-12-08 07:44:12',
                'updated_at' => '2019-12-08 07:44:12',
            ),
            2 => 
            array (
                'id' => 26,
                'percent_name' => 'جزء 1 8-12-2019',
                'grade' => 40,
                'material_id' => 33,
                'created_at' => '2019-12-08 07:43:50',
                'updated_at' => '2019-12-08 07:43:50',
            ),
            3 => 
            array (
                'id' => 25,
                'percent_name' => 'من الايه ١ الي ٢٠',
                'grade' => 50,
                'material_id' => 32,
                'created_at' => '2019-12-05 18:24:39',
                'updated_at' => '2019-12-05 18:24:39',
            ),
            4 => 
            array (
                'id' => 24,
                'percent_name' => '10',
                'grade' => 12,
                'material_id' => 31,
                'created_at' => '2019-12-05 09:44:05',
                'updated_at' => '2019-12-05 09:44:05',
            ),
            5 => 
            array (
                'id' => 23,
                'percent_name' => 'الجزء الاول',
                'grade' => 50,
                'material_id' => 29,
                'created_at' => '2019-12-04 18:22:52',
                'updated_at' => '2019-12-04 18:22:52',
            ),
            6 => 
            array (
                'id' => 22,
                'percent_name' => 'الجزء الاول',
                'grade' => 10,
                'material_id' => 28,
                'created_at' => '2019-12-04 14:32:40',
                'updated_at' => '2019-12-04 14:32:40',
            ),
            7 => 
            array (
                'id' => 21,
                'percent_name' => 'الثلاثين',
                'grade' => 25,
                'material_id' => 1,
                'created_at' => '2019-12-03 15:01:24',
                'updated_at' => '2019-12-03 15:01:24',
            ),
        ));
        
        
    }
}