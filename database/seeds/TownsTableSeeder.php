<?php

use Illuminate\Database\Seeder;

class TownsTableSeeder extends Seeder
{

    /**
     * Auto generated seed file
     *
     * @return void
     */
    public function run()
    {
        

        \DB::table('towns')->delete();
        
        \DB::table('towns')->insert(array (
            0 => 
            array (
                'id' => 27,
                'town_name' => 'محافظه الفيوم1',
                'active' => 1,
                'created_at' => '2019-12-09 09:39:36',
                'updated_at' => '2019-12-09 09:39:52',
            ),
            1 => 
            array (
                'id' => 26,
                'town_name' => 'محافظة 08/12/2019',
                'active' => 1,
                'created_at' => '2019-12-08 07:24:33',
                'updated_at' => '2019-12-08 07:24:33',
            ),
            2 => 
            array (
                'id' => 25,
                'town_name' => 'الجهراء',
                'active' => 1,
                'created_at' => '2019-12-05 18:11:47',
                'updated_at' => '2019-12-05 18:11:47',
            ),
            3 => 
            array (
                'id' => 23,
                'town_name' => 'الاحمدي',
                'active' => 1,
                'created_at' => '2019-12-04 18:17:26',
                'updated_at' => '2019-12-04 18:17:26',
            ),
            4 => 
            array (
                'id' => 22,
                'town_name' => 'محافظه الدقهليه',
                'active' => 1,
                'created_at' => '2019-12-04 14:10:02',
                'updated_at' => '2019-12-04 14:10:02',
            ),
            5 => 
            array (
                'id' => 1,
                'town_name' => 'مبارك الكبير',
                'active' => 1,
                'created_at' => '2019-12-03 14:52:31',
                'updated_at' => '2019-12-03 14:52:31',
            ),
            6 => 
            array (
                'id' => 2,
                'town_name' => 'محافظه القليوبيه',
                'active' => 1,
                'created_at' => '2019-12-03 14:27:12',
                'updated_at' => '2019-12-04 14:10:27',
            ),
        ));
        
        
    }
}