<?php

use Illuminate\Database\Seeder;

class CourseLevelsTableSeeder extends Seeder
{

    /**
     * Auto generated seed file
     *
     * @return void
     */
    public function run()
    {
        

        \DB::table('course_levels')->delete();
        
        \DB::table('course_levels')->insert(array (
            0 => 
            array (
                'id' => 78,
                'course_id' => NULL,
                'coursetype_id' => 58,
                'level_id' => 55,
                'created_at' => '2019-12-09 09:46:58',
                'updated_at' => '2019-12-09 09:46:58',
            ),
            1 => 
            array (
                'id' => 77,
                'course_id' => NULL,
                'coursetype_id' => 59,
                'level_id' => 55,
                'created_at' => '2019-12-09 09:46:41',
                'updated_at' => '2019-12-09 09:46:41',
            ),
            2 => 
            array (
                'id' => 76,
                'course_id' => NULL,
                'coursetype_id' => 56,
                'level_id' => 54,
                'created_at' => '2019-12-08 07:37:33',
                'updated_at' => '2019-12-08 07:37:33',
            ),
            3 => 
            array (
                'id' => 75,
                'course_id' => NULL,
                'coursetype_id' => 55,
                'level_id' => 53,
                'created_at' => '2019-12-05 18:15:32',
                'updated_at' => '2019-12-05 18:15:32',
            ),
            4 => 
            array (
                'id' => 74,
                'course_id' => NULL,
                'coursetype_id' => 46,
                'level_id' => 52,
                'created_at' => '2019-12-05 09:36:48',
                'updated_at' => '2019-12-05 09:36:48',
            ),
            5 => 
            array (
                'id' => 73,
                'course_id' => NULL,
                'coursetype_id' => 52,
                'level_id' => 52,
                'created_at' => '2019-12-05 09:36:31',
                'updated_at' => '2019-12-05 09:36:31',
            ),
            6 => 
            array (
                'id' => 72,
                'course_id' => NULL,
                'coursetype_id' => 4,
                'level_id' => 1,
                'created_at' => '2019-12-05 09:36:15',
                'updated_at' => '2019-12-05 09:36:15',
            ),
            7 => 
            array (
                'id' => 71,
                'course_id' => NULL,
                'coursetype_id' => 48,
                'level_id' => 51,
                'created_at' => '2019-12-05 09:35:54',
                'updated_at' => '2019-12-05 09:35:54',
            ),
            8 => 
            array (
                'id' => 70,
                'course_id' => NULL,
                'coursetype_id' => 5,
                'level_id' => 50,
                'created_at' => '2019-12-05 09:35:30',
                'updated_at' => '2019-12-05 09:35:30',
            ),
            9 => 
            array (
                'id' => 69,
                'course_id' => NULL,
                'coursetype_id' => 2,
                'level_id' => 50,
                'created_at' => '2019-12-05 09:35:10',
                'updated_at' => '2019-12-05 09:35:10',
            ),
            10 => 
            array (
                'id' => 68,
                'course_id' => NULL,
                'coursetype_id' => 52,
                'level_id' => 49,
                'created_at' => '2019-12-04 18:20:26',
                'updated_at' => '2019-12-04 18:20:26',
            ),
            11 => 
            array (
                'id' => 67,
                'course_id' => 1,
                'coursetype_id' => 1,
                'level_id' => 1,
                'created_at' => '2019-12-03 14:57:09',
                'updated_at' => '2019-12-03 14:57:09',
            ),
            12 => 
            array (
                'id' => 66,
                'course_id' => 1,
                'coursetype_id' => 42,
                'level_id' => 47,
                'created_at' => '2019-12-03 14:43:05',
                'updated_at' => '2019-12-03 14:43:05',
            ),
            13 => 
            array (
                'id' => 65,
                'course_id' => 2,
                'coursetype_id' => 42,
                'level_id' => 47,
                'created_at' => '2019-12-03 14:42:52',
                'updated_at' => '2019-12-03 14:42:52',
            ),
            14 => 
            array (
                'id' => 39,
                'course_id' => 1,
                'coursetype_id' => 1,
                'level_id' => 30,
                'created_at' => '2019-11-16 20:13:09',
                'updated_at' => '2019-11-16 20:13:09',
            ),
            15 => 
            array (
                'id' => 38,
                'course_id' => 1,
                'coursetype_id' => 1,
                'level_id' => 29,
                'created_at' => '2019-11-16 20:12:21',
                'updated_at' => '2019-11-16 20:12:21',
            ),
            16 => 
            array (
                'id' => 37,
                'course_id' => 2,
                'coursetype_id' => 1,
                'level_id' => 28,
                'created_at' => '2019-11-16 20:00:50',
                'updated_at' => '2019-11-16 20:00:50',
            ),
            17 => 
            array (
                'id' => 36,
                'course_id' => 1,
                'coursetype_id' => 1,
                'level_id' => 27,
                'created_at' => '2019-11-14 10:20:25',
                'updated_at' => '2019-11-14 10:20:25',
            ),
            18 => 
            array (
                'id' => 35,
                'course_id' => 1,
                'coursetype_id' => 1,
                'level_id' => 27,
                'created_at' => '2019-11-13 13:49:35',
                'updated_at' => '2019-11-13 13:49:35',
            ),
            19 => 
            array (
                'id' => 34,
                'course_id' => 2,
                'coursetype_id' => 2,
                'level_id' => 26,
                'created_at' => '2019-11-13 13:48:15',
                'updated_at' => '2019-11-13 13:48:15',
            ),
            20 => 
            array (
                'id' => 33,
                'course_id' => 1,
                'coursetype_id' => 1,
                'level_id' => 24,
                'created_at' => '2019-11-13 13:47:38',
                'updated_at' => '2019-11-13 13:47:38',
            ),
            21 => 
            array (
                'id' => 32,
                'course_id' => 2,
                'coursetype_id' => 1,
                'level_id' => 24,
                'created_at' => '2019-11-13 13:37:17',
                'updated_at' => '2019-11-13 13:37:17',
            ),
            22 => 
            array (
                'id' => 31,
                'course_id' => 1,
                'coursetype_id' => 1,
                'level_id' => 24,
                'created_at' => '2019-11-13 13:24:36',
                'updated_at' => '2019-11-13 13:24:36',
            ),
            23 => 
            array (
                'id' => 30,
                'course_id' => 1,
                'coursetype_id' => 1,
                'level_id' => 25,
                'created_at' => '2019-11-12 14:00:38',
                'updated_at' => '2019-11-12 14:00:38',
            ),
            24 => 
            array (
                'id' => 29,
                'course_id' => 1,
                'coursetype_id' => 2,
                'level_id' => 25,
                'created_at' => '2019-11-12 13:59:48',
                'updated_at' => '2019-11-12 13:59:48',
            ),
            25 => 
            array (
                'id' => 28,
                'course_id' => 1,
                'coursetype_id' => 1,
                'level_id' => 24,
                'created_at' => '2019-11-12 13:54:32',
                'updated_at' => '2019-11-12 13:54:32',
            ),
            26 => 
            array (
                'id' => 27,
                'course_id' => 1,
                'coursetype_id' => 2,
                'level_id' => 23,
                'created_at' => '2019-11-12 13:53:55',
                'updated_at' => '2019-11-12 13:53:55',
            ),
            27 => 
            array (
                'id' => 26,
                'course_id' => 2,
                'coursetype_id' => 1,
                'level_id' => 22,
                'created_at' => '2019-11-12 13:52:51',
                'updated_at' => '2019-11-12 13:52:51',
            ),
            28 => 
            array (
                'id' => 25,
                'course_id' => 1,
                'coursetype_id' => 1,
                'level_id' => 21,
                'created_at' => '2019-11-07 16:22:53',
                'updated_at' => '2019-11-07 16:22:53',
            ),
            29 => 
            array (
                'id' => 22,
                'course_id' => 2,
                'coursetype_id' => 3,
                'level_id' => 19,
                'created_at' => '2019-11-03 15:22:02',
                'updated_at' => '2019-11-03 15:22:02',
            ),
            30 => 
            array (
                'id' => 21,
                'course_id' => 2,
                'coursetype_id' => 2,
                'level_id' => 14,
                'created_at' => '2019-11-03 15:21:35',
                'updated_at' => '2019-11-03 15:21:35',
            ),
            31 => 
            array (
                'id' => 20,
                'course_id' => 1,
                'coursetype_id' => 2,
                'level_id' => 18,
                'created_at' => '2019-10-20 13:34:37',
                'updated_at' => '2019-10-20 13:34:37',
            ),
            32 => 
            array (
                'id' => 19,
                'course_id' => 1,
                'coursetype_id' => 2,
                'level_id' => 17,
                'created_at' => '2019-10-20 13:34:14',
                'updated_at' => '2019-10-20 13:34:14',
            ),
            33 => 
            array (
                'id' => 18,
                'course_id' => 1,
                'coursetype_id' => 1,
                'level_id' => 16,
                'created_at' => '2019-10-20 13:32:27',
                'updated_at' => '2019-10-20 13:32:27',
            ),
            34 => 
            array (
                'id' => 17,
                'course_id' => 1,
                'coursetype_id' => 1,
                'level_id' => 15,
                'created_at' => '2019-10-20 13:28:51',
                'updated_at' => '2019-10-20 13:28:51',
            ),
            35 => 
            array (
                'id' => 16,
                'course_id' => 1,
                'coursetype_id' => 1,
                'level_id' => 14,
                'created_at' => '2019-10-20 13:27:45',
                'updated_at' => '2019-10-20 13:27:45',
            ),
            36 => 
            array (
                'id' => 15,
                'course_id' => NULL,
                'coursetype_id' => 1,
                'level_id' => 13,
                'created_at' => '2019-10-17 13:05:41',
                'updated_at' => '2019-10-17 13:05:41',
            ),
            37 => 
            array (
                'id' => 14,
                'course_id' => NULL,
                'coursetype_id' => 3,
                'level_id' => 12,
                'created_at' => '2019-10-17 09:33:54',
                'updated_at' => '2019-10-17 09:33:54',
            ),
            38 => 
            array (
                'id' => 13,
                'course_id' => NULL,
                'coursetype_id' => 2,
                'level_id' => 11,
                'created_at' => '2019-10-17 09:32:02',
                'updated_at' => '2019-10-17 09:32:02',
            ),
            39 => 
            array (
                'id' => 12,
                'course_id' => NULL,
                'coursetype_id' => 2,
                'level_id' => 8,
                'created_at' => '2019-10-17 09:31:21',
                'updated_at' => '2019-10-17 09:31:21',
            ),
            40 => 
            array (
                'id' => 11,
                'course_id' => NULL,
                'coursetype_id' => 1,
                'level_id' => 8,
                'created_at' => '2019-10-17 09:31:05',
                'updated_at' => '2019-10-17 09:31:05',
            ),
            41 => 
            array (
                'id' => 10,
                'course_id' => NULL,
                'coursetype_id' => 1,
                'level_id' => 10,
                'created_at' => '2019-10-17 09:28:14',
                'updated_at' => '2019-10-17 09:28:14',
            ),
            42 => 
            array (
                'id' => 9,
                'course_id' => NULL,
                'coursetype_id' => 2,
                'level_id' => 9,
                'created_at' => '2019-10-17 09:28:04',
                'updated_at' => '2019-10-17 09:28:04',
            ),
            43 => 
            array (
                'id' => 8,
                'course_id' => NULL,
                'coursetype_id' => 1,
                'level_id' => 8,
                'created_at' => '2019-10-17 08:57:37',
                'updated_at' => '2019-10-17 08:57:37',
            ),
            44 => 
            array (
                'id' => 7,
                'course_id' => NULL,
                'coursetype_id' => 0,
                'level_id' => 7,
                'created_at' => '2019-10-16 18:56:19',
                'updated_at' => '2019-10-16 18:56:19',
            ),
            45 => 
            array (
                'id' => 23,
                'course_id' => NULL,
                'coursetype_id' => 2,
                'level_id' => 14,
                'created_at' => '2019-11-04 13:55:49',
                'updated_at' => '2019-11-04 13:55:49',
            ),
            46 => 
            array (
                'id' => 5,
                'course_id' => NULL,
                'coursetype_id' => 0,
                'level_id' => 5,
                'created_at' => '2019-10-16 18:50:40',
                'updated_at' => '2019-10-16 18:50:40',
            ),
            47 => 
            array (
                'id' => 4,
                'course_id' => 7,
                'coursetype_id' => 0,
                'level_id' => 4,
                'created_at' => '2019-09-26 14:14:01',
                'updated_at' => '2019-09-26 14:14:01',
            ),
            48 => 
            array (
                'id' => 3,
                'course_id' => 7,
                'coursetype_id' => 0,
                'level_id' => 3,
                'created_at' => '2019-09-26 14:13:24',
                'updated_at' => '2019-09-26 14:13:24',
            ),
            49 => 
            array (
                'id' => 2,
                'course_id' => 7,
                'coursetype_id' => 0,
                'level_id' => 2,
                'created_at' => '2019-09-26 14:12:36',
                'updated_at' => '2019-09-26 14:12:36',
            ),
            50 => 
            array (
                'id' => 24,
                'course_id' => NULL,
                'coursetype_id' => 1,
                'level_id' => 20,
                'created_at' => '2019-11-04 13:56:21',
                'updated_at' => '2019-11-04 13:56:21',
            ),
        ));
        
        
    }
}