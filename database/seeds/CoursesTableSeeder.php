<?php

use Illuminate\Database\Seeder;

class CoursesTableSeeder extends Seeder
{

    /**
     * Auto generated seed file
     *
     * @return void
     */
    public function run()
    {
        

        \DB::table('courses')->delete();
        
        \DB::table('courses')->insert(array (
            0 => 
            array (
                'id' => 1,
                'course_name' => 'حلقه الاول',
                'max_num' => 45,
                'time' => NULL,
                'center_id' => 1,
                'coursetype_id' => 1,
                'active' => 1,
                'created_at' => '2019-12-03 15:06:52',
                'updated_at' => '2019-12-03 15:06:52',
                'type_id' => 1,
            ),
            1 => 
            array (
                'id' => 2,
                'course_name' => 'mahmoud shennawy11',
                'max_num' => 20,
                'time' => NULL,
                'center_id' => 2,
                'coursetype_id' => 50,
                'active' => 1,
                'created_at' => '2019-09-16 16:01:02',
                'updated_at' => '2019-11-17 16:51:45',
                'type_id' => 1,
            ),
            2 => 
            array (
                'id' => 8,
                'course_name' => 'المتقدمين الأولى - فترة ثانية',
                'max_num' => 12,
                'time' => NULL,
                'center_id' => 3,
                'coursetype_id' => 3,
                'active' => 1,
                'created_at' => '2019-09-16 16:04:02',
                'updated_at' => '2019-09-16 16:04:02',
                'type_id' => 1,
            ),
            3 => 
            array (
                'id' => 10,
                'course_name' => 'لالالالالا',
                'max_num' => 23,
                'time' => NULL,
                'center_id' => 2,
                'coursetype_id' => 62,
                'active' => 1,
                'created_at' => '2019-11-04 12:19:05',
                'updated_at' => '2019-11-17 16:26:57',
                'type_id' => 1,
            ),
            4 => 
            array (
                'id' => 11,
                'course_name' => 'name222',
                'max_num' => 23,
                'time' => NULL,
                'center_id' => 2,
                'coursetype_id' => 3,
                'active' => 1,
                'created_at' => '2019-11-04 12:19:05',
                'updated_at' => '2019-11-17 16:34:40',
                'type_id' => 1,
            ),
            5 => 
            array (
                'id' => 12,
                'course_name' => 'حلقه الاول',
                'max_num' => 45,
                'time' => NULL,
                'center_id' => 5,
                'coursetype_id' => 15,
                'active' => 1,
                'created_at' => '2019-11-04 14:49:15',
                'updated_at' => '2019-11-24 09:53:56',
                'type_id' => 1,
            ),
            6 => 
            array (
                'id' => 13,
                'course_name' => 'الكتاب 1 فترة أولى',
                'max_num' => 10,
                'time' => NULL,
                'center_id' => 9,
                'coursetype_id' => 4,
                'active' => 4,
                'created_at' => '2019-11-07 16:19:54',
                'updated_at' => '2019-11-07 16:19:54',
                'type_id' => 1,
            ),
            7 => 
            array (
                'id' => 14,
                'course_name' => 'محمود الشناوى11',
                'max_num' => 20,
                'time' => NULL,
                'center_id' => 3,
                'coursetype_id' => 3,
                'active' => 1,
                'created_at' => '2019-11-17 09:11:07',
                'updated_at' => '2019-11-17 16:31:33',
                'type_id' => 1,
            ),
            8 => 
            array (
                'id' => 19,
                'course_name' => 'حلقه الخامس',
                'max_num' => 12,
                'time' => NULL,
                'center_id' => 3,
                'coursetype_id' => 2,
                'active' => 1,
                'created_at' => '2019-11-24 09:29:34',
                'updated_at' => '2019-11-24 09:29:34',
                'type_id' => 1,
            ),
            9 => 
            array (
                'id' => 20,
                'course_name' => 'حلقه الخامس',
                'max_num' => 12,
                'time' => 2,
                'center_id' => 3,
                'coursetype_id' => 3,
                'active' => 1,
                'created_at' => '2019-11-24 09:29:37',
                'updated_at' => '2019-11-24 09:29:37',
                'type_id' => 1,
            ),
            10 => 
            array (
                'id' => 21,
                'course_name' => 'القليوبيه3',
                'max_num' => 12,
                'time' => NULL,
                'center_id' => 3,
                'coursetype_id' => 5,
                'active' => 1,
                'created_at' => '2019-11-24 09:30:11',
                'updated_at' => '2019-11-24 09:30:11',
                'type_id' => 1,
            ),
            11 => 
            array (
                'id' => 22,
                'course_name' => 'asmaa kara11m',
                'max_num' => 12,
                'time' => NULL,
                'center_id' => 5,
                'coursetype_id' => 2,
                'active' => 1,
                'created_at' => '2019-11-24 09:44:51',
                'updated_at' => '2019-11-24 09:56:40',
                'type_id' => NULL,
            ),
            12 => 
            array (
                'id' => 23,
                'course_name' => 'المصري',
                'max_num' => 10,
                'time' => NULL,
                'center_id' => 15,
                'coursetype_id' => 2,
                'active' => 1,
                'created_at' => '2019-11-24 11:11:01',
                'updated_at' => '2019-11-24 11:11:01',
                'type_id' => NULL,
            ),
            13 => 
            array (
                'id' => 24,
                'course_name' => 'عمر كامل',
                'max_num' => 12,
                'time' => NULL,
                'center_id' => 25,
                'coursetype_id' => 38,
                'active' => 1,
                'created_at' => '2019-11-24 14:35:27',
                'updated_at' => '2019-11-24 14:35:27',
                'type_id' => NULL,
            ),
            14 => 
            array (
                'id' => 25,
                'course_name' => 'العارف بالله',
                'max_num' => 5,
                'time' => NULL,
                'center_id' => 26,
                'coursetype_id' => 2,
                'active' => 1,
                'created_at' => '2019-11-26 19:56:40',
                'updated_at' => '2019-11-26 19:56:40',
                'type_id' => NULL,
            ),
            15 => 
            array (
                'id' => 28,
                'course_name' => 'حلقه الاول',
                'max_num' => 12,
                'time' => NULL,
                'center_id' => 1,
                'coursetype_id' => 1,
                'active' => 1,
                'created_at' => '2019-12-03 14:42:33',
                'updated_at' => '2019-12-03 14:42:33',
                'type_id' => NULL,
            ),
            16 => 
            array (
                'id' => 29,
                'course_name' => 'الحمادي1',
                'max_num' => 10,
                'time' => NULL,
                'center_id' => 1,
                'coursetype_id' => 37,
                'active' => 1,
                'created_at' => '2019-12-03 14:56:20',
                'updated_at' => '2019-12-03 15:07:58',
                'type_id' => NULL,
            ),
            17 => 
            array (
                'id' => 30,
                'course_name' => 'حلقه الاول1',
                'max_num' => 45,
                'time' => NULL,
                'center_id' => 1,
                'coursetype_id' => 1,
                'active' => 1,
                'created_at' => '2019-12-03 15:05:55',
                'updated_at' => '2019-12-03 15:05:55',
                'type_id' => NULL,
            ),
            18 => 
            array (
                'id' => 32,
                'course_name' => 'ابو عمر',
                'max_num' => 5,
                'time' => NULL,
                'center_id' => 35,
                'coursetype_id' => 52,
                'active' => 1,
                'created_at' => '2019-12-04 18:19:30',
                'updated_at' => '2019-12-04 18:19:30',
                'type_id' => NULL,
            ),
            19 => 
            array (
                'id' => 34,
                'course_name' => 'محمد الرفاعي',
                'max_num' => 10,
                'time' => NULL,
                'center_id' => 40,
                'coursetype_id' => 65,
                'active' => 1,
                'created_at' => '2019-12-05 18:14:49',
                'updated_at' => '2019-12-05 18:14:49',
                'type_id' => NULL,
            ),
            20 => 
            array (
                'id' => 35,
                'course_name' => 'حلقة اليوم 08/12/2019',
                'max_num' => 10,
                'time' => NULL,
                'center_id' => 41,
                'coursetype_id' => 56,
                'active' => 1,
                'created_at' => '2019-12-08 07:30:13',
                'updated_at' => '2019-12-08 07:30:13',
                'type_id' => NULL,
            ),
            21 => 
            array (
                'id' => 36,
                'course_name' => 'حلقه الاول',
                'max_num' => 12,
                'time' => NULL,
                'center_id' => 41,
                'coursetype_id' => 60,
                'active' => 1,
                'created_at' => '2019-12-08 09:53:38',
                'updated_at' => '2019-12-08 09:53:38',
                'type_id' => NULL,
            ),
            22 => 
            array (
                'id' => 37,
                'course_name' => '1',
                'max_num' => 12,
                'time' => NULL,
                'center_id' => 1,
                'coursetype_id' => 1,
                'active' => 1,
                'created_at' => '2019-12-08 09:54:32',
                'updated_at' => '2019-12-08 09:54:32',
                'type_id' => NULL,
            ),
            23 => 
            array (
                'id' => 38,
                'course_name' => '2',
                'max_num' => 21,
                'time' => NULL,
                'center_id' => 1,
                'coursetype_id' => 59,
                'active' => 1,
                'created_at' => '2019-12-08 10:47:05',
                'updated_at' => '2019-12-08 10:47:05',
                'type_id' => NULL,
            ),
            24 => 
            array (
                'id' => 39,
                'course_name' => '5',
                'max_num' => 25,
                'time' => NULL,
                'center_id' => 35,
                'coursetype_id' => 57,
                'active' => 1,
                'created_at' => '2019-12-08 10:48:04',
                'updated_at' => '2019-12-08 10:48:04',
                'type_id' => NULL,
            ),
            25 => 
            array (
                'id' => 40,
                'course_name' => NULL,
                'max_num' => NULL,
                'time' => NULL,
                'center_id' => 1,
                'coursetype_id' => 55,
                'active' => NULL,
                'created_at' => '2019-12-08 15:34:01',
                'updated_at' => '2019-12-08 15:34:01',
                'type_id' => NULL,
            ),
            26 => 
            array (
                'id' => 41,
                'course_name' => 'الحلقه الجديده1',
                'max_num' => 12,
                'time' => NULL,
                'center_id' => 1,
                'coursetype_id' => 59,
                'active' => 1,
                'created_at' => '2019-12-09 09:45:28',
                'updated_at' => '2019-12-09 09:46:11',
                'type_id' => NULL,
            ),
            27 => 
            array (
                'id' => 42,
                'course_name' => NULL,
                'max_num' => NULL,
                'time' => NULL,
                'center_id' => 41,
                'coursetype_id' => 56,
                'active' => NULL,
                'created_at' => '2019-12-09 10:54:31',
                'updated_at' => '2019-12-09 10:54:31',
                'type_id' => NULL,
            ),
        ));
        
        
    }
}