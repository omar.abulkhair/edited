<?php

use Illuminate\Database\Seeder;

class VisitsTableSeeder extends Seeder
{

    /**
     * Auto generated seed file
     *
     * @return void
     */
    public function run()
    {
        

        \DB::table('visits')->delete();
        
        \DB::table('visits')->insert(array (
            0 => 
            array (
                'id' => 1,
                'country' => 'cairo',
                'date' => '2019-12-04 00:00:00',
            ),
        ));
        
        
    }
}