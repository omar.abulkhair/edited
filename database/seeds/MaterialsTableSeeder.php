<?php

use Illuminate\Database\Seeder;

class MaterialsTableSeeder extends Seeder
{

    /**
     * Auto generated seed file
     *
     * @return void
     */
    public function run()
    {
        

        \DB::table('materials')->delete();
        
        \DB::table('materials')->insert(array (
            0 => 
            array (
                'id' => 29,
                'material_name' => 'الدرس الاول - المتقدمون',
                'success' => 100,
                'p1' => NULL,
                'p2' => NULL,
                'p3' => NULL,
                'p4' => NULL,
                'p5' => NULL,
                'level_id' => 49,
                'course_id' => NULL,
                'active' => 1,
                'details' => 'ضصض',
                'created_at' => '2019-12-04 18:21:37',
                'updated_at' => '2019-12-09 09:48:19',
            ),
            1 => 
            array (
                'id' => 31,
                'material_name' => 'lesson12211111111',
                'success' => 12322222,
                'p1' => 12,
                'p2' => 32,
                'p3' => 22,
                'p4' => 2,
                'p5' => 1,
                'level_id' => 1,
                'course_id' => NULL,
                'active' => 1,
                'details' => 'd2323232323232323',
                'created_at' => '2019-12-05 09:41:52',
                'updated_at' => '2019-12-05 09:42:10',
            ),
            2 => 
            array (
                'id' => 32,
                'material_name' => 'الاول-ارتقاء',
                'success' => 100,
                'p1' => 100,
                'p2' => 90,
                'p3' => 80,
                'p4' => 70,
                'p5' => 50,
                'level_id' => 53,
                'course_id' => NULL,
                'active' => 1,
                'details' => 'السور من الانفال',
                'created_at' => '2019-12-05 18:23:17',
                'updated_at' => '2019-12-05 18:23:17',
            ),
            3 => 
            array (
                'id' => 33,
                'material_name' => 'درس 1 يوم 08-12-2019',
                'success' => 120,
                'p1' => 90,
                'p2' => 80,
                'p3' => 70,
                'p4' => 50,
                'p5' => 50,
                'level_id' => 54,
                'course_id' => NULL,
                'active' => 1,
                'details' => 'ينقسم الى 3 دروس',
                'created_at' => '2019-12-08 07:39:10',
                'updated_at' => '2019-12-08 07:39:10',
            ),
            4 => 
            array (
                'id' => 34,
                'material_name' => 'الدرس الاول',
                'success' => 10,
                'p1' => 10,
                'p2' => 22,
                'p3' => 11,
                'p4' => 21,
                'p5' => 1,
                'level_id' => 55,
                'course_id' => NULL,
                'active' => 1,
                'details' => 'لال',
                'created_at' => '2019-12-09 09:47:51',
                'updated_at' => '2019-12-09 09:47:51',
            ),
            5 => 
            array (
                'id' => 1,
                'material_name' => 'asmaa11',
                'success' => 1211,
                'p1' => 12,
                'p2' => 22,
                'p3' => 22,
                'p4' => 22,
                'p5' => 22,
                'level_id' => 47,
                'course_id' => 1,
                'active' => 1,
                'details' => 'asmaa11',
                'created_at' => '2019-12-03 14:43:51',
                'updated_at' => '2019-12-03 14:44:41',
            ),
            6 => 
            array (
                'id' => 2,
                'material_name' => 'القارعه',
                'success' => 50,
                'p1' => 100,
                'p2' => 90,
                'p3' => 80,
                'p4' => 70,
                'p5' => 55,
                'level_id' => 48,
                'course_id' => 2,
                'active' => 1,
                'details' => 'dsdfdf',
                'created_at' => '2019-12-03 14:58:28',
                'updated_at' => '2019-12-03 14:58:28',
            ),
            7 => 
            array (
                'id' => 28,
                'material_name' => 'الدرس الاول1',
                'success' => 20,
                'p1' => 12,
                'p2' => 2,
                'p3' => 12,
                'p4' => 21,
                'p5' => 22,
                'level_id' => 1,
                'course_id' => NULL,
                'active' => 1,
                'details' => 'تفاصيل',
                'created_at' => '2019-12-04 14:31:15',
                'updated_at' => '2019-12-04 14:31:29',
            ),
        ));
        
        
    }
}