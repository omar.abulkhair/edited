<?php

use Illuminate\Database\Seeder;

class LevelsTableSeeder extends Seeder
{

    /**
     * Auto generated seed file
     *
     * @return void
     */
    public function run()
    {
        

        \DB::table('levels')->delete();
        
        \DB::table('levels')->insert(array (
            0 => 
            array (
                'id' => 54,
                'level_name' => 'مستوى اليوم 08/12/2019',
                'course_id' => 0,
                'coursetype_id' => 56,
                'active' => 1,
                'created_at' => '2019-12-08 07:37:33',
                'updated_at' => '2019-12-08 07:37:33',
            ),
            1 => 
            array (
                'id' => 55,
                'level_name' => 'المستوي الرابع1',
                'course_id' => 0,
                'coursetype_id' => 58,
                'active' => 1,
                'created_at' => '2019-12-09 09:46:41',
                'updated_at' => '2019-12-09 09:46:58',
            ),
            2 => 
            array (
                'id' => 53,
                'level_name' => 'المستوي الاول-الارتقاء',
                'course_id' => 1,
                'coursetype_id' => 1,
                'active' => 1,
                'created_at' => '2019-12-05 18:15:32',
                'updated_at' => '2019-12-05 18:15:32',
            ),
            3 => 
            array (
                'id' => 51,
                'level_name' => 'حلقه الاول',
                'course_id' => 0,
                'coursetype_id' => 48,
                'active' => 0,
                'created_at' => '2019-12-05 09:35:54',
                'updated_at' => '2019-12-05 09:35:54',
            ),
            4 => 
            array (
                'id' => 50,
                'level_name' => 'level111111',
                'course_id' => 0,
                'coursetype_id' => 5,
                'active' => 0,
                'created_at' => '2019-12-05 09:35:10',
                'updated_at' => '2019-12-05 09:35:30',
            ),
            5 => 
            array (
                'id' => 49,
                'level_name' => 'المستوي الاول-المتقدمون',
                'course_id' => 0,
                'coursetype_id' => 52,
                'active' => 1,
                'created_at' => '2019-12-04 18:20:26',
                'updated_at' => '2019-12-04 18:20:26',
            ),
            6 => 
            array (
                'id' => 1,
                'level_name' => 'المستوي الاول11',
                'course_id' => 1,
                'coursetype_id' => 4,
                'active' => 1,
                'created_at' => '2019-12-03 14:57:09',
                'updated_at' => '2019-12-05 09:36:15',
            ),
            7 => 
            array (
                'id' => 3,
                'level_name' => 'المستوي الثالث ',
                'course_id' => 2,
                'coursetype_id' => 37,
                'active' => 1,
                'created_at' => NULL,
                'updated_at' => '2019-12-04 07:55:54',
            ),
            8 => 
            array (
                'id' => 2,
                'level_name' => 'المستوي التاني ',
                'course_id' => 2,
                'coursetype_id' => 38,
                'active' => 1,
                'created_at' => '2019-12-03 14:42:52',
                'updated_at' => '2019-12-03 14:43:05',
            ),
        ));
        
        
    }
}