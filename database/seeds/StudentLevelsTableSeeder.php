<?php

use Illuminate\Database\Seeder;

class StudentLevelsTableSeeder extends Seeder
{

    /**
     * Auto generated seed file
     *
     * @return void
     */
    public function run()
    {
        

        \DB::table('student_levels')->delete();
        
        \DB::table('student_levels')->insert(array (
            0 => 
            array (
                'id' => 1,
                'student_id' => 1,
                'level_id' => 1,
                'created_at' => NULL,
                'updated_at' => '2019-12-04 04:11:09',
            ),
        ));
        
        
    }
}