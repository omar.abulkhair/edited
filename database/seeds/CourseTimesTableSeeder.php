<?php

use Illuminate\Database\Seeder;

class CourseTimesTableSeeder extends Seeder
{

    /**
     * Auto generated seed file
     *
     * @return void
     */
    public function run()
    {
        

        \DB::table('course_times')->delete();
        
        \DB::table('course_times')->insert(array (
            0 => 
            array (
                'id' => 46,
                'course_id' => 23,
                'day' => 'السبت',
                'time_from' => '2:10 PM',
                'time_to' => '11:10 PM',
                'created_at' => '2019-11-24 11:11:01',
                'updated_at' => '2019-11-24 11:11:01',
            ),
            1 => 
            array (
                'id' => 43,
                'course_id' => 20,
                'day' => 'الأحد',
                'time_from' => '12:29 AM',
                'time_to' => '2:29 AM',
                'created_at' => '2019-11-24 09:29:37',
                'updated_at' => '2019-11-24 09:29:37',
            ),
            2 => 
            array (
                'id' => 44,
                'course_id' => 21,
                'day' => 'السبت',
                'time_from' => '12:30 AM',
                'time_to' => '2:30 AM',
                'created_at' => '2019-11-24 09:30:11',
                'updated_at' => '2019-11-24 09:30:11',
            ),
            3 => 
            array (
                'id' => 45,
                'course_id' => 22,
                'day' => 'الأحد',
                'time_from' => '1:44 AM',
                'time_to' => '3:44 AM',
                'created_at' => '2019-11-24 09:44:51',
                'updated_at' => '2019-11-24 09:44:51',
            ),
            4 => 
            array (
                'id' => 41,
                'course_id' => 18,
                'day' => 'الأحد',
                'time_from' => '10:47 AM',
                'time_to' => '12:47 AM',
                'created_at' => '2019-11-18 07:47:48',
                'updated_at' => '2019-11-18 07:47:48',
            ),
            5 => 
            array (
                'id' => 42,
                'course_id' => 19,
                'day' => 'الأحد',
                'time_from' => '12:29 AM',
                'time_to' => '2:29 AM',
                'created_at' => '2019-11-24 09:29:34',
                'updated_at' => '2019-11-24 09:29:34',
            ),
            6 => 
            array (
                'id' => 40,
                'course_id' => 1,
                'day' => NULL,
                'time_from' => NULL,
                'time_to' => NULL,
                'created_at' => '2019-11-17 15:09:32',
                'updated_at' => '2019-11-17 15:22:03',
            ),
            7 => 
            array (
                'id' => 39,
                'course_id' => 1,
                'day' => NULL,
                'time_from' => NULL,
                'time_to' => NULL,
                'created_at' => '2019-11-17 14:52:21',
                'updated_at' => '2019-11-17 15:09:32',
            ),
            8 => 
            array (
                'id' => 36,
                'course_id' => 1,
                'day' => NULL,
                'time_from' => NULL,
                'time_to' => NULL,
                'created_at' => '2019-11-17 13:11:36',
                'updated_at' => '2019-11-17 13:14:31',
            ),
            9 => 
            array (
                'id' => 37,
                'course_id' => 1,
                'day' => NULL,
                'time_from' => NULL,
                'time_to' => NULL,
                'created_at' => '2019-11-17 13:14:31',
                'updated_at' => '2019-11-17 13:16:05',
            ),
            10 => 
            array (
                'id' => 35,
                'course_id' => 1,
                'day' => NULL,
                'time_from' => NULL,
                'time_to' => NULL,
                'created_at' => '2019-11-17 13:10:07',
                'updated_at' => '2019-11-17 13:14:31',
            ),
            11 => 
            array (
                'id' => 34,
                'course_id' => 1,
                'day' => NULL,
                'time_from' => NULL,
                'time_to' => NULL,
                'created_at' => '2019-11-17 13:08:24',
                'updated_at' => '2019-11-17 13:14:31',
            ),
            12 => 
            array (
                'id' => 33,
                'course_id' => 16,
                'day' => 'السبت',
                'time_from' => '12:17 PM',
                'time_to' => '11:17 PM',
                'created_at' => '2019-11-17 11:17:37',
                'updated_at' => '2019-11-17 11:17:37',
            ),
            13 => 
            array (
                'id' => 32,
                'course_id' => 15,
                'day' => 'السبت',
                'time_from' => '12:14 PM',
                'time_to' => '11:14 PM',
                'created_at' => '2019-11-17 11:15:06',
                'updated_at' => '2019-11-17 11:15:06',
            ),
            14 => 
            array (
                'id' => 30,
                'course_id' => 2,
                'day' => NULL,
                'time_from' => NULL,
                'time_to' => NULL,
                'created_at' => '2019-11-17 10:39:32',
                'updated_at' => '2019-11-17 10:40:19',
            ),
            15 => 
            array (
                'id' => 31,
                'course_id' => 2,
                'day' => NULL,
                'time_from' => NULL,
                'time_to' => NULL,
                'created_at' => '2019-11-17 10:40:58',
                'updated_at' => '2019-11-17 14:43:25',
            ),
            16 => 
            array (
                'id' => 27,
                'course_id' => 1,
                'day' => NULL,
                'time_from' => NULL,
                'time_to' => NULL,
                'created_at' => '2019-11-17 08:21:32',
                'updated_at' => '2019-11-17 13:14:31',
            ),
            17 => 
            array (
                'id' => 29,
                'course_id' => 2,
                'day' => NULL,
                'time_from' => NULL,
                'time_to' => NULL,
                'created_at' => '2019-11-17 10:37:35',
                'updated_at' => '2019-11-17 10:39:32',
            ),
            18 => 
            array (
                'id' => 25,
                'course_id' => 2,
                'day' => NULL,
                'time_from' => NULL,
                'time_to' => NULL,
                'created_at' => '2019-11-13 13:24:03',
                'updated_at' => '2019-11-17 10:37:35',
            ),
            19 => 
            array (
                'id' => 24,
                'course_id' => 13,
                'day' => 'الثلاثاء',
                'time_from' => '4:45 PM',
                'time_to' => '5:45 PM',
                'created_at' => '2019-11-07 16:19:54',
                'updated_at' => '2019-11-07 16:19:54',
            ),
            20 => 
            array (
                'id' => 23,
                'course_id' => 13,
                'day' => 'السبت',
                'time_from' => '4:00 PM',
                'time_to' => '5:45 PM',
                'created_at' => '2019-11-07 16:19:54',
                'updated_at' => '2019-11-07 16:19:54',
            ),
            21 => 
            array (
                'id' => 21,
                'course_id' => 2,
                'day' => NULL,
                'time_from' => NULL,
                'time_to' => NULL,
                'created_at' => '2019-11-05 08:42:45',
                'updated_at' => '2019-11-05 08:42:46',
            ),
            22 => 
            array (
                'id' => 22,
                'course_id' => 2,
                'day' => NULL,
                'time_from' => NULL,
                'time_to' => NULL,
                'created_at' => '2019-11-05 08:42:46',
                'updated_at' => '2019-11-13 13:24:03',
            ),
            23 => 
            array (
                'id' => 20,
                'course_id' => 2,
                'day' => NULL,
                'time_from' => NULL,
                'time_to' => NULL,
                'created_at' => '2019-11-04 15:37:56',
                'updated_at' => '2019-11-05 08:41:52',
            ),
            24 => 
            array (
                'id' => 28,
                'course_id' => 14,
                'day' => 'السبت',
                'time_from' => '12:11 AM',
                'time_to' => '2:11 AM',
                'created_at' => '2019-11-17 09:11:07',
                'updated_at' => '2019-11-17 09:11:07',
            ),
            25 => 
            array (
                'id' => 15,
                'course_id' => 8,
                'day' => 'الثلاثاء',
                'time_from' => '5:45 PM',
                'time_to' => '7:30 PM',
                'created_at' => '2019-09-16 16:04:02',
                'updated_at' => '2019-09-16 16:04:02',
            ),
            26 => 
            array (
                'id' => 11,
                'course_id' => 6,
                'day' => 'الثلاثاء',
                'time_from' => '4:00 PM',
                'time_to' => '5:45 PM',
                'created_at' => '2019-09-16 16:01:02',
                'updated_at' => '2019-09-16 16:01:02',
            ),
            27 => 
            array (
                'id' => 14,
                'course_id' => 8,
                'day' => 'السبت',
                'time_from' => '5:45 PM',
                'time_to' => '7:30 PM',
                'created_at' => '2019-09-16 16:04:02',
                'updated_at' => '2019-09-16 16:04:02',
            ),
            28 => 
            array (
                'id' => 10,
                'course_id' => 6,
                'day' => 'السبت',
                'time_from' => '4:00 PM',
                'time_to' => '5:45 PM',
                'created_at' => '2019-09-16 16:01:02',
                'updated_at' => '2019-09-16 16:01:02',
            ),
            29 => 
            array (
                'id' => 12,
                'course_id' => 7,
                'day' => 'السبت',
                'time_from' => '5:45 PM',
                'time_to' => '7:30 PM',
                'created_at' => '2019-09-16 16:03:05',
                'updated_at' => '2019-09-16 16:03:05',
            ),
            30 => 
            array (
                'id' => 17,
                'course_id' => 10,
                'day' => 'السبت',
                'time_from' => '11:18 PM',
                'time_to' => '10:18 PM',
                'created_at' => '2019-11-04 12:19:05',
                'updated_at' => '2019-11-04 12:19:05',
            ),
            31 => 
            array (
                'id' => 18,
                'course_id' => 11,
                'day' => 'السبت',
                'time_from' => '11:18 PM',
                'time_to' => '10:18 PM',
                'created_at' => '2019-11-04 12:19:05',
                'updated_at' => '2019-11-04 12:19:05',
            ),
            32 => 
            array (
                'id' => 55,
                'course_id' => 31,
                'day' => 'السبت',
                'time_from' => '12:06 PM',
                'time_to' => '1:06 PM',
                'created_at' => '2019-12-03 15:06:52',
                'updated_at' => '2019-12-03 15:06:52',
            ),
            33 => 
            array (
                'id' => 16,
                'course_id' => 9,
                'day' => 'السبت',
                'time_from' => '11:18 PM',
                'time_to' => '10:18 PM',
                'created_at' => '2019-11-04 12:19:05',
                'updated_at' => '2019-11-04 12:19:05',
            ),
            34 => 
            array (
                'id' => 13,
                'course_id' => 7,
                'day' => 'الثلاثاء',
                'time_from' => '5:45 PM',
                'time_to' => '7:30 PM',
                'created_at' => '2019-09-16 16:03:05',
                'updated_at' => '2019-09-16 16:03:05',
            ),
            35 => 
            array (
                'id' => 54,
                'course_id' => 30,
                'day' => 'الأحد',
                'time_from' => '11:05 PM',
                'time_to' => '1:05 PM',
                'created_at' => '2019-12-03 15:05:55',
                'updated_at' => '2019-12-03 15:05:55',
            ),
            36 => 
            array (
                'id' => 53,
                'course_id' => 29,
                'day' => 'الأحد',
                'time_from' => '1:55 PM',
                'time_to' => '12:55 PM',
                'created_at' => '2019-12-03 14:56:20',
                'updated_at' => '2019-12-03 14:56:20',
            ),
            37 => 
            array (
                'id' => 52,
                'course_id' => 28,
                'day' => 'السبت',
                'time_from' => '12:42 PM',
                'time_to' => '10:42 PM',
                'created_at' => '2019-12-03 14:42:33',
                'updated_at' => '2019-12-03 14:42:33',
            ),
            38 => 
            array (
                'id' => 47,
                'course_id' => 24,
                'day' => 'السبت',
                'time_from' => '4:00 PM',
                'time_to' => '5:35 PM',
                'created_at' => '2019-11-24 14:35:27',
                'updated_at' => '2019-11-24 14:35:27',
            ),
            39 => 
            array (
                'id' => 48,
                'course_id' => 24,
                'day' => 'الثلاثاء',
                'time_from' => '4:00 PM',
                'time_to' => '5:35 PM',
                'created_at' => '2019-11-24 14:35:27',
                'updated_at' => '2019-11-24 14:35:27',
            ),
            40 => 
            array (
                'id' => 49,
                'course_id' => 25,
                'day' => 'السبت',
                'time_from' => '10:56 PM',
                'time_to' => '11:56 PM',
                'created_at' => '2019-11-26 19:56:40',
                'updated_at' => '2019-11-26 19:56:40',
            ),
            41 => 
            array (
                'id' => 56,
                'course_id' => 32,
                'day' => 'السبت',
                'time_from' => '9:19 PM',
                'time_to' => '9:19 PM',
                'created_at' => '2019-12-04 18:19:30',
                'updated_at' => '2019-12-04 18:19:30',
            ),
            42 => 
            array (
                'id' => 58,
                'course_id' => 34,
                'day' => 'السبت',
                'time_from' => '9:14 PM',
                'time_to' => '11:14 PM',
                'created_at' => '2019-12-05 18:14:49',
                'updated_at' => '2019-12-05 18:14:49',
            ),
            43 => 
            array (
                'id' => 59,
                'course_id' => 35,
                'day' => 'السبت',
                'time_from' => '9:30 AM',
                'time_to' => '11:30 AM',
                'created_at' => '2019-12-08 07:30:13',
                'updated_at' => '2019-12-08 07:30:13',
            ),
            44 => 
            array (
                'id' => 60,
                'course_id' => 35,
                'day' => 'الأثنين',
                'time_from' => '9:30 AM',
                'time_to' => '11:30 AM',
                'created_at' => '2019-12-08 07:30:14',
                'updated_at' => '2019-12-08 07:30:14',
            ),
            45 => 
            array (
                'id' => 61,
                'course_id' => 35,
                'day' => 'الأربعاء',
                'time_from' => '9:30 AM',
                'time_to' => '11:30 AM',
                'created_at' => '2019-12-08 07:30:14',
                'updated_at' => '2019-12-08 07:30:14',
            ),
            46 => 
            array (
                'id' => 62,
                'course_id' => 36,
                'day' => 'السبت',
                'time_from' => '1:53 AM',
                'time_to' => '10:53 AM',
                'created_at' => '2019-12-08 09:53:38',
                'updated_at' => '2019-12-08 09:53:38',
            ),
            47 => 
            array (
                'id' => 63,
                'course_id' => 37,
                'day' => 'الأحد',
                'time_from' => '2:54 AM',
                'time_to' => '10:54 AM',
                'created_at' => '2019-12-08 09:54:32',
                'updated_at' => '2019-12-08 09:54:32',
            ),
            48 => 
            array (
                'id' => 64,
                'course_id' => 38,
                'day' => 'السبت',
                'time_from' => '1:46 PM',
                'time_to' => '11:47 PM',
                'created_at' => '2019-12-08 10:47:05',
                'updated_at' => '2019-12-08 10:47:05',
            ),
            49 => 
            array (
                'id' => 65,
                'course_id' => 39,
                'day' => 'السبت',
                'time_from' => '2:47 PM',
                'time_to' => '11:48 PM',
                'created_at' => '2019-12-08 10:48:04',
                'updated_at' => '2019-12-08 10:48:04',
            ),
            50 => 
            array (
                'id' => 66,
                'course_id' => 41,
                'day' => 'السبت',
                'time_from' => '1:45 AM',
                'time_to' => '12:45 AM',
                'created_at' => '2019-12-09 09:45:28',
                'updated_at' => '2019-12-09 09:45:28',
            ),
        ));
        
        
    }
}