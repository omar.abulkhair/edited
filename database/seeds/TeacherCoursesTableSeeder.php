<?php

use Illuminate\Database\Seeder;

class TeacherCoursesTableSeeder extends Seeder
{

    /**
     * Auto generated seed file
     *
     * @return void
     */
    public function run()
    {
        

        \DB::table('teacher_courses')->delete();
        
        \DB::table('teacher_courses')->insert(array (
            0 => 
            array (
                'id' => 34,
                'teacher_id' => 31,
                'course_id' => 1,
                'created_at' => '2019-12-09 10:55:59',
                'updated_at' => '2019-12-09 10:55:59',
            ),
            1 => 
            array (
                'id' => 33,
                'teacher_id' => 30,
                'course_id' => 32,
                'created_at' => '2019-12-09 07:18:36',
                'updated_at' => '2019-12-09 07:18:36',
            ),
            2 => 
            array (
                'id' => 32,
                'teacher_id' => 29,
                'course_id' => 36,
                'created_at' => '2019-12-08 15:40:08',
                'updated_at' => '2019-12-08 15:40:08',
            ),
            3 => 
            array (
                'id' => 31,
                'teacher_id' => 30,
                'course_id' => 34,
                'created_at' => '2019-12-08 15:37:04',
                'updated_at' => '2019-12-08 15:37:04',
            ),
            4 => 
            array (
                'id' => 30,
                'teacher_id' => 30,
                'course_id' => 11,
                'created_at' => '2019-12-08 15:34:54',
                'updated_at' => '2019-12-08 15:34:54',
            ),
            5 => 
            array (
                'id' => 29,
                'teacher_id' => 29,
                'course_id' => 10,
                'created_at' => '2019-12-08 12:49:58',
                'updated_at' => '2019-12-08 12:49:58',
            ),
            6 => 
            array (
                'id' => 28,
                'teacher_id' => 24,
                'course_id' => 2,
                'created_at' => '2019-12-04 14:24:13',
                'updated_at' => '2019-12-04 14:24:13',
            ),
            7 => 
            array (
                'id' => 27,
                'teacher_id' => 1,
                'course_id' => 1,
                'created_at' => '2019-12-04 09:25:53',
                'updated_at' => '2019-12-04 09:25:53',
            ),
        ));
        
        
    }
}