<?php

use Illuminate\Database\Seeder;

class UsersTableSeeder extends Seeder
{

    /**
     * Auto generated seed file
     *
     * @return void
     */
    public function run()
    {


        DB::table('users')->insert([
            'username' => 'admin',
            'type' => 'admin',
            'name' => 'admin',
            'permmisions' => 'a:2:{i:0;i:1;i:1;i:2;}',
            'email' => 'omar.alhillal@email.com',
            'password' => bcrypt('123'),
        ]);

    }
}