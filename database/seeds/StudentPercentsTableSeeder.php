<?php

use Illuminate\Database\Seeder;

class StudentPercentsTableSeeder extends Seeder
{

    /**
     * Auto generated seed file
     *
     * @return void
     */
    public function run()
    {
        

        \DB::table('student_percents')->delete();
        
        \DB::table('student_percents')->insert(array (
            0 => 
            array (
                'id' => 1,
                'grade_id' => 1,
                'material_id' => 1,
                'percent_id' => 1,
                'grade' => 1,
                'created_at' => NULL,
                'updated_at' => '2019-12-04 04:24:19',
            ),
        ));
        
        
    }
}