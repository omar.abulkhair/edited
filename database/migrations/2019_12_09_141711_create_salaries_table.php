<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateSalariesTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('salaries', function(Blueprint $table)
		{
			$table->integer('id', true);
			$table->integer('teacher_id')->nullable();
			$table->integer('salary')->nullable();
			$table->integer('days')->nullable();
			$table->text('hours', 65535)->nullable();
			$table->text('month', 65535)->nullable();
			$table->text('year', 65535)->nullable();
			$table->integer('bonus')->nullable();
			$table->integer('minus')->nullable();
			$table->integer('parts')->nullable();
			$table->integer('final')->nullable();
			$table->integer('status')->nullable();
			$table->text('notes', 65535)->nullable();
			$table->timestamps();
		});
	}


	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('salaries');
	}

}
