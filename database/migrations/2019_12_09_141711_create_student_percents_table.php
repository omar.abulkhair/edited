<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateStudentPercentsTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('student_percents', function(Blueprint $table)
		{
			$table->integer('id', true);
			$table->integer('grade_id')->nullable();
			$table->integer('material_id')->nullable();
			$table->integer('percent_id')->nullable();
			$table->integer('grade')->nullable();
			$table->timestamps();
		});
	}


	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('student_percents');
	}

}
