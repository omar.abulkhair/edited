<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateCentersTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('centers', function(Blueprint $table)
		{
			$table->integer('id', true);
			$table->text('center_name', 65535)->nullable();
			$table->integer('town_id')->nullable();
			$table->text('address', 65535)->nullable();
			$table->text('manager', 65535)->nullable();
			$table->text('head_manager', 65535)->nullable();
			$table->text('phone2', 65535)->nullable();
			$table->text('phone3', 65535)->nullable();
			$table->text('phone', 65535)->nullable();
			$table->text('email', 65535)->nullable();
			$table->text('website', 65535)->nullable();
			$table->integer('active')->nullable();
			$table->integer('age')->nullable();
			$table->timestamps();
			$table->integer('type_id')->nullable()->default(1);
		});
	}


	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('centers');
	}

}
