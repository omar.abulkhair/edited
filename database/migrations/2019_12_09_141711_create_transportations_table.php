<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateTransportationsTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('transportations', function(Blueprint $table)
		{
			$table->integer('id', true);
			$table->boolean('sat')->nullable();
			$table->boolean('sun')->nullable();
			$table->boolean('mon')->nullable();
			$table->boolean('tue')->nullable();
			$table->boolean('wed')->nullable();
			$table->boolean('thu')->nullable();
			$table->boolean('fri')->nullable();
			$table->text('arrival', 65535)->nullable();
			$table->text('launch', 65535)->nullable();
			$table->integer('price')->nullable();
			$table->text('bus', 65535)->nullable();
			$table->integer('manager_id')->nullable();
			$table->integer('center_id')->nullable();
			$table->integer('active')->nullable();
			$table->timestamps();
		});
	}


	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('transportations');
	}

}
