<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateStudentGradesTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('student_grades', function(Blueprint $table)
		{
			$table->integer('id', true);
			$table->integer('student_id')->nullable();
			$table->integer('material_id')->nullable();
			$table->integer('center_id')->nullable();
			$table->date('date')->nullable();
			$table->string('save')->nullable();
			$table->string('tajweed')->nullable();
			$table->string('performance')->nullable();
			$table->integer('total')->nullable();
			$table->text('percent', 65535)->nullable();
			$table->timestamps();
		});
	}


	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('student_grades');
	}

}
