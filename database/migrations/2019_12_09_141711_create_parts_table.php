<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreatePartsTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('parts', function(Blueprint $table)
		{
			$table->integer('id', true);
			$table->integer('teacher_id')->nullable();
			$table->integer('material_id')->nullable();
			$table->string('final')->nullable();
			$table->string('total')->nullable();
			$table->string('percent')->nullable();
			$table->text('month', 65535)->nullable();
			$table->text('year', 65535)->nullable();
			$table->integer('day')->nullable();
			$table->integer('part')->nullable();
			$table->timestamps();
		});
	}


	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('parts');
	}

}
