<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateUsersTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('users', function(Blueprint $table)
		{
			$table->increments('id');
			$table->string('name')->nullable();
			$table->text('name_en', 65535)->nullable();
			$table->text('username', 65535)->nullable();
			$table->string('type', 50)->nullable();
			$table->string('permmisions');
			$table->string('mobile')->nullable();
			$table->text('website', 65535)->nullable();
			$table->text('about', 65535)->nullable();
			$table->string('email')->nullable()->unique();
			$table->string('password')->nullable();
			$table->text('recover', 65535)->nullable();
			$table->string('country')->nullable();
			$table->string('facebook')->nullable();
			$table->string('twitter')->nullable();
			$table->string('google')->nullable();
			$table->string('instagram')->nullable();
			$table->text('linkedin', 65535)->nullable();
			$table->text('phone', 65535)->nullable();
			$table->string('address')->nullable();
			$table->text('details', 65535)->nullable();
			$table->string('image')->nullable();
			$table->text('remember_token', 65535)->nullable();
			$table->boolean('active')->nullable();
			$table->timestamps();
		});
	}


	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('users');
	}

}
