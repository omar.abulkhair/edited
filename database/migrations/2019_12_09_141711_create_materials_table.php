<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateMaterialsTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('materials', function(Blueprint $table)
		{
			$table->integer('id', true);
			$table->text('material_name', 65535)->nullable();
			$table->integer('success')->nullable();
			$table->integer('p1')->nullable();
			$table->integer('p2')->nullable();
			$table->integer('p3')->nullable();
			$table->integer('p4')->nullable();
			$table->integer('p5')->nullable();
			$table->integer('level_id')->nullable();
			$table->integer('course_id')->nullable();
			$table->integer('active')->nullable();
			$table->string('details')->nullable();
			$table->timestamps();
		});
	}


	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('materials');
	}

}
