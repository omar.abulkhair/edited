<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateStudentsTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('students', function(Blueprint $table)
		{
			$table->integer('id', true);
			$table->text('image', 65535)->nullable();
			$table->text('student_name', 65535)->nullable();
			$table->date('birth')->nullable();
			$table->text('gender', 65535)->nullable();
			$table->text('address', 65535)->nullable();
			$table->text('email', 65535)->nullable();
			$table->string('phone')->nullable();
			$table->text('national_id', 65535)->nullable();
			$table->integer('exemption_id')->nullable()->default(2);
			$table->integer('material_id')->nullable()->default(1);
			$table->date('first_day')->nullable();
			$table->text('year', 65535)->nullable();
			$table->integer('age')->nullable();
			$table->integer('guardian_id')->nullable();
			$table->integer('coursetype_id')->nullable();
			$table->integer('studentgrade_id')->nullable();
			$table->text('nationality', 65535)->nullable();
			$table->boolean('transportation')->nullable();
			$table->integer('center_id')->nullable();
			$table->integer('level_id')->nullable();
			$table->integer('part_id')->nullable()->default(1);
			$table->integer('attend_id')->nullable()->default(1);
			$table->integer('course_id')->nullable();
			$table->integer('season_id')->nullable();
			$table->integer('active')->nullable()->default(1);
			$table->text('notes', 65535)->nullable();
			$table->timestamps();
		});
	}


	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('students');
	}

}
