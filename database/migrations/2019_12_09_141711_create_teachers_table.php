<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateTeachersTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('teachers', function(Blueprint $table)
		{
			$table->integer('id', true);
			$table->text('code', 65535)->nullable();
			$table->text('teacher_name', 65535)->nullable();
			$table->text('username', 65535)->nullable();
			$table->text('password', 65535)->nullable();
			$table->text('recover', 65535)->nullable();
			$table->date('birth')->nullable();
			$table->text('address', 65535)->nullable();
			$table->text('email', 65535)->nullable();
			$table->string('job');
			$table->text('phone', 65535)->nullable();
			$table->integer('job_id')->nullable();
			$table->integer('hours')->nullable();
			$table->text('image', 65535)->nullable();
			$table->integer('salary')->nullable();
			$table->date('first_day')->nullable();
			$table->string('holiday')->nullable()->default('saturday/friday');
			$table->text('national_id', 65535)->nullable();
			$table->integer('center_id')->nullable();
			$table->integer('course_id')->nullable();
			$table->integer('coursetype_id')->nullable();
			$table->integer('active')->nullable();
			$table->timestamps();
		});
	}


	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('teachers');
	}

}
